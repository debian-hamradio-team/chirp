��         �  �  l!      �,  -   �,  9   �,     -  ;   '-  +   c-  )   �-  &   �-     �-  I   �-     I.     P.  6   _.    �.  �   �/  �  J0  �   �1  �   m2  �   3  �   �3    �4  �   �5    �6  �   �7  �   �8  �   �9  �   �:    x;  o  �<  q  �=  �   f?  �   B@  p  A  e  �B  �   �C    �D  �   �E  �   �F  �   �G  �   ~H  �   kI  �   VJ  �   >K  	  <L  �   FM  �   N  �   �N  �   �O  �   {P  �   \Q  #  :R    ^S  M  jT  �   �U  �   sV  �   *W  �   �W    �X    �Y  �   �Z  �   �[  �   �\  m   ]  �   ]  �   ^^  �   H_  �   �_    �`  �   b  �   �b  �   �c  ^   �d  %   e     Be     He     Te     Ze  	   ^e     he     e     �e     �e     �e     �e     �e     �e     �e     f     
f     f     f     +f     7f     Df  <   [f  y   �f     g  ]   g  J   wg     �g     �g     �g     �g     h     h     ,h     Bh  (   Ph     yh      ~h    �h  ,   �i     �i     �i     j     j     j  
   *j  (   5j  %   ^j     �j     �j     �j  Q   �j  k   �j     jk     xk     }k  
   �k     �k  	   �k     �k     �k     �k     �k  	   �k     �k     �k     �k     �k      l  A   l     Ql     cl     l     �l     �l     �l     �l     �l     �l      �l     m     m     /m     Fm     \m     cm     vm  C   �m     �m     �m     �m     	n      n     (n     8n     Kn     dn     �n  �   �n  �   o  (   �o  +   p  �   Ip     �p     �p     �p     
q     )q     8q     Pq     ]q     mq     ~q     �q     �q  $   �q     �q     r     $r     2r     Cr     Ir  e   Rr  u   �r     .s     Ks     bs     }s     �s     �s     �s     �s     �s  1   �s  	   �s     �s  	   t     t     t     !t     At  �   Wt  �   "u  �   �u    �v  �   �w  �   jx  �   %y  �   �y  �   }z  �   C{  8  |  �   :}  �   �}  �   �~  �   .  �   �  �   {�  �   �  .   Á  	   �  &   ��     #�  +   @�  (   l�  -   ��     Â     Ȃ     ق     �     �     ��  
   ��     
�     �  Y   "�  ,   |�  7   ��     �     �     ��     �     #�     2�     8�     =�     I�     Z�     p�     ��  '   ��     Ä     ф     �     �  "   �     &�     8�     F�     K�     T�     ]�     o�     {�     ��     ��  4   ��  	   ؅  
   �     �     ��     �  �   -�  v  ��     +�     <�  .   D�     s�     ��  	   ��     ��  :        ��     �     6�  %   U�     {�     ��     ��     ��     ��     ��     ��  	   ܉     �  	   �     �  5   �  
   K�     V�     l�     ��     ��     ��  7   ��  
   ��     �     �     �     �  
   .�     9�     L�     _�      u�     ��     ��     ��     ��     Ћ     ܋     �     ��     �      �     <�     E�     L�     `�     n�     ��     ��     ��      ��     ߌ  �   �     z�     ��     ��  3   ��  *   ō  (   ��  &   �  ?   @�  �  ��  (  E�    n�     ��  $   ��     ��     ͒     Ғ     ؒ  !   �     �     �     �  
   .�     9�     G�     V�     b�     k�     x�     ��     ��     ��     ��     Ó     ɓ     �     ��  �   �  ;   ��  t   �     ]�     o�     ��  	   ��     ��     ��     ��     ̕     ڕ     �     ��     �  W   $�     |�     ��     ��  �   ��     y�     ��     ��     ��     ؗ     �     �  	   �     �  +   �  	   J�  	   T�     ^�     l�     �     ��     ��     ��     ��     ۘ     �     �  :   ��  9   7�     q�     ��     ��     ��     ƙ  .   ˙     ��     �  3   :�  5   n�     ��     ��          ʚ     К     ߚ     �  (   �  �   .�     ��  �  ߛ  �   ��  �   ��  �   )�  ,   !�  ^  N�     ��  �   ��  G   t�  �   ��  |   ��  �   *�  #   �  =   �  <  T�  �   ��  V   v�     ͨ     �  +  	�  &  5�  �   \�  r  K�  ,   ��     �  	   �     ��     �     �  /   (�     X�  <   f�  .   ��  6   Ү     	�  @   �     V�  L   f�  ,   ��     �  )   ��     )�     F�  �   V�  "    �     #�     C�     ^�  /   l�     ��     ��     ȱ     ر  6   �     #�     3�     F�     Y�     n�     ~�     ��     ��     ��  '   ܲ     �      "�     C�     J�     Q�     V�     _�     g�     s�  �   {�  7   0�  �   h�  �   �     ��  !   ҵ     ��     ��  
   ��     
�     �     �  y  *�  ,   ��  R   ѷ     $�  V   ;�  2   ��  $   Ÿ  )   �  "   �  L   7�     ��     ��  H   ��  g  �  �   I�  �  �  �   �  �   о  �   ��  0  u�  D  ��    ��  %  �  "  (�    K�    Q�  �   W�  *  Q�  �  |�  �  (�  �   ��  �   ��  �  ��  �  +�    ��  >  ��  �   �  �   �    ��  �   ��  
  ��  �   ��    ��  %  �  �   '�  �   	�  �   ��  �   ��     ��  �   ��  ^  ��  &  �  �  3�  �   ��  �   ��  �   ��  �   y�  V  V�  M  ��    ��    �  �   )�     ��  -  M�  (  {�  �   ��    x�  @  ��  &  ��  
  ��     �  {   	�  $   ��  	   ��     ��  
   ��     ��     ��     ��  %   �  
   ,�     7�     L�     ^�     v�     ��     ��     ��     ��  	   ��     ��     ��     ��     ��  B   �  �   _�     ��  ]   �  N   _�     ��     ��     ��     ��     �     �     5�     O�  /   ^�     ��  0   ��  F  ��  2   �     @�     I�     ^�     o�     v�     ��  4   ��  &   ��  
   ��     ��  
     h     p   �     �                 !    6    G    N    S    b 
   w    �    �    �    �    � V   �    #    A    `    p    �    � 	   �    �    � '   � 	   	        )    B    Z    f    � R   �    �     �         8 
   Y    d    x    � )   �    � �   � �   � 3   � 3   � �   �    z    �    � !   �    �    �            "    0    K    h '   �    � *   �    �    �    	    	 �   	 �   �	 "   ,
    O
 $   n
    �
    �
    �
    �
 	   �
    �
 :   �
 
   *    5    < 	   M    W %   j    � �   � �   � �   j   8 �   V �    �   � �   q �   > �    N  � �   ' �   � �   � �   = �   � �   � �   5 9   � 
    0    !   O /   q ,   � 9   �                 -    ;    C    I    U    a �   z ;    M   A    �    �    �    �    �    �    �            )    I    e *   �    �    �    �    � +       2    F    [    c    l    t    �    �    �    � :   �    �    
        *     H �   i �       �!    �! G   �! %   �! %   "    E"    N" P   W"    �" '   �" ,   �" 2   # )   M#    w#    |#    �#    �#    �#    �#    �#     �#    �#    �# Q   
$    \$    j$    �$    �$    �$ )   �$ D   �$    D%    V%    k%    r%     �% 
   �%    �%    �%    �% 1   �%    &    =&    C&    R&    p&    �&    �&    �&    �& +   �&    '    '    '    3'    A'    S'    e'    w' !   �'    �' �   �' 
   j(    u(    �( ;   �( 0   �( 2   �( .   ') M   V)   �) \  �+ B  -    Z. ,   k. $   �.    �.    �.    �. .   �.    /    %/    1/    J/    V/    m/    �/    �/    �/ "   �/    �/     �/    0    0    #0 #   )0    M0    c0 �   t0 R   ?1 �   �1    2    02    I2    R2    ^2    v2    �2    �2    �2    �2 *   �2    3 c   3 !   �3    �3    �3 �   �3    �4 ,   �4    �4    �4    5    .5    65    Q5    a5 5   s5    �5    �5    �5    �5    �5    6     6    26 /   L6    |6    �6    �6 O   �6 P   �6    C7 .   \7    �7 7   �7    �7 1   �7 !   8 &   98 N   `8 P   �8     9    9 	   &9    09    79    H9 &   O9 )   v9 �   �9    _: Q  w: �   �= �   �> '  r? (   �@ �  �@    PB �   ]B N   IC �   �C �   �D �   .E .   
F M   9F h  �F   �G X   �H *   NI ,   yI 7  �I h  �J   GL �  _M ;   O    XO    ]O    jO    ~O    �O L   �O     P X   P 9   nP K   �P    �P Q   Q    ]Q V   uQ =   �Q 0   
R 2   ;R !   nR    �R �   �R &   sS )   �S    �S    �S D   �S    8T    OT    mT    �T K   �T    �T    �T    U    U    2U    JU    \U    uU     �U 0   �U    �U %   V    )V    1V    :V    >V    MV    YV 
   iV �   tV >   DW �   �W �   VX    Y     %Y    FY    KY 
   QY    \Y 
   jY    uY    \   �   @          ^       ~   j  3      �  �  >              �   G   U   �  �      �  U               u  �        �   �   �           �      �   %  Y  -      !         �      m  �   ?            �  .  P  �  s     �          7        �   l       �  �  �  $   �       �   �   �  \  z   �   �  X   �             �     �  �      	  9    �  �  E  R       �  �   V      )   T     �      �  �   �  �   K   _             �    �     �      �      �   -   ,      ]   �       �   �   `      �      w  �        �       P                          �  �  ^      �   Q              
   �   �  7   �           �  �       �  �   J   �   �       �       �  q      �  �  j   �   �    S  �      0  }      �      �  +  �   �          e  �   �              �  g  �      �     p   �   V   r  H      0   (       k      5              I  �  �  F  �          �   N  Z  Z   `       1    �         �  �  �  �   z      �  (      �    �   !  �  �  �   <     *   �       v   	  �   s  ?   @      �   �    <  �   o   d   {       2   �     T           �      [       �  �   5   	   L     G    �     |   '           �   �  �   =  �  �           t   %   �  W  �   �  �             �  �  �   M         M          �   �                  �                      �  �            �     �  |  g   q       �   �      t  +   a   �  )      =       
  �  �   B    �       1       Q       �   B       �   �   �   �  �      �  O       �  �  b   #       �   �   �           .   �      �  f  u   :          ,   p  �   �  �   �  �  �       �  [  �  �       �      �     �    �   �   �      �  �       �   �  �   k         �      {  �       �           �   4   �  "  �              �   �         �   O      �          D  �   �  �   &   i                  �  _       y    #    3  �     �   9              }      �   8           ~            '            �   �   d  �  v  �  C      L  �  x    h         X    �       /  �  c   �   �                    ;      �   I   S   �   R  �  �  �  �  �      A   e   �   �   �           w   �     �   f   J  �               b  F       �  �   :   E       �  �   �         l  �   m           �       "   �   �    c  �         �   �             �  �  8  �       �   /   &      y       >             �     �  ;       �  i             $  �   K  �       �  �   n       �   �   �   �   6      h  �  *      �  �   N                  �   o  4  �   �   �   �  �  n       �   C       �            a  �  6           r   2  D   �   �  H     �   �  �  W   �   �  �      �   �  ]                         �             x   Y   
      �      A    �   %(value)s must be between %(min)i and %(max)i %i Memories and shift all up %i Memories and shift all up %i Memory %i Memories %i Memory and shift block up %i Memories and shift block up %s has not been saved. Save before closing? (Describe what actually happened instead) (Describe what you expected to happen) (Describe what you were doing) (Has this ever worked before? New radio? Does it work with OEM software?) (none) ...and %i more 1. Connect programming cable to MIC jack.
2. Press OK. 1. Ensure your firmware version is 4_10 or higher
2. Turn radio off
3. Connect your interface cable
4. Turn radio on
5. Press and release PTT 3 times while holding MONI key
6. Supported baud rates: 57600 (default) and 19200
   (rotate dial while holding MONI to change)
7. Click OK
 1. Turn Radio off.
2. Connect data cable.
3. While holding "A/N LOW" button, turn radio on.
4. <b>After clicking OK</b>, press "SET MHz" to send image.
 1. Turn Radio off.
2. Connect data cable.
3. While holding "A/N LOW" button, turn radio on.
4. Press "MW D/MR" to receive image.
5. Make sure display says "-WAIT-" (see note below if not)
6. Click OK to dismiss this dialog and start transfer.
Note: if you don't see "-WAIT-" at step 5, try cycling
      power and pressing and holding red "*L" button to unlock
      radio, then start back at step 1.
 1. Turn Radio off.
2. Connect data cable.
3. While holding "F/W" button, turn radio on.
4. <b>After clicking OK</b>, press "RPT" to send image.
 1. Turn Radio off.
2. Connect data cable.
3. While holding "F/W" button, turn radio on.
4. Press "REV" to receive image.
5. Click OK to start transfer.
 1. Turn Radio off.
2. Connect data cable.
3. While holding "TONE" and "REV" buttons, turn radio on.
4. <b>After clicking OK</b>, press "TONE" to send image.
 1. Turn Radio off.
2. Connect data cable.
3. While holding "TONE" and "REV" buttons, turn radio on.
4. Press "REV" to receive image.
5. Make sure display says "CLONE RX" and green led is blinking
6. Click OK to start transfer.
 1. Turn radio off.
2. Connect cable
3. Press and hold in the MHz, Low, and D/MR keys on the radio while turning it on
4. Radio is in clone mode when TX/RX is flashing
5. <b>After clicking OK</b>, press the MHz key on the radio to send image.
    ("TX" will appear on the LCD). 
 1. Turn radio off.
2. Connect cable
3. Press and hold in the MHz, Low, and D/MR keys on the radio while turning it on
4. Radio is in clone mode when TX/RX is flashing
5. Press the Low key on the radio ("RX" will appear on the LCD).
6. Click OK. 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. <b>After clicking OK</b> here, press the [C.S.] key to
    send image.
 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. <b>After clicking OK</b>, press the [A] key to send image.
 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. Click OK here.
    ("Receiving" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. Press the [A](RCV) key ("receiving" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. Press the [C] key ("RX" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to CAT/LINEAR jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. <b>After clicking OK</b>,
     press the [C](SEND) key to send image.
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the "left" [V/M] key while turning the
     radio on.
4. Rotate the "right" DIAL knob to select "CLONE START".
5. Press the [SET] key. The display will disappear
     for a moment, then the "CLONE" notation will appear.
6. <b>After clicking OK</b>, press the "left" [V/M] key to
     send image.
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the "left" [V/M] key while turning the
     radio on.
4. Rotate the "right" DIAL knob to select "CLONE START".
5. Press the [SET] key. The display will disappear
     for a moment, then the "CLONE" notation will appear.
6. Press the "left" [LOW] key ("CLONE -RX-" will appear on
     the display).
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the [FW] key while turning the radio on
     ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [BAND] key to send image.
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the [FW] key while turning the radio on
     ("CLONE" will appear on the display).
4. Press the [MODE] key ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the [MHz(PRI)] key while turning the
     radio on.
4. Rotate the DIAL job to select "F-7 CLONE".
5. Press and hold in the [BAND(SET)] key. The display
     will disappear for a moment, then the "CLONE" notation
     will appear.
6. Press the [LOW(ACC)] key ("--RX--" will appear on the display).
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the [MHz(PRI)] key while turning the
 radio on.
4. Rotate the DIAL job to select "F-7 CLONE".
5. Press and hold in the [BAND(SET)] key. The display
 will disappear for a moment, then the "CLONE" notation
 will appear.
6. <b>After clicking OK</b>, press the [V/M(MW)] key to send image.
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold [DISP] key while turning on radio
     ("CLONE" will appear on radio LCD).
4. <b>After clicking OK here in Chirp</b>,
     press the [Send] screen button.
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold in [DISP] key while turning on radio
     ("CLONE" will appear on radio LCD).
4. Press [RECEIVE] screen button
     ("-WAIT-" will appear on radio LCD).
5. Finally, press OK button below.
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold in the [F] key while turning the radio on
     ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [BAND] key to send image.
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold in the [F] key while turning the radio on
     ("CLONE" will appear on the display).
4. Press the [Dx] key ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold in the [MHz(SETUP)] key while turning the radio
     on ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [REV(DW)] key
     to send image.
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold in the [MHz(SETUP)] key while turning the radio
     on ("CLONE" will appear on the display).
4. Press the [MHz(SETUP)] key
     ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC Jack.
3. Press and hold in the [MHz(SETUP)] key while turning the radio
     on ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [GM(AMS)] key
     to send image.
 1. Turn radio off.
2. Connect cable to MIC Jack.
3. Press and hold in the [MHz(SETUP)] key while turning the radio
     on ("CLONE" will appear on the display).
4. Press the [MHz(SETUP)] key
     ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC/EAR jack.
3. Press and hold in the [F/W] key while turning the radio on
    ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [VFO(DW)SC] key to receive
    the image from the radio.
 1. Turn radio off.
2. Connect cable to MIC/EAR jack.
3. Press and hold in the [F/W] key while turning the radio on
    ("CLONE" will appear on the display).
4. Press the [MR(SKP)SC] key ("CLONE WAIT" will appear
    on the LCD).
5. Click OK to send image to radio.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold [PTT] &amp; Knob while turning the
     radio on.
4. <b>After clicking OK</b>, press the [PTT] switch to send image.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold [PTT] &amp; Knob while turning the
     radio on.
4. Press the [MONI] switch ("WAIT" will appear on the LCD).
5. Press OK.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [F/W] key while turning the radio on
     ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [BAND] key to send image.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [F/W] key while turning the radio on
     ("CLONE" will appear on the display).
4. Press the [V/M] key ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [MON-F] key while turning the radio on
     ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [BAND] key to send image.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [MON-F] key while turning the radio on
     ("CLONE" will appear on the display).
4. Press the [V/M] key ("CLONE WAIT" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [MONI] switch while turning the
     radio on.
4. Rotate the DIAL job to select "F8 CLONE".
5. Press the [F/W] key momentarily.
6. <b>After clicking OK</b>, hold the [PTT] switch
     for one second to send image.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [MONI] switch while turning the
     radio on.
4. Rotate the DIAL job to select "F8 CLONE".
5. Press the [F/W] key momentarily.
6. Press the [MONI] switch ("--RX--" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [moni] key while turning the radio on.
4. Select CLONE in menu, then press F. Radio restarts in clone mode.
     ("CLONE" will appear on the display).
5. <b>After clicking OK</b>, briefly hold [PTT] key to send image.
    ("-TX-" will appear on the LCD). 
 1. Turn radio off.
2. Connect cable to mic jack.
3. Press and hold in the [LOW(A/N)] key while turning the radio on.
4. <b>After clicking OK</b>, press the [MHz(SET)] key to send image.
 1. Turn radio off.
2. Connect cable to mic jack.
3. Press and hold in the [LOW(A/N)] key while turning the radio on.
4. Press the [D/MR(MW)] key ("--WAIT--" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to mic jack.
3. Press and hold in the [MHz], [LOW], and [D/MR] keys
   while turning the radio on.
4. <b>After clicking OK</b>, press the [MHz(SET)] key to send image.
 1. Turn radio off.
2. Connect cable to mic jack.
3. Press and hold in the [MHz], [LOW], and [D/MR] keys
   while turning the radio on.
4. Press the [D/MR(MW)] key ("--WAIT--" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Turn radio on (volume may need to be set at 100%).
5. Ensure that the radio is tuned to channel with no activity.
6. Click OK to download image from device.
 1. Turn radio off.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Turn radio on (volume may need to be set at 100%).
5. Ensure that the radio is tuned to channel with no activity.
6. Click OK to upload image to device.
 1. Turn radio off.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Turn radio on.
5. Ensure that the radio is tuned to channel with no activity.
6. Click OK to download image from device.
 1. Turn radio off.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Turn radio on.
5. Ensure that the radio is tuned to channel with no activity.
6. Click OK to upload image to device.
 1. Turn radio off.
2. Connect data cable.
3. Prepare radio for clone.
4. <b>After clicking OK</b>, press the key to send image.
 1. Turn radio off.
2. Connect data cable.
3. Prepare radio for clone.
4. Press the key to receive the image.
 1. Turn radio off.
2. Connect mic and hold [ACC] on mic while powering on.
    ("CLONE" will appear on the display)
3. Replace mic with PC programming cable.
4. <b>After clicking OK</b>, press the [SET] key to send image.
 1. Turn radio off.
2. Connect mic and hold [ACC] on mic while powering on.
    ("CLONE" will appear on the display)
3. Replace mic with PC programming cable.
4. Press the [DISP/SS] key
    ("R" will appear on the lower left of LCD).
 1. Turn radio off.
2. Remove front head.
3. Connect data cable to radio, use the same connector where
     head was connected to, <b>not the mic connector</b>.
4. Click OK.
 1. Turn radio off.
3. Press and hold in the [moni] key while turning the radio on.
4. Select CLONE in menu, then press F. Radio restarts in clone mode.
     ("CLONE" will appear on the display).
5. Press the [moni] key ("-RX-" will appear on the LCD).
 1. Turn radio on.
2. Connect cable to DATA terminal.
3. Unclip battery.
4. Press and hold in the [AMS] key and power key while clipping 
 in back battery the ("ADMS" will appear on the display).
5. Press the [MODE] key ("-WAIT-" will appear on the LCD).
<b>Then click OK</b> 1. Turn radio on.
2. Connect cable to DATA terminal.
3. Unclip battery.
4. Press and hold in the [AMS] key and power key while clipping 
 in back battery the("ADMS" will appear on the display).
5. <b>After clicking OK</b>, press the [BAND] key.
 1. Turn radio on.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Click OK to download image from device.

It will may not work if you turn on the radio with the cable already attached
 1. Turn radio on.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Click OK to upload the image to device.

It will may not work if you turn on the radio with the cable already attached A new CHIRP version is available. Please visit the website as soon as possible to download it! AM mode does not allow duplex or tone About About CHIRP Agree All All Files All supported formats| Always start with recent list Amateur An error has occurred Applying settings Automatic from system Available modules Bandplan Bands Banks Bin Browser Bug number not found Bug number: Bug subject: Building Radio Browser CHIRP must be restarted for the new selection to take effect Cached results can only be included for a proximity query with Latitude, Longitude, and Distance set. Please uncheck "%s" Canada Changing this setting requires refreshing the settings from the image, which will happen now. Channels with equivalent TX and RX %s are represented by tone mode of "%s" Chirp Image Files Choice Required Choose %s DTCS Code Choose %s Tone Choose Cross Mode Choose Diff Target Choose a recent model Choose duplex Choose the module to load from issue %i: City Click here for full license text Click on the "Special Channels" toggle-button of the memory
editor to see/set the EXT channels. P-VFO channels 100-109
are considered Settings.
Only a subset of the over 200 available radio settings
are supported in this release.
Ignore the beeps from the radio on upload and download.
 Clone completed, checking for spurious bytes Cloning Cloning from radio Cloning to radio Close Close String Close file Close string value with double-quote (") Cluster %i memory Cluster %i memories Comment Communicate with radio Complete Complex or non-standard tone squelch mode (starts the tone mode selection wizard) Connect your interface cable to the PC Port on the
back of the 'TX/RX' unit. NOT the Com Port on the head.
 Convert to FM Copy Country Cross Mode Custom Port Custom... Cut DTCS DTCS
Polarity DTMF decode DV Memory Danger Ahead Dec Delete Detailed information Developer Mode Developer state is now %s. CHIRP must be restarted to take effect Diff Raw Memories Diff against another editor Digital Code Digital Modes Disable reporting Disabled Distance Do not prompt again for %s Do you accept the risk? Double-click to change bank name Download Download from radio Download from radio... Download instructions Driver Driver information Driver messages Dual-mode digital repeaters that support analog will be shown as FM Duplex Edit details for %i memories Edit details for memory %i Enable Automatic Edits Enabled Enter Frequency Enter Offset (MHz) Enter TX Frequency (MHz) Enter a new name for bank %s: Enter custom port: Enter details about this update. Be descriptive about what you were doing, what you expected to happen, and what actually happened. Enter information about the bug including a short but meaningful subject and information about the radio model (if applicable) in question. In the next step you will have a chance to add more details about the problem. Enter information to add to the bug here Enter the bug number that should be updated Enter your username and password for chirpmyradio.com. If you do not have an account click below to create one before proceeding. Erased memory %s Error applying filter Error applying settings Error communicating with radio Example: "foo" Example: ( expression ) Example: 123 Example: 146.52 Example: AND, OR Example: freq<146.0,148.0> Example: name="myrepeater Example: name~"myrepea" Exclude private and closed repeaters Experimental driver Export can only write CSV files Export to CSV Export to CSV... Extra FM Radio FREE repeater database, which provides information
about repeaters in Europe. No account is required. FREE repeater database, which provides most up-to-date
information about repeaters in Europe. No account is
required. Failed to load radio browser Failed to parse result Failed to send bug report: Features File a new bug File does not exist: %s Files Files: Filter Filter results with location matching this string Filter... Find Find Next Find... Finish Float Finish float value like: 146.52 Finished radio job %s Follow these instructions to download the radio memory:
1 - Connect your interface cable
2 - Radio > Download from radio: DO NOT mess with the radio
during download!
3 - Disconnect your interface cable
 Follow these instructions to download the radio memory:
1 - Connect your interface cable
2 - Radio > Download from radio: Don't adjust any settings
on the radio head!
3 - Disconnect your interface cable
 Follow these instructions to download the radio memory:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio, volume @ 50%
4 - CHIRP Menu - Radio - Download from radio
 Follow these instructions to download your config:
1 - Turn off your radio
2 - Connect your interface cable to the Speaker-2 jack
3 - Turn on your radio
4 - Radio > Download from radio
5 - Disconnect the interface cable! Otherwise there will be
    no right-side audio!
 Follow these instructions to download your info:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the download of your radio data
 Follow these instructions to download your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio (unlock it if password protected)
4 - Click OK to start
 Follow these instructions to read your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Click OK to start
 Follow these instructions to upload the radio memory:
1 - Connect your interface cable
2 - Radio > Upload to radio: DO NOT mess with the radio
during upload!
3 - Disconnect your interface cable
 Follow these instructions to upload the radio memory:
1 - Connect your interface cable
2 - Radio > Upload to radio: Don't adjust any settings
on the radio head!
3 - Disconnect your interface cable
 Follow these instructions to upload the radio memory:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio, volume @ 50%
4 - CHIRP Menu - Radio - Upload to radio
 Follow these instructions to upload your config:
1 - Turn off your radio
2 - Connect your interface cable to the Speaker-2 jack
3 - Turn on your radio
4 - Radio > Upload to radio
5 - Disconnect the interface cable, otherwise there will be
    no right-side audio!
6 - Cycle power on the radio to exit clone mode
 Follow these instructions to upload your info:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the upload of your radio data
 Follow these instructions to upload your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio (unlock it if password protected)
4 - Click OK to start
 Follow these instructions to write your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Click OK to start
 Follow this instructions to download your info:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the download of your radio data
 Follow this instructions to read your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the download of your radio data
 Follow this instructions to upload your info:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the upload of your radio data
 Follow this instructions to write your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the upload of your radio data
 Found empty list value for %(name)s: %(value)r Frequency Frequency %s is out of supported range Frequency granularity in kHz Frequency in this range must not be AM mode Frequency in this range requires AM mode Frequency outside TX bands must be duplex=off GMRS Getting settings Goto Memory Goto Memory: Goto... Help Help Me... Hex Hide empty memories Honor the CTCSS/DCS receive squelch configuration when enabled, else only carrier squelch Human-readable comment (not stored in radio) If set, sort results by distance from these coordinates Import Import from file... Import messages Import not recommended Include Cached Index Info Information Insert Row Above Install desktop icon? Interact with driver Internal driver error Invalid %(value)s (use decimal degrees) Invalid Entry Invalid ZIP code Invalid edit: %s Invalid locator Invalid or unsupported module file Invalid value: %r Issue number: LIVE Language Latitude Length must be %i Limit Bands Limit Modes Limit Status Limit prefixes Limit results to this distance (km) from coordinates Limit use Live Radio Load Module... Load module from issue Load module from issue... Loading a module will not affect open tabs. It is recommended (unless instructed otherwise) to close all tabs before loading a module. Loading modules can be extremely dangerous, leading to damage to your computer, radio, or both. NEVER load a module from a source you do not trust, and especially not from anywhere other than the main CHIRP website (chirpmyradio.com). Loading a module from another source is akin to giving them direct access to your computer and everything on it! Proceed despite this risk? Loading settings Locator Login failed: Check your username and password Logo string 1 (12 characters) Logo string 2 (12 characters) Longitude Memories Memories are read-only due to unsupported firmware version Memory %i is not deletable Memory field name (one of %s) Memory label (stored in radio) Memory must be in a bank to be edited Memory {num} not in bank {bank} Mode Model Modes Module Module Loaded Module loaded successfully More Info More than one port found: %s Move Down Move Up Move operations are disabled while the view is sorted New Window New version available No empty rows below! No example for %s No modules found No modules found in issue %i No more space available; some memories were not applied No results No results! Number Offset Offset/
TX Freq One of: %s Only certain bands Only certain modes Only certain prefixes Only memory tabs may be exported Only working repeaters Open Open Recent Open Stock Config Open a file Open a module Open debug log Open in new window Open repeaters only Open stock config directory Operator Option Optional: -122.0000 Optional: 100 Optional: 20.0000 Optional: 45.0000 Optional: 52.0000 Optional: AA00 - AA00aa11 Optional: County, Hospital, etc. Overwrite memories? P-VFO channels 100-109 are considered Settings.
Only a subset of the over 130 available radio settings
are supported in this release.
 Parsing Password Paste Pasted memories will overwrite %s existing memories Pasted memories will overwrite memories %s Pasted memories will overwrite memory %s Pasted memory will overwrite memory %s Please be sure to quit CHIRP before installing the new version! Please follow this steps carefully:
1 - Turn on your radio
2 - Connect the interface cable to your radio
3 - Click the button on this window to start download
    (you may see another dialog, click ok)
4 - Radio will beep and led will flash
5 - You will get a 10 seconds timeout to press "MON" before
    data upload start
6 - If all goes right radio will beep at end.
After cloning remove the cable and power cycle your radio to
get into normal mode.
 Please follow this steps carefully:
1 - Turn on your radio
2 - Connect the interface cable to your radio.
3 - Click the button on this window to start download
    (Radio will beep and led will flash)
4 - Then press the "A" button in your radio to start cloning.
    (At the end radio will beep)
 Please note that developer mode is intended for use by developers of the CHIRP project, or under the direction of a developer. It enables behaviors and functions that can damage your computer and your radio if not used with EXTREME care. You have been warned! Proceed anyway? Please wait Plug in your cable and then click OK Polish repeaters database Port Power Prefixes Press enter to set this in memory Print Preview Printing Prolific USB device Properties Property Name Property Value QTH Locator Query %s Query Source Query string is invalid Query syntax OK Query syntax help Querying RX DTCS Radio Radio did not ack block %i Radio information Radio model: Radio sent data after the last awaited block, this happens when the selected model is a non-US but the radio is a US one. Please choose the correct model and try again. RadioReference Canada requires a login before you can query RadioReference.com is the world's largest
radio communications data provider
<small>Premium account required</small> Receive DTCS code Receive frequency Recent Recent... Recommend using 55.2 Refresh required Refreshed memory %s Reload Driver Reload Driver and File Remove Remove selected model from list Rename bank RepeaterBook is Amateur Radio's most comprehensive,
worldwide, FREE repeater directory. Report or update a bug... Reporting a new bug: %r Reporting enabled Reporting helps the CHIRP project know which radio models and OS platforms to spend our limited efforts on. We would really appreciate if you left it enabled. Really disable reporting? Restart Required Restore %i tab Restore %i tabs Restore tabs on start Results from other areas Retrieved settings Save Save before closing? Save file Saved settings Scan control (skip, include, priority, etc) Scanlists Scrambler Security Risk Select Bandplan... Select Bands Select Language Select Modes Select a bandplan Select a tab and memory to diff Select prefixes Service Settings Settings are read-only due to unsupported firmware version Shift amount (or transmit frequency) controlled by duplex Show Raw Memory Show debug log location Show extra fields Show image backup location Skip Some memories are incompatible with this radio Some memories are not deletable Sort %i memory Sort %i memories Sort %i memory ascending Sort %i memories ascending Sort %i memory descending Sort %i memories descending Sort by column: Sort memories Sorting State State/Province Success Successfully sent bug report: TX-RX DTCS polarity (normal or reversed) Tested and mostly works, but may give you issues
when using lesser common radio variants.
Proceed at your own risk, and let us know about issues! The DMR-MARC Worldwide Network The FT-450D radio driver loads the 'Special Channels' tab
with the PMS scanning range memories (group 11), 60meter
channels (group 12), the QMB (STO/RCL) memory, the HF and
50m HOME memories and all the A and B VFO memories.
There are VFO memories for the last frequency dialed in
each band. The last mem-tune config is also stored.
These Special Channels allow limited field editing.
This driver also populates the 'Other' tab in the channel
memory Properties window. This tab contains values for
those channel memory settings that don't fall under the
standard Chirp display columns. With FT450 support, the gui now
uses the mode 'DIG' to represent data modes and a new column
'DATA MODE' to select USER-U, USER-L and RTTY 
 The X3Plus driver is currently experimental.
There are no known issues but you should proceed with caution.
Please save an unedited copy of your first successful
download to a CHIRP Radio Images (*.img) file.
 The author of this module is not a recognized CHIRP developer. It is recommended that you not load this module as it could pose a security risk. Proceed anyway? The debug log file is not available when CHIRP is run interactively from the command-line. Thus, this tool will not upload what you expect. It is recommended that you quit now and run CHIRP non-interactively (or with stdin redirected to /dev/null) The following information will be submitted: The recommended procedure for importing memories is to open the source file and copy/paste memories from it into your target image. If you continue with this import function, CHIRP will replace all memories in your currently-open file with those in %(file)s. Would you like to open this file to copy/paste memories across, or proceed with the import? This Memory This driver has been tested with v3 of the ID-5100. If your radio is not fully updated please help by opening a bug report with a debug log so we can add support for the other revisions. This driver is in development and should be considered as experimental. This image is missing firmware information. It may have been generated with an old or modified version of CHIRP. It is advised that you download a fresh image from your radio and use that going forward for the best safety and compatibility. This is a live-mode radio, which means changes are sent to the radio in real-time as you make them. Upload is not necessary! This is a radio-independent file and cannot be uploaded directly to a radio. Open a radio image (or download one from a radio) and then copy/paste items from this tab into that one in order to upload This is an early stage beta driver
 This is an early stage beta driver - upload at your own risk
 This is an experimental driver for the Quansheng UV-K5. It may harm your radio, or worse. Use at your own risk.

Before attempting to do any changes please download the memory image from the radio with chirp and keep it. This can be later used to recover the original settings. 

some details are not yet implemented This is experimental support for BJ-9900 which is still under development.
Please ensure you have a good backup with OEM software.
Also please send in bug and enhancement requests!
You have been warned. Proceed at your own risk! This is the ticket number for an already-created issue on the chirpmyradio.com website This memory and shift all up This memory and shift block up This option may break your radio! Each radio has a unique set of calibration data and uploading the data from the image will cause physical harm to the radio if it is from a different piece of hardware. Do not use this unless you know what you are doing and accept the risk of destroying your radio! This radio has a tricky way of enter into program mode,
even the original software has a few tries to get inside.
I will try 8 times (most of the time ~3 will doit) and this
can take a few seconds, if don't work, try again a few times.
If you can get into it, please check the radio and cable.
 This should only be enabled if you are using modified firmware that supports wider frequency coverage. Enabling this will cause CHIRP not to enforce OEM restrictions and may lead to undefined or unregulated behavior. Use at your own risk! This tool will upload details about your system to an existing issue on the CHIRP tracker. It requires your username and password for chirpmyradio.com in order to work. Information about your system, including your debug log, config file, and any open image files will be uploaded. An attempt will be made to redact any personal information before it leaves your system. This will load a module from a website issue Tone Tone Mode Tone Squelch Tone squelch mode Transmit Power Transmit shift, split mode, or transmit inhibit Transmit tone Transmit/receive DTCS code for DTCS mode, else transmit code Transmit/receive modulation (FM, AM, SSB, etc) Transmit/receive tone for TSQL mode, else receive tone Tuning Step Type a simple search string or a formatted query and press enter USB Port Finder Unable to determine port for your cable. Check your drivers and connections. Unable to edit memory before radio is loaded Unable to find stock config %r Unable to import while the view is sorted Unable to open the clipboard Unable to query Unable to read last block. This often happens when the selected model is US but the radio is a non-US one (or widebanded). Please choose the correct model and try again. Unable to reveal %s on this system Unable to set %s on this memory Unable to upload this file United States Unplug your cable (if needed) and then click OK Unsupported model %r Update an existing bug Updating bug %s Upload instructions Upload is disabled due to unsupported firmware version Upload to radio Upload to radio... Uploaded memory %s Use fixed-width font Use larger font Username Value does not fit in %i bits Value must be at least %.4f Value must be at most %.4f Value must be exactly %i decimal digits Value must be zero or greater Value to search memory field for Values Vendor View WARNING! Warning Warning: %s Welcome With this option checked, a proximity search will include cached results from other localities if they are in range, match other filter parameters, and have been downloaded before. Would you like CHIRP to install a desktop icon for you? You have opened multiple issues within the last week. CHIRP limits the number of issues you can open to avoid abuse. If you really need to open another, please do so via the website. Your Prolific-based USB device will not work without reverting to an older version of the driver. Visit the CHIRP website to read more about how to resolve this? Your QTH Locator Your cable appears to be on port: bits bytes bytes each disabled enabled {bank} is full Project-Id-Version: CHIRP
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-05 01:25-0300
Last-Translator: MELERIX
Language-Team: 
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.5
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: ..
X-Poedit-SearchPath-0: .
 %(value)s debe estar entre %(min)i y %(max)i %i Memoria y desplazar todo hacia arriba %i Memorias y desplazar todo hacia arriba %i Memoria %i Memorias %i Memoria y desplazar bloque hacia arriba %i Memorias y desplazar bloque hacia arriba %s no ha sido guardado. ¿Guardar antes de cerrar? (Describe lo que ocurrió en cambio) (Describe lo que esperabas que ocurriera) (Describe lo que estabas haciendo) (¿Ha funcionado esto antes? ¿Radio nueva? ¿Funciona con el software OEM?) (nada) ...y %i más 1. Conecta el cable de programación a la toma MIC.
2. Presiona Aceptar. 1. Asegúrate de que tu versión de firmware es 4_10 o superior
2. Apaga la radio
3. Conecta tu cable de interfaz
4. Enciende la radio
5. Presiona y suelta PTT 3 veces mientras mantienes presionada la tecla MONI
6. Tasas de baudios admitidas: 57600 (predeterminado) y 19200
   (gira el dial mientras mantienes presionado MONI para cambiar)
7. Cliquea Aceptar
 1. Apaga la radio.
2. Conecta el cable de datos.
3. Mientras mantienes presionado el botón "A/N LOW", enciende la radio.
4. <b>Después de cliquear Aceptar</b>, presiona "SET MHz" para enviar imagen.
 1. Apaga la radio.
2. Conecta el cable de datos.
3. Mientras mantienes presionado el botón "A/N LOW", enciende la radio.
4. Presiona "MW D/MR" para recibir la imagen.
5. Asegúrate de que la pantalla dice "-WAIT-" (mira la nota abajo si no)
6. Cliquea Aceptar para descartar este dialogo e iniciar la transferencia.
Nota: si no ves "-WAIT-" en el paso 5, intenta apagar y
      encender y manteniendo presionado el botón rojo "*L" para desbloquear
      la radio, entonces vuelve a iniciar en el paso 1.
 1. Apaga la radio.
2. Conecta el cable de datos.
3. Mientras mantienes presionado el botón "F/W", enciende la radio.
4. <b>Después de cliquear Aceptar</b>, presiona "RPT" para enviar imagen.
 1. Apaga la radio.
2. Conecta el cable de datos.
3. Mientras mantienes presionado el botón "F/W", enciende la radio.
4. Presiona "REV" para recibir la imagen.
6. Cliquea Aceptar para iniciar la transferencia.
 1. Apaga la radio.
2. Conecta el cable de datos.
3. Mientras mantienes presionados los botones "TONE" y "REV", enciende la radio.
4. <b>Después de cliquear Aceptar</b>, presiona "TONE" para enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable de datos.
3. Mientras mantienes presionados los botones "TONE" y "REV", enciende la radio.
4. Presiona "REV" para recibir la imagen.
5. Asegúrate de que la pantalla diga "CLONE RX" y el led verde esté parpadeando
6. Cliquea Aceptar para iniciar la transferencia.
 1. Apaga la radio.
2. Conecta el cable
3. Presiona y mantén las teclas MHz, Low, y D/MR en la radio mientras la enciendes
4. La radio estará en modo clonación cuando TX/RX esté parpadeando
5. <b>Después de cliquear Aceptar</b>, presiona la tecla MHz en la radio para enviar la imagen.
    ("TX" aparecerá en la LCD). 
 1. Apaga la radio.
2. Conecta el cable
3. Presiona y mantén las teclas MHz, Low, y D/MR en la radio mientras la enciendes
4. La radio estará en modo clonación cuando TX/RX esté parpadeando
5. Presiona la tecla Low en la radio ("RX" aparecerá en la LCD).
6. Cliquea Aceptar. 1. Apaga la radio.
2. Conecta el cable al conector ACC.
3. Presiona y mantén las teclas [MODE &lt;] y [MODE &gt;] mientras
     enciendes la radio ("CLONE MODE" aparecerá en la
     pantalla).
4. <b>Después de cliquear Aceptar</b> aquí, presiona la tecla [C.S.] para
    enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable al conector ACC.
3. Presiona y mantén las teclas [MODE &lt;] y [MODE &gt;] mientras
     enciendes la radio ("CLONE MODE" aparecerá en la
     pantalla).
4. <b>Después de cliquear Aceptar</b> aquí, presiona la tecla [A] para
    enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable al conector ACC.
3. Presiona y mantén las teclas [MODE &lt;] y [MODE &gt;] mientras
     enciendes la radio ("CLONE MODE" aparecerá en la
     pantalla).
4. Cliquea Aceptar aquí.
    ("Receiving" aparecerá en la LCD).
 1. Apaga la radio.
2. Conecta el cable al conector ACC.
3. Presiona y mantén las teclas [MODE &lt;] y [MODE &gt;] mientras
     enciendes la radio ("CLONE MODE" aparecerá en la
     pantalla).
4. Presiona la tecla [A](RCV) ("receiving" aparecerá en la LCD).
 1. Apaga la radio.
2. Conecta el cable al conector ACC.
3. Presiona y mantén las teclas [MODE &lt;] y [MODE &gt;] mientras
     enciendes la radio ("CLONE MODE" aparecerá en la
     pantalla).
4. Presiona la tecla [C] ("RX" aparecerá en la LCD).
 1. Apaga la radio.
2. Conecta el cable al conector CAT/LINEAR.
3. Presiona y mantén las teclas [MODE &lt;] y [MODE &gt;] mientras
     enciendes la radio ("CLONE MODE" aparecerá en la
     pantalla).
4. <b>Después de cliquear Aceptar</b>,
     presiona la tecla [C](SEND) para enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable al conector DATA.
3. Presiona y mantén la tecla "izquierda" [V/M] mientras enciendes la
     radio.
4. Gira la perilla "derecha" DIAL para seleccionar "CLONE START".
5. Presiona la tecla [SET]. La pantalla desaparecerá
     por un momento, entonces la notación "CLONE" aparecerá.
6. <b>Después de cliquear Aceptar</b>, presiona la tecla "izquierda" [V/M] para
     enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable al conector DATA.
3. Presiona y mantén la tecla "izquierda" [V/M] mientras enciendes la
     radio.
4. Gira la perilla "derecha" DIAL para seleccionar "CLONE START".
5. Presiona la tecla [SET]. La pantalla desaparecerá
     por un momento, entonces la notación "CLONE" aparecerá.
6. Presiona la tecla "izquierda" [LOW] ("CLONE -RX-" aparecerá en
     la pantalla).
 1. Apaga la radio.
2. Conecta el cable al terminal DATA.
3. Presiona y mantén la tecla [FW] mientras enciendes la radio
     ("CLONE" aparecerá en la pantalla).
4. <b>Después de cliquear Aceptar</b>, presiona la tecla [BAND] para enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable al conector DATA.
3. Presiona y mantén la tecla [FW] mientras enciendes la radio
     ("CLONE" aparecerá en la pantalla).
4. Presiona la tecla [MODE] ("-WAIT-" aparecerá en la LCD).
 1. Apaga la radio.
2. Conecta el cable al conector DATA.
3. Presiona y mantén la tecla [MHz(PRI)] mientras enciendes la
     radio.
4. Gira la perilla DIAL para seleccionar "F-7 CLONE".
5. Presiona y mantén la tecla [BAND(SET)]. La pantalla
     desaparecerá por un momento, entonces la notación "CLONE"
     aparecerá.
6. Presiona la tecla [LOW(ACC)] ("--RX--" aparecerá en la pantalla).
 1. Apaga la radio.
2. Conecta el cable al conector DATA.
3. Presiona y mantén la tecla [MHz(PRI)] mientras enciendes la
 radio.
4. Gira la perilla DIAL para seleccionar "F-7 CLONE".
5. Presiona y mantén la tecla [BAND(SET)]. La pantalla
 desaparecerá por un momento, entonces la notación "CLONE"
 aparecerá.
6. <b>Después de cliquear Aceptar</b>, presiona la tecla [V/M(MW)] para enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable al terminal DATA.
3. Presiona y mantén la tecla [DISP] mientras enciendes la radio
     ("CLONE" aparecerá en la LCD de la radio).
4. <b>Después de cliquear Aceptar aquí en Chirp</b>,
     presiona el botón [Send] de la pantalla.
 1. Apaga la radio.
2. Conecta el cable al terminal DATA.
3. Presiona y mantén la tecla [DISP] mientras enciendes la radio
     ("CLONE" aparecerá en la LCD de la radio).
4. Presiona el botón [RECEIVE] de la pantalla
     ("-WAIT-" aparecerá en la LCD de la radio).
5. Finalmente, presiona el botón Aceptar abajo.
 1. Apaga la radio.
2. Conecta el cable al terminal DATA.
3. Presiona y mantén la tecla [F] mientras enciendes la radio
     ("CLONE" aparecerá en la pantalla).
4. <b>Después de cliquear Aceptar</b>, presiona la tecla [BAND] para enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable al terminal DATA.
3. Presiona y mantén la tecla [F] mientras enciendes la radio
     ("CLONE" aparecerá en la pantalla).
4. Presiona la tecla [Dx] ("-WAIT-" aparecerá en la LCD).
 1. Apaga la radio.
2. Conecta el cable al terminal DATA.
3. Presiona y mantén la tecla [MHz(SETUP)] mientras enciendes la radio
     ("CLONE" aparecerá en la pantalla).
4. <b>Después de cliquear Aceptar</b>, presiona la tecla [REV(DW)]
     para enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable al terminal DATA.
3. Presiona y mantén la tecla [MHz(SETUP)] mientras enciendes la radio
     ("CLONE" aparecerá en la pantalla).
4. Presiona la tecla [MHz(SETUP)]
     ("-WAIT-" aparecerá en la LCD).
 1. Apaga la radio.
2. Conecta el cable al conector MIC.
3. Presiona y mantén la tecla [MHz(SETUP)] mientras enciendes la radio
     ("CLONE" aparecerá en la pantalla).
4. <b>Después de cliquear Aceptar</b>, presiona la tecla [GM(AMS)]
     para enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable la conector MIC.
3. Presiona y mantén la tecla [MHz(SETUP)] mientras enciendes la radio
     ("CLONE" aparecerá en la pantalla).
4. Presiona la tecla [MHz(SETUP)]
     ("-WAIT-" aparecerá en la LCD).
 1. Apaga la radio.
2. Conecta el cable al conector MIC/EAR.
3. Presiona y mantén la tecla [F/W] mientras enciendes la radio
    ("CLONE" aparecerá en la pantalla).
4. <b>Después de cliquear Aceptar</b>, presiona la tecla [VFO(DW)SC] para recibir
    la imagen desde la radio.
 1. Apaga la radio.
2. Conecta el cable al conector MIC/EAR.
3. Presiona y mantén la tecla [F/W] mientras enciendes la radio
    ("CLONE" aparecerá en la pantalla).
4. Presiona la tecla [MR(SKP)SC] ("CLONE WAIT" aparecerá
    en la LCD).
5. Cliquea Aceptar para enviar la imagen a la radio.
 1. Apaga la radio.
2. Conecta el cable al conector MIC/SP.
3. Presiona y mantén la perilla [PTT] mientras enciendes la
     radio.
4. <b>Después de cliquear Aceptar</b>, presiona el interruptor [PTT] para enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable al conector MIC/SP.
3. Presiona y mantén la perilla [PTT] mientras enciendes la
     radio.
4. Presiona el interruptor [MONI] ("WAIT" aparecerá en la LCD).
5. Presiona Aceptar.
 1. Apaga la radio.
2. Conecta el cable al conector MIC/SP.
3. Presiona y mantén la tecla [F/W] mientras enciendes la radio
     ("CLONE" aparecerá en la pantalla).
4. <b>Después de cliquear Aceptar</b>, presiona la tecla [BAND] para enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable al conector MIC/SP.
3. Presiona y mantén la tecla [F/W] mientras enciendes la radio
     ("CLONE" aparecerá en pantalla).
4. Presiona la tecla [V/M] ("-WAIT-" aparecerá en la LCD).
 1. Apaga la radio.
2. Conecta el cable al conector MIC/SP.
3. Presiona y mantén la tecla [MON-F] mientras enciendes la radio
     ("CLONE" aparecerá en la pantalla).
4. <b>Después de cliquear Aceptar</b>, presiona la tecla [BAND] para enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable al conector MIC/SP.
3. Presiona y mantén la tecla [MON-F] mientras enciendes la radio
     ("CLONE" aparecerá en la pantalla).
4. Presiona la tecla [V/M] ("CLONE WAIT" aparecerá en la LCD).
 1. Apaga la radio.
2. Conecta el cable al conector MIC/SP.
3. Presiona y mantén el interruptor [MONI] mientras enciendes la
     radio.
4. Gira el DIAL para seleccionar "F8 CLONE".
5. Presiona la tecla [F/W] momentáneamente.
6. <b>Después de cliquear Aceptar</b>, mantén presionado el interruptor [PTT]
     por un segundo para enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable al conector MIC/SP.
3. Presiona y mantén el interruptor [MONI] mientras enciendes la
     radio.
4. Gira el DIAL para seleccionar "F8 CLONE".
5. Presiona la tecla [F/W] momentáneamente.
6. Presiona el interruptor [MONI] ("--RX--" aparecerá en la LCD).
 1. Apaga la radio.
2. Conecta el cable al conector MIC/SP.
3. Presiona y mantén la tecla [moni] mientras enciendes la radio.
4. Selecciona CLONE en el menú, entonces presiona F. La radio se reinicia en modo clonación.
     ("CLONE" aparecerá en la pantalla).
5. <b>Después de cliquear Aceptar</b>, mantén presionada brevemente la tecla [PTT] para enviar la imagen.
    ("-TX-" aparecerá en la pantalla). 
 1. Apaga la radio.
2. Conecta el cable al conector de micrófono.
3. Presiona y mantén la tecla [LOW(A/N)] mientras enciendes la radio.
4. <b>Después de cliquear Aceptar</b>, presiona la tecla [MHz(SET)] para enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable al conector de micrófono.
3. Presiona y mantén la tecla [LOW(A/N)] mientras enciendes la radio.
4. Presiona la tecla [D/MR(MW)] ("--WAIT--" aparecerá en la LCD).
 1. Apaga la radio.
2. Conecta el cable al conector de micrófono.
3. Presiona y mantén las teclas [MHz], [LOW] y [D/MR]
   mientras enciendes la radio.
4. <b>Después de cliquear Aceptar</b>, presiona la tecla [MHz(SET)] para enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable al conector de micrófono.
3. Presiona y mantén las teclas [MHz], [LOW] y [D/MR]
   mientras enciendes la radio.
4. Presiona la tecla [D/MR(MW)] ("--WAIT--" aparecerá en la LCD).
 1. Apaga la radio.
2. Conecta el cable al conector mic/spkr.
3. Asegúrate de que el conector esté firmemente conectado.
4. Enciende la radio (es posible que el volumen necesite establecerse al 100%).
5. Asegúrate de que la radio esté sintonizada a un canal sin actividad.
6. Cliquea Aceptar para descargar la imagen desde el dispositivo.
 1. Apaga la radio.
2. Conecta el cable al conector mic/spkr.
3. Asegúrate de que el conector esté firmemente conectado.
4. Enciende la radio (es posible que el volumen necesite establecerse al 100%).
5. Asegúrate de que la radio esté sintonizada a un canal sin actividad.
6. Cliquea Aceptar para cargar la imagen al dispositivo.
 1. Apaga la radio.
2. Conecta el cable al conector mic/spkr.
3. Asegúrate de que el conector esté firmemente conectado.
4. Enciende la radio.
5. Asegúrate de que la radio esté sintonizada a un canal sin actividad.
6. Cliquea Aceptar para descargar la imagen desde dispositivo.
 1. Apaga la radio.
2. Conecta el cable al conector mic/spkr.
3. Asegúrate de que el conector esté firmemente conectado.
4. Enciende la radio.
5. Asegúrate de que la radio esté sintonizada a un canal sin actividad.
6. Cliquea Aceptar para cargar la imagen al dispositivo.
 1. Apaga la radio.
2. Conecta el cable de datos.
3. Prepara la radio para clonar.
4. <b>Después de cliquear Aceptar</b>, presiona la tecla para enviar la imagen.
 1. Apaga la radio.
2. Conecta el cable de datos.
3. Prepara la radio para clonar.
4. Presiona la tecla para recibir la imagen.
 1. Apaga la radio.
2. Conecta el micrófono y mantén presionado [ACC] en el micrófono mientras lo enciendes.
    ("CLONE" aparecerá en la pantalla)
3. Remplaza el micrófono con el cable de programación de PC.
4. <b>Después de cliquear Aceptar</b>, presiona la tecla [SET] para enviar la imagen.
 1. Apaga la radio.
2. Conecta micrófono y mantén presionado [ACC] en micrófono mientras enciendes.
    ("CLONE" aparecerá en la pantalla)
3. Remplaza micrófono con el cable de programación de PC.
4. Presiona la tecla [DISP/SS]
    ("R" aparecerá en la parte inferior izquierda de la LCD).
 1. Apaga la radio.
2. Remueve la cabeza frontal.
3. Conecta el cable de datos a la radio, usa el mismo conector en donde
     la cabeza estaba conectada, <b>no el conector de micrófono</b>.
4. Cliquea Aceptar.
 1. Apaga la radio.
3. Presiona y mantén la tecla [moni] mientras enciendes la radio.
4. Selecciona CLONE en el menú, entonces presiona F. La radio se reiniciara en modo clonación.
     ("CLONE" aparecerá en la pantalla).
5. Presiona la tecla [moni] ("-RX-" aparecerá en la LCD).
 1. Enciende la radio.
2. Conecta el cable al terminal DATA.
3. Desengancha la batería.
4. Presiona y mantén la tecla [AMS] y la tecla de encendido mientras enganchas 
 de vuelta la batería ("ADMS" aparecerá en la pantalla).
5. Presiona la tecla [MODE] ("-WAIT-" aparecerá en la LCD).
</b>Entonces cliquea Aceptar<b> 1. Enciende la radio.
2. Conecta el cable al terminal DATA.
3. Desengancha la batería.
4. Presiona y mantén la tecla [AMS] y la tecla de encendido mientras enganchas 
 de vuelta la batería ("ADMS" aparecerá en la pantalla).
5. <b>Después de cliquear Aceptar</b>, presiona la tecla [BAND].
 1. Enciende la radio.
2. Conecta el cable al conector mic/spkr.
3. Asegúrate de que el conector esté firmemente conectado.
4. Cliquea Aceptar para descargar la imagen desde el dispositivo.

Esto podría no funcionar si enciendes la radio con el cable ya conectado
 1. Enciende la radio.
2. Conecta el cable al conector mic/spkr.
3. Asegúrate de que el conector esté firmemente conectado.
4. Cliquea Aceptar para cargar la imagen al dispositivo.

Esto podría no funcionar si enciendes la radio con el cable ya conectado Una nueva versión de CHIRP está disponible, ¡Por favor visita el sitio web tan pronto como sea posible para descargarla! El modo AM no permite dúplex o tono Acerca de Acerca de CHIRP De acuerdo Todo Todos los archivos Todos los formatos soportados| Siempre iniciar con la lista reciente Aficionado Ha ocurrido un error Aplicando ajustes Automático del sistema Módulos disponibles Plan de banda Bandas Bancos Binario Navegador Número de error no encontrado Número de error: Asunto del error: Construyendo navegador de radio CHIRP debe ser reiniciado para que la nueva selección tome efecto Los resultados en caché solo pueden ser incluidos para una consulta de proximidad con latitud, longitud y distancia establecidas. Por favor desmarca "%s" Canada Cambiar este ajuste requiere actualizar los ajustes desde la imagen, lo cual ocurrirá ahora. Canales con TX y RX equivalentes %s son representados por modo de tono de "%s" Archivos de imagen Chirp Elección requerida Elegir %s código DTCS Elegir %s tono Elegir modo cruzado Elige el objetivo de diferencia Elegir un modelo reciente Elegir Dúplex Elige el módulo para cargar desde problema %i: Ciudad Cliquea aquí para el texto de licencia completo Cliquea en el botón para alternar "Canales Especiales" del editor de memoria
para ver/establecer los canales EXT. Los canales P-VFO 100-109
se consideran Ajustes.
Sólo un subconjunto de las más de 200 ajustes de radio disponibles
están soportados en este lanzamiento.
Ignora los pitidos de la radio al cargar y descargar.
 Clonado completado, comprobando por bytes espurios Clonando Clonando desde radio Clonando a radio Cerrar Cerrar cadena Cerrar archivo Cerrar el valor de la cadena con comillas dobles (") Agrupar %i memoria Agrupar %i memorias Comentario Comunicarse con la radio Completado Modo de silenciador de tono complejo o no estándar (inicia el asistente de selección del modo de tono) Conecta tu cable de interfaz al Puerto de PC en la
espalda de la unidad 'TX/RX'. NO el Puerto Com en la cabeza.
 Convertir a FM Copiar País Modo cruzado Puerto personalizado Personalizado... Cortar DTCS Polaridad
DTCS Decodificación DTMF Memoria DV Peligro adelante Decimal Borrar Información detallada Modo desarrollador Estado de desarrollador ahora está %s. CHIRP debe ser reiniciado para que tome efecto Diferenciar memorias en bruto Diferenciar contra otro editor Código digital Modos digitales Deshabilitar reportes Desahbilitado Distancia No preguntar de nuevo para %s ¿Aceptas el riesgo? Doble clic para cambiar nombre de banco Descargar Descargar desde radio Descargar desde radio... Descargar instrucciones Controlador Información del controlador Mensajes del controlador Los repetidores digitales de modo dual que soportan análogo se mostrarán como FM Dúplex Editar detalles para %i memorias Editar detalles para memoria %i Habilitar ediciones automáticas Habilitado Ingresar frecuencia Ingresar desplazamiento (MHz) Ingresar frecuencia TX (MHz) Ingresa un nuevo nombre para el banco %s: Ingresar puerto personalizado: Ingresa detalles sobre esta actualización. Se descriptivo sobre lo que estabas haciendo, lo que esperabas que ocurriera, y lo que realmente ocurrió. Ingresa información sobre el error incluyendo un asunto breve pero significativo e información sobre el modelo de radio (si es aplicable) en cuestión. En el siguiente paso tendrás la oportunidad de agregar más detalles sobre el problema. Ingresa la información para agregar al error aquí Ingresa el numero de error que debe ser actualizado Ingresar tu nombre de usuario y contraseña para chirpmyradio.com. Si no tienes una cuenta cliquea debajo para crear una antes de proceder. Memoria borrada %s Error aplicando filtro Error aplicando ajustes Error comunicándose con la radio Ejemplo: "foo" Ejemplo: ( expresión ) Ejemplo: 123 Ejemplo: 146.52 Ejemplo: Y, O Ejemplo: frec<146.0,148.0> Ejemplo: nombre="mirepetidor Ejemplo: nombre~"mirepe" Excluir repetidores privados y cerrados Controlador experimental Exportar sólo puede escribir archivos CSV Exportar a CSV Exportar a CSV... Extra Radio FM Base de datos de repetidores GRATUITA que proporciona información
sobre repetidores en Europa. No es necesario tener una cuenta. Base de datos de repetidores GRATUITA, la cual proporciona la
información más actualizada sobre repetidores en Europa. No requiere
cuenta. Fallo al cargar navegador de radio Fallo al analizar el resultado Fallo al enviar el reporte de error: Caracteristicas Archivar un nuevo error El archivo no existe: %s Archivos Archivos: Filtrar Filtrar resultados con ubicación coincidiendo esta cadena Filtrar... Buscar Buscar siguiente Buscar... Finalizar flotante Finalizar valor flotante como: 146.52 Trabajos de radio terminados %s Sigue estas instrucciones para descargar la memoria de la radio:
1 - Conecta tu cable de interfaz
2 - Radio > Descargar desde radio: ¡NO te metas con la radio
durante la descarga!
3 - Desconecta tu cable de interfaz
 Sigue estas instrucciones para descargar la memoria de la radio:
1 - Conecta tu cable de interfaz
2 - Radio > Descargar desde radio: ¡No ajustes ningún ajuste
en la cabeza de la radio!
3 - Desconecta tu cable de interfaz
 Sigue estas instrucciones para descargar la memoria de la radio:
1 - Apaga tu radio
2 - Conecta tu cable de interfaz
3 - Enciende tu radio, volumen @ 50%
4 - Menú de CHIRP - Radio > Descargar desde radio
 Sigue estas instrucciones para descargar tu configuración:
1 - Apaga tu radio
2 - Conecta tu cable de interfaz al conector Speaker-2
3 - Enciende tu radio
4 - Radio > Descargar desde radio
5 - ¡Desconecta el cable de interfaz! ¡De lo contrario no habrá
    audio del lado derecho!
 Sigue estas instrucciones para descargar tu información:
1 - Apaga tu radio
2 - Conecta tu cable de interfaz
3 - Enciende tu radio
4 - Haz la descarga de tus datos de radio
 Sigue estas instrucciones para descargar tu radio:
1 - Apaga tu radio
2 - Conecta tu cable de interfaz
3 - Enciende tu radio (desbloquéala si está protegida por contraseña)
4 - Cliquea Aceptar para iniciar
 Sigue estas instrucciones para leer tu radio:
1 - Apaga tu radio
2 - Conecta tu cable de interfaz
3 - Enciende tu radio
4 - Cliquea Aceptar para iniciar
 Sigue estas instrucciones para cargar la memoria de la radio:
1 - Conecta tu cable de interfaz
2 - Radio > Cargar a radio: ¡NO te metas con la radio
durante la carga!
3 - Desconecta tu cable de interfaz
 Sigue estas instrucciones para cargar la memoria de la radio:
1 - Conecta tu cable de interfaz
2 - Radio > Cargar a radio: ¡No ajustes ningún ajuste
en la cabeza de la radio!
3 - Desconecta tu cable de interfaz
 Sigue estas instrucciones para cargar la memoria de la radio:
1 - Apaga tu radio
2 - Conecta tu cable de interfaz
3 - Enciende tu radio, volumen @ 50%
4 - Menú de CHIRP - Radio > Cargar a radio
 Sigue estas instrucciones para cargar tu configuración:
1 - Apaga tu radio
2 - Conecta tu cable de interfaz al conector Speaker-2
3 - Enciende tu radio
4 - Radio > Cargar a radio
5 - ¡Desconecta el cable de interfaz, de otro modo no habrá
    audio del lado derecho!
6 - Apaga y enciende la radio para salir del modo de clonación
 Sigue estas instrucciones para cargar tu información:
1 - Apaga tu radio
2 - Conecta tu cable de interfaz
3 - Enciende tu radio
4 - Haz la carga de tus datos de radio
 Sigue estas instrucciones para cargar tu radio:
1 - Apaga tu radio
2 - Conecta tu cable de interfaz
3 - Enciende tu radio (desbloquéala si está protegida por contraseña)
4 - Cliquea Aceptar para iniciar
 Sigue estas instrucciones para escribir tu radio:
1 - Apaga tu radio
2 - Conecta tu cable de interfaz
3 - Enciende tu radio
4 - Cliquea Aceptar para iniciar
 Sigue estas instrucciones para descargar tu información:
1 - Apaga tu radio
2 - Conecta tu cable de interfaz
3 - Enciende tu radio
4 - Haz la descarga de tus datos de radio
 Sigue estas instrucciones para leer tu radio:
1 - Apaga tu radio
2 - Conecta tu cable de interfaz
3 - Enciende tu radio
4 - Haz la descarga de tus datos de radio
 Sigue estas instrucciones para cargar tu información:
1 - Apaga tu radio
2 - Conecta cable de interfaz
3 - Enciende tu radio
4 - Haz la carga de tus datos de radio
 Sigue estas instrucciones para escribir tu radio:
1 - Apaga tu radio
2 - Conecta tu cable de interfaz
3 - Enciende tu radio
4 - Haz la carga de tus datos de radio
 Encontrado valor de lista vacía para %(name)s: %(value)r Frecuencia La frecuencia %s está fuera del rango soportado Granularidad de frecuencia en kHz La frecuencia en este rango no debe ser modo AM La frecuencia en este rango requiere modo AM La frecuencia fuera de las bandas TX debe ser dúplex=off GMRS Obteniendo ajustes Ir a memoria Ir a memoria: Ir a... Ayuda Ayúdame... Hexadecimal Ocultar memorias vacías Respeta la configuración del silenciador de recepción CTCSS/DCS cuando está activada, de lo contrario sólo el silenciador de portadora Comentario legible para humanos (no almacenado en la radio) Si se establece, ordenar los resultados por distancia desde estas coordenadas Importar Importar desde archivo... Importar mensajes Importación no recomendada Incluir en caché Índice Información Información Insertar fila arriba ¿Instalar icono de escritorio? Interactuar con controlador Error interno del controlador %(value)s inválido (usa grados decimales) Entrada inválida Código postal inválido Edición inválida: %s Localizador inválido Archivo de módulo inválido o no soportado Valor inválido: %r Número de problema: EN VIVO Lenguaje Latitud Largo debe ser %i Bandas límite Modos límite Estado límite Limitar prefijos Limitar resultados a esta distancia (km) desde coordenadas Limitar uso Radio en vivo Cargar módulo... Cargar módulo desde problema Cargar módulo desde problema... Cargar un módulo no afectará las pestañas abiertas. Se recomienda (a menos que se indique lo contrario) cerrar todas las pestañas antes de cargar un módulo. Cargar módulos puede ser extremadamente peligroso, pudiendo dañar tu computador, radio o ambos. NUNCA cargues un módulo desde una fuente que no confíes, y especialmente desde ningún sitio que no sea el sitio web principal de CHIRP (chirpmyradio.com). ¡Cargar un módulo desde otra fuente es como darles acceso directo a tu computador y a todo en este! ¿Proceder a pesar de este riesgo? Cargando ajustes Localizador Inicio de sesión fallido: Comprueba tu nombre de usuario y contraseña Cadena del logotipo 1 (12 caracteres) Cadena del logotipo 2 (12 caracteres) Longitud Memorias Las memorias son de sólo lectura debido a una versión de firmware no soportada La memoria %i no es borrable Nombre del campo de memoria (uno de %s) Etiqueta de memoria (almacenada en la radio) La memoria debe estar en un banco para ser editada La memoria {num} no está en banco {bank} Modo Modelo Modos Módulo Módulo cargado Módulo cargado exitosamente Más información Más de un puerto encontrado: %s Mover abajo Mover arriba Las operaciones de movimiento están deshabilitadas mientras la vista es ordenada Nueva ventana Nueva versión disponible ¡No hay filas vacías debajo! No hay ejemplo para %s No se encontraron módulos No se encontraron módulos en problema %i No hay más espacio disponible; algunas memorias no fueron aplicadas No hay resultados ¡No hay resultados! Numero Desplazamiento Desplazamiento/
Frecuencia de TX Uno de: %s Sólo ciertas bandas Sólo ciertos modos Sólo ciertos prefijos Sólo pestañas de memorias pueden ser exportadas Sólo repetidores funcionando Abrir Abrir reciente Abrir configuración original Abrir un archivo Abrir un módulo Abrir registro de depuración Abrir en nueva ventana Sólo repetidores abiertos Abrir directorio de configuración original Operador Opción Opcional: -122.0000 Opcional: 100 Opcional: 20.0000 Opcional: 45.0000 Opcional: 52.0000 Opcional: AA00 - AA00aa11 Opcional: Condado, Hospital, etc. ¿Sobrescribir memorias? Los canales P-VFO 100-109 se consideran Ajustes.
Sólo un subconjunto de las más de 130 ajustes de radio disponibles
están soportados en este lanzamiento.
 Analizando Contraseña Pegar Las memorias pegadas sobrescribirán %s memorias existentes Las memorias pegadas sobrescribirán memorias %s Las memorias pegadas sobrescribirán la memoria %s La memoria pegada sobrescribirá la memoria %s ¡Por favor asegúrate de salir de CHIRP antes de instalar la nueva versión! Por favor sigue estos pasos cuidadosamente:
1 - Enciende tu radio
2 - Conecta el cable de interfaz a tu radio
3 - Cliquea el botón en esta ventana para iniciar la descarga
    (puedes ver otro dialogo, cliquea aceptar)
4 - La radio emitirá un pitido y el led parpadeara
5 - Tendrás un tiempo de espera de 10 segundos para presionar "MON" antes
    de que la carga de datos inicie
6 - Si todo va bien la radio emitirá un pitido al final.
Después de clonar remueve el cable y apaga y enciende tu radio para
entrar en modo normal.
 Por favor sigue estos pasos cuidadosamente:
1 - Enciende tu radio
2 - Conecta el cable de interfaz a tu radio.
3 - Cliquea el botón en esta ventana para iniciar la descarga
    (La radio emitirá un pitido y el led parpadeará)
4 - Entonces presiona el botón "A" en tu radio para iniciar la clonación.
    (Al final la radio emitirá un pitido)
 Por favor te en cuenta que el modo desarrollador está pensado para uso por desarrolladores del proyecto CHIRP, o bajo la dirección de un desarrollador. Esto habilita comportamientos y funciones que pueden dañar tu computador y tu radio si no se usan con EXTREMO cuidado. ¡has sido advertido! ¿Proceder de todos modos? Por favor espera Conecta tu cable y luego haz clic en ACEPTAR Base de datos de repetidores Polacos Puerto Potencia Prefijos Presiona enter para configurar esto en memoria Previsualización de impresión Imprimiendo Dispositivo USB Prolific Propiedades Nombre de la propiedad Valor de la propiedad Localizador de QTH Consulta %s Consultar fuente La cadena de consulta es inválida Sintaxis de consulta OK Ayuda de la sintaxis de consulta Consultando RX DTCS Radio La radio no reconoció el bloque %i Información de radio Modelo de radio: La radio envió datos después del último bloque esperado, esto sucede cuando el modelo seleccionado no es de EE.UU pero la radio es una de EE.UU. Por favor elije el modelo correcto e intenta de nuevo. RadioReference Canadá requiere un inicio de sesión antes de que puedas consultar RadioReference.com es el proveedor de datos
de comunicaciones de radio más largo del mundo
<small>Requiere cuenta premium</small> Código DTCS de recepción Frecuencia de recepción Reciente Reciente... Se recomienda usar 55.2 Actualización requerida Memoria actualizada %s Recargar controlador Recargar controlador y archivo Remover Remover el modelo seleccionado de la lista Renombrar banco RepeaterBook es el directorio GRATUITO de repetidores,
mundial, de Radio Aficionados más completo. Reportar o actualizar un error... Reportando un nuevo error: %r Reportes habilitados Reportes ayudan al proyecto CHIRP a saber en cuales modelos de radio y plataformas de SO gastar nuestros limitados esfuerzos. Apreciamos mucho si lo dejas habilitado. ¿Realmente deshabilitar reportes? Reinicio requerido Restaurar %i pestaña Restaurar %i pestañas Restaurar pestañas al iniciar Resultados de otras áreas Ajustes recuperados Guardar ¿Guardar antes de cerrar? Guardar archivo Ajustes guardados Control de escáner (omitir, incluir, prioridad, etc) Listas de escaneo Codificador Riesgo de seguridad Seleccionar plan de banda... Seleccionar bandas Seleccionar lenguaje Seleccionar modos Seleccionar plan de banda Selecciona una pestaña y memoria a diferenciar Seleccionar prefijos Servicio Ajustes Los ajustes son de sólo lectura debido a una versión de firmware no soportada Cantidad de desplazamiento (o frecuencia de transmisión) controlada por dúplex Mostrar memoria en bruto Mostrar ubicación del registro de depuración Mostrar campos adicionales Mostrar ubicación de la copia de respaldo de la imagen Omitir Algunas memorias son incompatibles con esta radio Algunas memorias no son borrables Ordenar %i memoria Ordenar %i memorias Ordenar %i memoria de forma ascendente Ordenar %i memorias de forma ascendente Ordenar %i memoria de forma descendente Ordenar %i memorias de forma descendente Ordenar por columna: Ordenar memorias Ordenando Estado Estado/Provincia Éxito Reporte de error enviado exitosamente: Polaridad TX-RX CTCS (normal o invertida) Probada y en su mayoría funciona, pero puede darte algunos problemas
cuando se utilizan variantes de radio poco comunes.
¡Procede bajo tu propio riesgo, y haznos saber sobre los problemas! La red mundial DMR-MARC El controlador de la radio FT-450D carga la pestaña 'Canales Especiales'
con las memorias de rango de escaneo PMS (grupo 11), canales de 60
metros (grupo 12), la memoria QMB (STO/RCL), las HF y
memorias 50m HOME y todas las memorias VFO A y B.
Hay memorias de VFO para la última frecuencia marcada en
cada banda. La última configuración de mem-tune también se almacena.
Estos Canales Especiales permiten una edición de campo limitada.
Este controlador también llena la pestaña 'Otro' en la ventana
de Propiedades de la memoria del canal. Esta pestaña contiene valores para
esos ajustes de memoria de canal que no caen bajo el
estándar de columnas de visualización Chirp. Con el soporte de la FT450, la gui ahora
usa el modo 'DIG' para representar los modos de datos y una nueva columna
'DATA MODE' para seleccionar USER-U, USER-L y RTTY

 El controlador X3Plus es actualmente experimental.
No hay problemas conocidos pero debes proceder con precaución.
Por favor guarda una copia sin editar de tu primera descarga
exitosa a un archivo de imagen de Radio de CHIRP (*.img).
 El autor de este módulo no está reconocido por el desarrollador de CHIRP. Se recomienda que no cargues este módulo ya que podría poner un riesgo de seguridad. ¿Proceder de todos modos? El archivo de registro de depuración no está disponible cuando CHIRP se ejecuta interactivamente desde la línea de comandos. Por lo tanto, esta herramienta no cargará lo que esperas. Se recomienda que salgas ahora y ejecutes CHIRP de forma no interactiva (o con stdin redirigido a /dev/null) La siguiente información será enviada: El procedimiento recomendado para importar memorias es para abrir el archivo de origen y copiar/pegar memorias desde este en tu imagen objetivo. Si continúas con esta función de importación, CHIRP remplazara todas las memorias de tu archivo actualmente abierto por las de %(file)s. ¿Te gustaría abrir este archivo para copiar/pegar memorias a través de este, o proceder con la importación? Esta memoria Este controlador ha sido probado con v3 del ID-5100. Si tu radio no está completamente actualizada por favor ayuda abriendo un reporte de error con un registro de depuración para que podamos agregar soporte para las otras revisiones. Este controlador está en desarrollo y debe ser considerado como experimental. A esta imagen le falta la información de firmware. Esta puede haber sido generada con una versión antigua o modificada de CHIRP. Se recomienda que descargues una imagen nueva de tu radio y utilizarla en adelante para la mejor seguridad y compatibilidad. Este es un modo de radio en vivo, lo que significa que los cambios son enviados a la radio en tiempo real cuando los haces. ¡Cargar no es necesario! Este es un archivo de radio independiente y no puede ser cargado directamente a una radio. Abre una imagen de radio (o descarga una de una radio) y entonces copia/pega los elementos desde esta pestaña a esa para cargar Este es un controlador beta de etapa temprana
 Este es un controlador beta de etapa temprana - cargar bajo tu propio riesgo
 Este es un controlador experimental para la Quansheng UV-K5. Este podría dañar tu radio, o algo peor. Úsalo bajo tu propio riesgo.

Antes de intentar hacer algún cambio por favor descarga la imagen de memoria de la radio con chirp y guárdala. Esta se puede usar después para recuperar los ajustes originales.

algunos detalles aun no están implementados Este es un soporte experimental para BJ-9900 que aún está en desarrollo.
Por favor asegúrate de tener un buen respaldo con el software OEM.
¡También por favor envía en solicitudes de errores y mejoras!
Has sido advertido. ¡Procede bajo tu propio riesgo! Este es el número de boleto para un problema ya creado en el sitio web chirpmyradio.com Esta memoria y desplazar todo hacia arriba Esta memoria y desplazar bloque hacia arriba ¡Esta opción podría romper tu radio! Cara radio tiene un conjunto único de datos de calibración y cargar los datos de la imagen causará daño físico a la radio si esta es de una pieza de hardware diferente. ¡No uses esto a menos que sepas lo que estás haciendo y aceptas el riesgo de destruir tu radio! Esta radio tiene una forma engorrosa de entrar en el modo de programación,
incluso el software original tiene algunos cuantos intentos de entrar.
Lo intentaré 8 veces (la mayoría de las veces ~3 lo harán) y esto
puede tardar unos segundos, si no funciona, inténtelo de nuevo unas cuantas veces.
Si puede ingresar, por favor comprueba la radio y el cable.
 Esto sólo debe habilitarse si utilizas un firmware modificado que soporte una cobertura de frecuencias más amplia. Habilitar esto hará que CHIRP no aplique las restricciones del OEM y puede conducir a un comportamiento indefinido o no regulado. ¡Úsalo bajo tu propio riesgo! Esta herramienta cargará detalles sobre tu sistema a un problema existente en el rastreador de CHIRP. Esta requiere tu nombre de usuario y contraseña para chirpmyradio.com para poder trabajar. Información de tu sistema, incluyendo tu registro de depuración, archivo de configuración, y cualquier archivo de imagen abierto serán cargados. Se hará un intento para redactar cualquier información personal antes de que esta deje tu sistema. Esto va a cargar un módulo desde un problema del sitio web Tono Modo de tono Silenciador de tono Modo de silenciador de tono Potencia de transmisión Desplazamiento de transmisión, modo dividido, o inhibición de transmisión Tono de transmisión Código DTCS de transmisión/recepción para el modo DTCS, sino, código de transmisión Modulación de transmisión/recepción (FM, AM, SSB, etc) Tono de transmisión/recepción para el modo TSQL, sino, tono de recepción Paso de sintonización Escribe una cadena de búsqueda simple o una consulta formateada y presiona enter Buscador de puertos USB No se puede determinar puerto para tu cable. Comprueba tus controladores y conexiones. No se puede editar memoria antes de que la radio este cargada No se puede buscar la configuración original %r No se puede importar mientras la vista es ordenada No se puede abrir el portapapeles No se puede consultar No se puede leer el último bloque. Esto suele suceder cuando el modelo seleccionado es de EE.UU pero la radio no es una de EE.UU (o de banda ancha). Por favor elije el modelo correcto e intenta de nuevo. No se puede revelar %s en este sistema No se puede establecer %s en esta memoria No se puede cargar este archivo Estados Unidos Desconecta tu cable (si es necesario) y entonces haz clic en ACEPTAR Modelo no soportado %r Actualizar un error existente Actualizando error %s Cargar instrucciones La carga está deshabilitada debido a una versión de firmware no soportada Cargar a radio Cargar a radio... Memoria cargada %s Usar fuente de ancho fijo Usar fuente más grande Nombre de usuario Valor no cabe en %i bits Valor debe ser al menos %.4f Valor debe ser como máximo %.4f Valor debe ser exactamente %i dígitos decimales Valor debe ser cero o superior Valor a buscar en el campo de memoria Valores Vendedor Ver ¡ADVERTENCIA! Advertencia Advertencia: %s Bienvenido Con esta opción marcada, una búsqueda de proximidad incluirá resultados en caché desde otras localidades si ellas están en rango, coinciden con otros parámetros de filtro, y han sido descargadas antes. ¿Quieres que CHIRP instale un icono en el escritorio para ti? Haz abierto múltiples problemas dentro de la última semana. CHIRP limita el numero de problemas que puedes abrir para evitar abusos. Si realmente necesitas abrir otro, por favor hazlo a través del sitio web. Tu dispositivo USB basado en Prolific no funcionará sin revertir a una versión más antigua del controlador. Visita el sitio web de CHIRP para leer más sobre ¿cómo resolver esto? Tú localizador de QTH Tu cable parece estar en puerto: bits bytes bytes cada deshabilitado habilitado {bank} está lleno 