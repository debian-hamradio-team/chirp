��    �     l  9  �      �#  -   �#  9   �#     	$  ;   $  +   [$     �$     �$    �$  �   �%  �  Q&  �   �'  �   �(    e)  �   |*    q+  �   w,  �   q-  �   i.  �   e/    U0  o  a1  q  �2  �   C4  �   5  p  �5  e  f7  �   �8  �   �9  �   �:  �   r;  �   _<  �   J=  �   2>  	  0?  �   :@  �   �@  �   �A  �   �B  �   oC  �   PD  #  .E    RF  M  ^G  �   �H  �   gI  �   J  �   �J    �K    �L  �   �M  �   �N  �   �O  m   P  �   sP  �   RQ  �   <R  �   �R    �S  �   �T  �   �U  �   �V  ^   �W     X     X     "X  	   &X     0X     GX     OX     eX     wX     �X     �X     �X     �X     �X     �X     �X  <   �X     Y  ]   Y  J   yY     �Y     �Y     �Y     �Y     	Z     Z  (   )Z     RZ    WZ  ,   v[     �[     �[     �[     �[  
   �[  %   �[     \     \     %\  Q   .\  k   �\     �\     �\     �\  
   ]     ]  	   ]     (]     ,]     1]     ?]  	   K]     U]     b]     f]     m]  A   |]     �]     �]     �]     �]     �]     ^     ^     *^      B^     c^     l^     �^     �^     �^     �^     �^  C   �^     _     "_     ?_     Z_     q_     y_     �_     �_     �_     �_     �_     �_     `     .`     B`     b`     p`     �`     �`  e   �`  u   �`     la     �a     �a     �a     �a     �a  1   �a      b  	   b     b     b  �   -b  �   �b  �   �c    �d  �   �e  �   @f  �   �f  �   �g  �   Sh  �   i  8  �i  �   k  �   �k  �   nl  �   m  �   �m  �   Qn  �   �n  .   �o  	   �o     �o     �o     �o     p     p     p     &p  
   +p     6p     :p  ,   Np  7   {p     �p     �p     �p     �p     �p     �p      q     q     q     3q     Hq  '   ^q     �q     �q     �q  "   �q     �q     �q     �q     �q     r     r     "r     .r     :r  4   Gr  
   |r     �r     �r     �r  �   �r  v  Ns     �t     �t     �t  	   u     u     %u     @u  %   _u     �u     �u     �u     �u     �u     �u     �u     �u  	   v     v  5   v  
   Kv     Vv     lv     �v     �v  
   �v     �v     �v     �v     �v     �v      �v     w     2w     7w     Cw     Uw     aw     ow     ~w     �w     �w     �w     �w     �w      �w     	x  �   x     �x     �x  3   �x  *   �x  (   y  &   :y  ?   ay  �  �y  (  f{    �|     �}  $   �}     �}     �}  !   �}     ~     ~  
   ~     #~     ,~     9~     A~     G~     b~  �   t~  ;     t   Y     �     �     �     �     �     %�     <�  W   H�     ��  �   ��     k�     |�     ��     ��     ā     Ɂ  	   ށ     �  +   ��  	   #�  	   -�     7�     E�     X�     e�     u�     ��     ��     ��  9   ��     ߂     �     �     �     4�  .   9�     h�     ��  3   ��  5   ܃     �     "�     0�     8�     >�     M�  (   U�     ~�  �  ��  �   t�  �   F�  ^  �     F�  �   R�  G   �  �   U�  |   F�  �   Ì  #   ��  =   ��  <  �  �   *�     �     ,�  +  K�  &  w�  �   ��  ,   ��     ��  	   ��     ɓ     ֓     �  /   ��     '�  <   5�  .   r�  6   ��     ؔ     �  L   ��  ,   A�     n�  )   ��     ��     ԕ  �   �  "   ��     ��     і     �  /   ��     *�     ?�     S�     c�     v�     ��     ��     ��     ̗     �  '   �     +�     I�     P�     W�     \�     e�     m�     y�  7   ��  !   ��     ۘ     ��  
   �     �     ��     �  `  �  -   r�  U   ��     ��  Z   �  ;   j�     ��     ��  j  ��  �   )�    ��  �   �  5  ��  M  #�     q�  <  ��  5  ϥ  )  �  .  /�  "  ^�  ?  ��  �  ��  �  �  
  7�  �   B�  �  ?�  �  �    ��  �   ��    ��    ��    ��    ͹  (  ߺ  G  �  �   P�  �   5�    �     /�    0�    B�  d  J�  ?  ��  �  ��  �   ��  �   t�  �   N�  �   K�  O  :�  N  ��    ��    ��  �   �  �   ��    Q�  #  p�  �   ��  6  m�  C  ��  &  ��    �    �  o   '�  	   ��     ��     ��     ��     ��     ��     ��     
�     (�     :�     N�     b�     i�  	   q�  
   {�     ��  Q   ��     ��  v   ��  \   v�     ��     ��     ��     �     $�     7�  2   F�     y�  Z  �  3   ��     �  '   �  (   >�     g�     n�  .   }�     ��     ��     ��  k   ��  }   G�     ��     ��     ��  
   ��     ��     ��     �     �     �     .�     =�     I�     P�  	   T�     ^�  a   p�     ��     ��     ��  $   �     6�     B�     K�     d�  0   �     ��     ��      ��      ��     �     %�     B�  W   U�     ��  %   ��  &   ��     �      �     (�     =�  &   W�  (   ~�     ��     ��  .   ��  %   	�     /�  2   D�     w�     ��     ��     ��  �   ��  �   D�  .   ��     �     #�     4�     K�     T�  S   \�  
   ��     ��     ��     ��  �   ��  �   ��  �   ��  A  ��  �   #�  �   ��  �   ��    ��    ��  �   ��  �  ��  �   �  �   ��  �   ��  �   ��  �   C�  �   �  �   ��  7   ��  
   ��     ��     ��     ��     �     '�     =�     F�     K�     X�     \�  4   w�  P   ��     ��     �     $�     :�     Q�     W�     \�     h�  #   ��     ��     ��  .   ��     �     �     -�  (   C�     l�     ��     ��     ��     ��     ��     ��     ��     ��  E   �     J�     Z�  #   o�  &   ��  �   ��  �  Z�     �     2�     P�  	   n�  	   x�  $   ��  &   ��  :   ��  (   	     2     7     ?     E     L     [     w  	   �     �  =   �     �     �         * )   ?    i    y    � 	   �    �    � 7   � ,       4    ;    J    b    t    � !   � (   �    �    �         !   -    O �   g         <     5   ] 3   � -   � E   � R  ; \  � e  �    Q
 '   l
    �
 	   �
 2   �
    �
 
   �
    �
            *    2 -   8    f �    A   A �   �            2    P    h    y    � N   �    �       + )   D #   n    �    �    �    �    � 4       < 
   J    U !   j    �    �    �    �    �    � @   � )   . -   X #   � -   �    � 1   � &        9 4   Z :   �    �    �    �    �    �     +       : �  V �   � �   �   �     �    Q    7  S �   �   N +   W R   � �  � .  k  ,   �! /   �! X  �! y  P# 6  �$ B   & 	   D&    N&    ]&    o&    �& E   �&    �& N   �& 5   L' N   �'    �'    �' W   �' D   M( 1   �( /   �( $   �(    ) �   1) (   �) ,   * &   J* 
   q* ;   |*    �*    �*    �*    +    ,+ #   F+    j+ $   �+ $   �+ $   �+ 1   �+ %   +,    Q, 	   Y,    c, 
   h, 	   s,    }, 	   �, E   �, #   �,    �,    -    - 
   -    $-    ,-    ^   H  �   �   �     �   )   R       �  [             �       �   �      :       f          �    %   �          �       /   �   Q      A  1  S     �       �           E   B  �        �   ]   ;  �      *  �       �    �   E  
  �   �   �      �   U       O  $   �       �   �       �      6  ;   �   �   �  %  �   �   g   �  !   u  O   �   �     p         z     G   c      �   �          d   �   y   �       �   �           w        �   �  �   �      �   k  �   �      d      n   h  s       `   3         0   4  �       f     n  9    +   �   �         �   �   V       \   �   �      �  �      S  �   }   �   |      �   ?       �     �   P  b  X              �   c   �   D     u   Y   "      {   �   X         W   h      I       �  �   [         b   �   �   s  i   /  J       v           !          �  #   �  T  &   �   }  �     �   e  �   �   �        �   5       t   �   �       �   @   �   �   :  >       �       �  l  �  �   |   {      �       �  D     �  B     �  �   �          K   �   �           v                     ,  i  r  q   �        �       I  l   p   �  <   �      r       �     �   �  �           &  A   �               >      t      Y      y      �   7     M  �   (       =           N  �   ?  �  6   C  J  -           )  j   �      �       *   7             $  �       g  �   �   2  o   �       �   �   U              N       �   �      �   z   Z       �   8     5  K  -         �   �   Z  �   �   a            �   �   P   3   (  m  #          �           =     x     �      �   �               ~  H   �   	                 m   L                 �       `  �   V  _   �       �  .   �   �       W  �  T   �  �       �   ^  �   �           9             e   2           L   	   �   ~   �  �      '    M       w   �   4       _      8   �  '           a  �       �  "       �       o              �  
       �     x      �           G      k   0  �  �           q  �   �  �              �   \     �   F       <  �        �  C       F  R  �     .      j      1   �      +  Q   �       ]  �   �  ,   @      �        �             %(value)s must be between %(min)i and %(max)i %i Memories and shift all up %i Memories and shift all up %i Memory %i Memories %i Memory and shift block up %i Memories and shift block up %s has not been saved. Save before closing? (none) ...and %i more 1. Ensure your firmware version is 4_10 or higher
2. Turn radio off
3. Connect your interface cable
4. Turn radio on
5. Press and release PTT 3 times while holding MONI key
6. Supported baud rates: 57600 (default) and 19200
   (rotate dial while holding MONI to change)
7. Click OK
 1. Turn Radio off.
2. Connect data cable.
3. While holding "A/N LOW" button, turn radio on.
4. <b>After clicking OK</b>, press "SET MHz" to send image.
 1. Turn Radio off.
2. Connect data cable.
3. While holding "A/N LOW" button, turn radio on.
4. Press "MW D/MR" to receive image.
5. Make sure display says "-WAIT-" (see note below if not)
6. Click OK to dismiss this dialog and start transfer.
Note: if you don't see "-WAIT-" at step 5, try cycling
      power and pressing and holding red "*L" button to unlock
      radio, then start back at step 1.
 1. Turn Radio off.
2. Connect data cable.
3. While holding "TONE" and "REV" buttons, turn radio on.
4. <b>After clicking OK</b>, press "TONE" to send image.
 1. Turn Radio off.
2. Connect data cable.
3. While holding "TONE" and "REV" buttons, turn radio on.
4. Press "REV" to receive image.
5. Make sure display says "CLONE RX" and green led is blinking
6. Click OK to start transfer.
 1. Turn radio off.
2. Connect cable
3. Press and hold in the MHz, Low, and D/MR keys on the radio while turning it on
4. Radio is in clone mode when TX/RX is flashing
5. <b>After clicking OK</b>, press the MHz key on the radio to send image.
    ("TX" will appear on the LCD). 
 1. Turn radio off.
2. Connect cable
3. Press and hold in the MHz, Low, and D/MR keys on the radio while turning it on
4. Radio is in clone mode when TX/RX is flashing
5. Press the Low key on the radio ("RX" will appear on the LCD).
6. Click OK. 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. <b>After clicking OK</b> here, press the [C.S.] key to
    send image.
 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. <b>After clicking OK</b>, press the [A] key to send image.
 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. Click OK here.
    ("Receiving" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. Press the [A](RCV) key ("receiving" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. Press the [C] key ("RX" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to CAT/LINEAR jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. <b>After clicking OK</b>,
     press the [C](SEND) key to send image.
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the "left" [V/M] key while turning the
     radio on.
4. Rotate the "right" DIAL knob to select "CLONE START".
5. Press the [SET] key. The display will disappear
     for a moment, then the "CLONE" notation will appear.
6. <b>After clicking OK</b>, press the "left" [V/M] key to
     send image.
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the "left" [V/M] key while turning the
     radio on.
4. Rotate the "right" DIAL knob to select "CLONE START".
5. Press the [SET] key. The display will disappear
     for a moment, then the "CLONE" notation will appear.
6. Press the "left" [LOW] key ("CLONE -RX-" will appear on
     the display).
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the [FW] key while turning the radio on
     ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [BAND] key to send image.
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the [FW] key while turning the radio on
     ("CLONE" will appear on the display).
4. Press the [MODE] key ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the [MHz(PRI)] key while turning the
     radio on.
4. Rotate the DIAL job to select "F-7 CLONE".
5. Press and hold in the [BAND(SET)] key. The display
     will disappear for a moment, then the "CLONE" notation
     will appear.
6. Press the [LOW(ACC)] key ("--RX--" will appear on the display).
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the [MHz(PRI)] key while turning the
 radio on.
4. Rotate the DIAL job to select "F-7 CLONE".
5. Press and hold in the [BAND(SET)] key. The display
 will disappear for a moment, then the "CLONE" notation
 will appear.
6. <b>After clicking OK</b>, press the [V/M(MW)] key to send image.
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold in the [F] key while turning the radio on
     ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [BAND] key to send image.
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold in the [F] key while turning the radio on
     ("CLONE" will appear on the display).
4. Press the [Dx] key ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold in the [MHz(SETUP)] key while turning the radio
     on ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [REV(DW)] key
     to send image.
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold in the [MHz(SETUP)] key while turning the radio
     on ("CLONE" will appear on the display).
4. Press the [MHz(SETUP)] key
     ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC Jack.
3. Press and hold in the [MHz(SETUP)] key while turning the radio
     on ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [GM(AMS)] key
     to send image.
 1. Turn radio off.
2. Connect cable to MIC Jack.
3. Press and hold in the [MHz(SETUP)] key while turning the radio
     on ("CLONE" will appear on the display).
4. Press the [MHz(SETUP)] key
     ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC/EAR jack.
3. Press and hold in the [F/W] key while turning the radio on
    ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [VFO(DW)SC] key to receive
    the image from the radio.
 1. Turn radio off.
2. Connect cable to MIC/EAR jack.
3. Press and hold in the [F/W] key while turning the radio on
    ("CLONE" will appear on the display).
4. Press the [MR(SKP)SC] key ("CLONE WAIT" will appear
    on the LCD).
5. Click OK to send image to radio.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold [PTT] &amp; Knob while turning the
     radio on.
4. <b>After clicking OK</b>, press the [PTT] switch to send image.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold [PTT] &amp; Knob while turning the
     radio on.
4. Press the [MONI] switch ("WAIT" will appear on the LCD).
5. Press OK.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [F/W] key while turning the radio on
     ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [BAND] key to send image.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [F/W] key while turning the radio on
     ("CLONE" will appear on the display).
4. Press the [V/M] key ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [MON-F] key while turning the radio on
     ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [BAND] key to send image.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [MON-F] key while turning the radio on
     ("CLONE" will appear on the display).
4. Press the [V/M] key ("CLONE WAIT" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [MONI] switch while turning the
     radio on.
4. Rotate the DIAL job to select "F8 CLONE".
5. Press the [F/W] key momentarily.
6. <b>After clicking OK</b>, hold the [PTT] switch
     for one second to send image.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [MONI] switch while turning the
     radio on.
4. Rotate the DIAL job to select "F8 CLONE".
5. Press the [F/W] key momentarily.
6. Press the [MONI] switch ("--RX--" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [moni] key while turning the radio on.
4. Select CLONE in menu, then press F. Radio restarts in clone mode.
     ("CLONE" will appear on the display).
5. <b>After clicking OK</b>, briefly hold [PTT] key to send image.
    ("-TX-" will appear on the LCD). 
 1. Turn radio off.
2. Connect cable to mic jack.
3. Press and hold in the [LOW(A/N)] key while turning the radio on.
4. <b>After clicking OK</b>, press the [MHz(SET)] key to send image.
 1. Turn radio off.
2. Connect cable to mic jack.
3. Press and hold in the [LOW(A/N)] key while turning the radio on.
4. Press the [D/MR(MW)] key ("--WAIT--" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to mic jack.
3. Press and hold in the [MHz], [LOW], and [D/MR] keys
   while turning the radio on.
4. <b>After clicking OK</b>, press the [MHz(SET)] key to send image.
 1. Turn radio off.
2. Connect cable to mic jack.
3. Press and hold in the [MHz], [LOW], and [D/MR] keys
   while turning the radio on.
4. Press the [D/MR(MW)] key ("--WAIT--" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Turn radio on (volume may need to be set at 100%).
5. Ensure that the radio is tuned to channel with no activity.
6. Click OK to download image from device.
 1. Turn radio off.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Turn radio on (volume may need to be set at 100%).
5. Ensure that the radio is tuned to channel with no activity.
6. Click OK to upload image to device.
 1. Turn radio off.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Turn radio on.
5. Ensure that the radio is tuned to channel with no activity.
6. Click OK to download image from device.
 1. Turn radio off.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Turn radio on.
5. Ensure that the radio is tuned to channel with no activity.
6. Click OK to upload image to device.
 1. Turn radio off.
2. Connect data cable.
3. Prepare radio for clone.
4. <b>After clicking OK</b>, press the key to send image.
 1. Turn radio off.
2. Connect data cable.
3. Prepare radio for clone.
4. Press the key to receive the image.
 1. Turn radio off.
2. Connect mic and hold [ACC] on mic while powering on.
    ("CLONE" will appear on the display)
3. Replace mic with PC programming cable.
4. <b>After clicking OK</b>, press the [SET] key to send image.
 1. Turn radio off.
2. Connect mic and hold [ACC] on mic while powering on.
    ("CLONE" will appear on the display)
3. Replace mic with PC programming cable.
4. Press the [DISP/SS] key
    ("R" will appear on the lower left of LCD).
 1. Turn radio off.
2. Remove front head.
3. Connect data cable to radio, use the same connector where
     head was connected to, <b>not the mic connector</b>.
4. Click OK.
 1. Turn radio off.
3. Press and hold in the [moni] key while turning the radio on.
4. Select CLONE in menu, then press F. Radio restarts in clone mode.
     ("CLONE" will appear on the display).
5. Press the [moni] key ("-RX-" will appear on the LCD).
 1. Turn radio on.
2. Connect cable to DATA terminal.
3. Unclip battery.
4. Press and hold in the [AMS] key and power key while clipping 
 in back battery the ("ADMS" will appear on the display).
5. Press the [MODE] key ("-WAIT-" will appear on the LCD).
<b>Then click OK</b> 1. Turn radio on.
2. Connect cable to DATA terminal.
3. Unclip battery.
4. Press and hold in the [AMS] key and power key while clipping 
 in back battery the("ADMS" will appear on the display).
5. <b>After clicking OK</b>, press the [BAND] key.
 1. Turn radio on.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Click OK to download image from device.

It will may not work if you turn on the radio with the cable already attached
 1. Turn radio on.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Click OK to upload the image to device.

It will may not work if you turn on the radio with the cable already attached A new CHIRP version is available. Please visit the website as soon as possible to download it! About About CHIRP All All Files All supported formats| Amateur An error has occurred Applying settings Automatic from system Available modules Bandplan Bands Banks Bin Browser Building Radio Browser CHIRP must be restarted for the new selection to take effect Canada Changing this setting requires refreshing the settings from the image, which will happen now. Channels with equivalent TX and RX %s are represented by tone mode of "%s" Chirp Image Files Choice Required Choose %s DTCS Code Choose %s Tone Choose Cross Mode Choose duplex Choose the module to load from issue %i: City Click on the "Special Channels" toggle-button of the memory
editor to see/set the EXT channels. P-VFO channels 100-109
are considered Settings.
Only a subset of the over 200 available radio settings
are supported in this release.
Ignore the beeps from the radio on upload and download.
 Clone completed, checking for spurious bytes Cloning Cloning from radio Cloning to radio Close Close file Cluster %i memory Cluster %i memories Comment Communicate with radio Complete Complex or non-standard tone squelch mode (starts the tone mode selection wizard) Connect your interface cable to the PC Port on the
back of the 'TX/RX' unit. NOT the Com Port on the head.
 Convert to FM Copy Country Cross Mode Custom Port Custom... Cut DTCS DTCS
Polarity DTMF decode DV Memory Danger Ahead Dec Delete Developer Mode Developer state is now %s. CHIRP must be restarted to take effect Diff Raw Memories Digital Code Digital Modes Disable reporting Disabled Distance Do not prompt again for %s Do you accept the risk? Double-click to change bank name Download Download from radio Download from radio... Download instructions Driver Driver information Driver messages Dual-mode digital repeaters that support analog will be shown as FM Duplex Edit details for %i memories Edit details for memory %i Enable Automatic Edits Enabled Enter Frequency Enter Offset (MHz) Enter TX Frequency (MHz) Enter a new name for bank %s: Enter custom port: Erased memory %s Error applying settings Error communicating with radio Experimental driver Export can only write CSV files Export to CSV Export to CSV... Extra FM Radio FREE repeater database, which provides information
about repeaters in Europe. No account is required. FREE repeater database, which provides most up-to-date
information about repeaters in Europe. No account is
required. Failed to load radio browser Failed to parse result Features File does not exist: %s Files Filter Filter results with location matching this string Find Find Next Find... Finished radio job %s Follow these instructions to download the radio memory:
1 - Connect your interface cable
2 - Radio > Download from radio: DO NOT mess with the radio
during download!
3 - Disconnect your interface cable
 Follow these instructions to download the radio memory:
1 - Connect your interface cable
2 - Radio > Download from radio: Don't adjust any settings
on the radio head!
3 - Disconnect your interface cable
 Follow these instructions to download the radio memory:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio, volume @ 50%
4 - CHIRP Menu - Radio - Download from radio
 Follow these instructions to download your config:
1 - Turn off your radio
2 - Connect your interface cable to the Speaker-2 jack
3 - Turn on your radio
4 - Radio > Download from radio
5 - Disconnect the interface cable! Otherwise there will be
    no right-side audio!
 Follow these instructions to download your info:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the download of your radio data
 Follow these instructions to download your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio (unlock it if password protected)
4 - Click OK to start
 Follow these instructions to read your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Click OK to start
 Follow these instructions to upload the radio memory:
1 - Connect your interface cable
2 - Radio > Upload to radio: DO NOT mess with the radio
during upload!
3 - Disconnect your interface cable
 Follow these instructions to upload the radio memory:
1 - Connect your interface cable
2 - Radio > Upload to radio: Don't adjust any settings
on the radio head!
3 - Disconnect your interface cable
 Follow these instructions to upload the radio memory:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio, volume @ 50%
4 - CHIRP Menu - Radio - Upload to radio
 Follow these instructions to upload your config:
1 - Turn off your radio
2 - Connect your interface cable to the Speaker-2 jack
3 - Turn on your radio
4 - Radio > Upload to radio
5 - Disconnect the interface cable, otherwise there will be
    no right-side audio!
6 - Cycle power on the radio to exit clone mode
 Follow these instructions to upload your info:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the upload of your radio data
 Follow these instructions to upload your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio (unlock it if password protected)
4 - Click OK to start
 Follow these instructions to write your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Click OK to start
 Follow this instructions to download your info:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the download of your radio data
 Follow this instructions to read your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the download of your radio data
 Follow this instructions to upload your info:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the upload of your radio data
 Follow this instructions to write your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the upload of your radio data
 Found empty list value for %(name)s: %(value)r Frequency Frequency granularity in kHz GMRS Getting settings Goto Memory Goto Memory: Goto... Help Help Me... Hex Hide empty memories Human-readable comment (not stored in radio) If set, sort results by distance from these coordinates Import Import from file... Import messages Import not recommended Index Info Information Insert Row Above Install desktop icon? Interact with driver Internal driver error Invalid %(value)s (use decimal degrees) Invalid Entry Invalid ZIP code Invalid edit: %s Invalid or unsupported module file Invalid value: %r Issue number: LIVE Language Latitude Length must be %i Limit Bands Limit Modes Limit Status Limit results to this distance (km) from coordinates Live Radio Load Module... Load module from issue Load module from issue... Loading a module will not affect open tabs. It is recommended (unless instructed otherwise) to close all tabs before loading a module. Loading modules can be extremely dangerous, leading to damage to your computer, radio, or both. NEVER load a module from a source you do not trust, and especially not from anywhere other than the main CHIRP website (chirpmyradio.com). Loading a module from another source is akin to giving them direct access to your computer and everything on it! Proceed despite this risk? Loading settings Logo string 1 (12 characters) Logo string 2 (12 characters) Longitude Memories Memory %i is not deletable Memory label (stored in radio) Memory must be in a bank to be edited Memory {num} not in bank {bank} Mode Model Modes Module Module Loaded Module loaded successfully More than one port found: %s Move Down Move Up Move operations are disabled while the view is sorted New Window New version available No empty rows below! No modules found No modules found in issue %i No results No results! Number Offset Only certain bands Only certain modes Only memory tabs may be exported Only working repeaters Open Open Recent Open Stock Config Open a file Open a module Open debug log Open in new window Open stock config directory Option Optional: -122.0000 Optional: 100 Optional: 45.0000 Optional: County, Hospital, etc. Overwrite memories? P-VFO channels 100-109 are considered Settings.
Only a subset of the over 130 available radio settings
are supported in this release.
 Parsing Paste Pasted memories will overwrite %s existing memories Pasted memories will overwrite memories %s Pasted memories will overwrite memory %s Pasted memory will overwrite memory %s Please be sure to quit CHIRP before installing the new version! Please follow this steps carefully:
1 - Turn on your radio
2 - Connect the interface cable to your radio
3 - Click the button on this window to start download
    (you may see another dialog, click ok)
4 - Radio will beep and led will flash
5 - You will get a 10 seconds timeout to press "MON" before
    data upload start
6 - If all goes right radio will beep at end.
After cloning remove the cable and power cycle your radio to
get into normal mode.
 Please follow this steps carefully:
1 - Turn on your radio
2 - Connect the interface cable to your radio.
3 - Click the button on this window to start download
    (Radio will beep and led will flash)
4 - Then press the "A" button in your radio to start cloning.
    (At the end radio will beep)
 Please note that developer mode is intended for use by developers of the CHIRP project, or under the direction of a developer. It enables behaviors and functions that can damage your computer and your radio if not used with EXTREME care. You have been warned! Proceed anyway? Please wait Plug in your cable and then click OK Port Power Press enter to set this in memory Print Preview Printing Properties Query %s Query Source RX DTCS Radio Radio did not ack block %i Radio information Radio sent data after the last awaited block, this happens when the selected model is a non-US but the radio is a US one. Please choose the correct model and try again. RadioReference Canada requires a login before you can query RadioReference.com is the world's largest
radio communications data provider
<small>Premium account required</small> Receive DTCS code Receive frequency Refresh required Refreshed memory %s Reload Driver Reload Driver and File Rename bank RepeaterBook is Amateur Radio's most comprehensive,
worldwide, FREE repeater directory. Reporting enabled Reporting helps the CHIRP project know which radio models and OS platforms to spend our limited efforts on. We would really appreciate if you left it enabled. Really disable reporting? Restart Required Restore %i tab Restore %i tabs Restore tabs on start Retrieved settings Save Save before closing? Save file Saved settings Scan control (skip, include, priority, etc) Scanlists Scrambler Security Risk Select Bandplan... Select Bands Select Language Select Modes Select a bandplan Service Settings Shift amount (or transmit frequency) controlled by duplex Show Raw Memory Show debug log location Show extra fields Show image backup location Skip Some memories are incompatible with this radio Some memories are not deletable Sort %i memory Sort %i memories Sort %i memory ascending Sort %i memories ascending Sort %i memory descending Sort %i memories descending Sort by column: Sort memories Sorting State State/Province Success TX-RX DTCS polarity (normal or reversed) The DMR-MARC Worldwide Network The FT-450D radio driver loads the 'Special Channels' tab
with the PMS scanning range memories (group 11), 60meter
channels (group 12), the QMB (STO/RCL) memory, the HF and
50m HOME memories and all the A and B VFO memories.
There are VFO memories for the last frequency dialed in
each band. The last mem-tune config is also stored.
These Special Channels allow limited field editing.
This driver also populates the 'Other' tab in the channel
memory Properties window. This tab contains values for
those channel memory settings that don't fall under the
standard Chirp display columns. With FT450 support, the gui now
uses the mode 'DIG' to represent data modes and a new column
'DATA MODE' to select USER-U, USER-L and RTTY 
 The X3Plus driver is currently experimental.
There are no known issues but you should proceed with caution.
Please save an unedited copy of your first successful
download to a CHIRP Radio Images (*.img) file.
 The author of this module is not a recognized CHIRP developer. It is recommended that you not load this module as it could pose a security risk. Proceed anyway? The recommended procedure for importing memories is to open the source file and copy/paste memories from it into your target image. If you continue with this import function, CHIRP will replace all memories in your currently-open file with those in %(file)s. Would you like to open this file to copy/paste memories across, or proceed with the import? This Memory This driver has been tested with v3 of the ID-5100. If your radio is not fully updated please help by opening a bug report with a debug log so we can add support for the other revisions. This driver is in development and should be considered as experimental. This image is missing firmware information. It may have been generated with an old or modified version of CHIRP. It is advised that you download a fresh image from your radio and use that going forward for the best safety and compatibility. This is a live-mode radio, which means changes are sent to the radio in real-time as you make them. Upload is not necessary! This is a radio-independent file and cannot be uploaded directly to a radio. Open a radio image (or download one from a radio) and then copy/paste items from this tab into that one in order to upload This is an early stage beta driver
 This is an early stage beta driver - upload at your own risk
 This is an experimental driver for the Quansheng UV-K5. It may harm your radio, or worse. Use at your own risk.

Before attempting to do any changes please download the memory image from the radio with chirp and keep it. This can be later used to recover the original settings. 

some details are not yet implemented This is experimental support for BJ-9900 which is still under development.
Please ensure you have a good backup with OEM software.
Also please send in bug and enhancement requests!
You have been warned. Proceed at your own risk! This memory and shift all up This memory and shift block up This option may break your radio! Each radio has a unique set of calibration data and uploading the data from the image will cause physical harm to the radio if it is from a different piece of hardware. Do not use this unless you know what you are doing and accept the risk of destroying your radio! This radio has a tricky way of enter into program mode,
even the original software has a few tries to get inside.
I will try 8 times (most of the time ~3 will doit) and this
can take a few seconds, if don't work, try again a few times.
If you can get into it, please check the radio and cable.
 This should only be enabled if you are using modified firmware that supports wider frequency coverage. Enabling this will cause CHIRP not to enforce OEM restrictions and may lead to undefined or unregulated behavior. Use at your own risk! This will load a module from a website issue Tone Tone Mode Tone Squelch Tone squelch mode Transmit Power Transmit shift, split mode, or transmit inhibit Transmit tone Transmit/receive DTCS code for DTCS mode, else transmit code Transmit/receive modulation (FM, AM, SSB, etc) Transmit/receive tone for TSQL mode, else receive tone Tuning Step USB Port Finder Unable to determine port for your cable. Check your drivers and connections. Unable to edit memory before radio is loaded Unable to find stock config %r Unable to import while the view is sorted Unable to open the clipboard Unable to query Unable to read last block. This often happens when the selected model is US but the radio is a non-US one (or widebanded). Please choose the correct model and try again. Unable to reveal %s on this system Unable to set %s on this memory Unable to upload this file United States Unplug your cable (if needed) and then click OK Unsupported model %r Upload instructions Upload to radio Upload to radio... Uploaded memory %s Use fixed-width font Use larger font Value does not fit in %i bits Value must be at least %.4f Value must be at most %.4f Value must be exactly %i decimal digits Value must be zero or greater Values Vendor View WARNING! Warning Warning: %s Welcome Would you like CHIRP to install a desktop icon for you? Your cable appears to be on port: bits bytes bytes each disabled enabled {bank} is full Project-Id-Version: CHIRP
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-05-22 16:39-0400
Last-Translator: Alexandre J. Raymond <alexandre.j.raymond@gmail.com>
Language-Team: French
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.4.4
 %(value)s doit être entre %(min)i et %(max)i %i Mémoires et tout décaler vers le haut %i Mémoires et tout décaler vers le haut %i Mémoire %i Mémoires %i Mémoire et décaler le bloc vers le haut %i Mémoires et décaler le bloc vers le haut %s n'a pas été enregistré. Enregistrer avant de fermer ? (vide) ...et %i de plus 1. Assurez-vous que le firmware est en version is 4_10 ou supérieure
2. Éteignez la radio
3. Connectez votre câble d'interface
4. Allumez la radio
5. Appuyez et relâchez le PTT 3 fois tout en maintenant appuyée la touche MONI
6. Vitesses supportées: 57600 (défaut) et 19200
   (tournez le rotacteur en maintenant appuyée la touche MONI
7. Cliquez sur OK
 1. Éteignez la radio.
2. Connectez le câble de données.
3. En maintenant appuyée la touche "A/N LOW", allumez la radio.
4. <b>Après avoir cliqué sur OK</b>, appuyez sur "SET MHz" pour envoyer l'image.
 1. Éteignez la radio.
2. Connectez le câble de données.
3. Tout en maintenant appuyée la touche "A/N LOW", allumez la radio.
4. Appuyez sur "MW D/MR" pour recevoir l'image.
5. Assurez-vous de l'affichage "-WAIT-" (consultez la note ci-dessous dans le cas contraire)
6. Cliquez sur OK pour ignorer le dialogue et lancer le transfert.
Note: si vous ne voyez pas "-WAIT-" à l'étape 5, essayez de redémarrer
      en appuyant et en maintenant le bouton rouge "*L" pour déverrouiller
      la radio, retournez ensuite à l'étape 1.
 1. Éteignez la radio.
2. Connectez le câble de données.
3. Allumez la radio tout en maintenant les touches "TONE" and "REV" appuyées.
4. <b>Après avoir cliqué sur OK</b>, appuyez sur "TONE" pour envoyer l'image.
 1. Éteignez la radio.
2. Connectez le câble de données.
3. Allumez la radio tout en maintenant les touches "TONE" and "REV" appuyées.
4. Appuyez sur "REV" pour recevoir l'image.
5. Assurez vous que l'affichage indique "CLONE RX" et que la LED verte clignote
6. Cliquez sur OK pour démarrer le transfert.
 1. Éteignez la radio.
2. Connectez le câble
3. Appuyez et maintenez les touches MHz, Low, et D/MR de la radio pendant son allumage
4. La radio est en mode de clonage quand TX/RX clignote
5. <b>Après avoir cliqué sur OK</b>, appuyez sur la touche MHz de la radio pour envoyer l'image.
    ("TX" va apparaître sur l'écran LCD). 
 1. Éteignez la radio.
2. Connectez le câble
3. Appuyez et maintenez les touches MHz, Low, et D/MR de la radio pendant son allumage
4. La radio est en mode de clonage quand TX/RX clignote
5. Appuyez sur la touche Low de la radio ("RX" va apparaître sur l'écran LCD).
6. Cliquez sur OK. 1. Éteignez la radio.
2. Connectez le câble à la prise jack ACC.
3. Appuyez et maintenez les touches [MODE &lt;] et [MODE &gt;] pendant
     que vous allumez la radio ("CLONE MODE" va apparaître sur
     l'afficheur).
4. <b>Après avoir cliqué sur OK</b> ici, appuyez la touche [C.S.] pour
    envoyer l'image.
 1. Éteignez la radio.
2. Connectez le câble à la prise jack ACC.
3. Appuyez et maintenez les touches [MODE &lt;] et [MODE &gt;] pendant
     que vous allumez la radio ("CLONE MODE" va apparaître sur
     l'afficheur).
4. <b>Après avoir cliqué sur OK</b>, appuyez sur la touche [A] pour envoyer l'image.
 1. Éteignez la radio.
2. Connectez le câble à la prise jack ACC.
3. Appuyez et maintenez les touches [MODE &lt;] et [MODE &gt;] pendant
     que vous allumez la radio ("CLONE MODE" va apparaître sur
     l'afficheur).
4. Cliquez ici sur OK.
    ("Receiving" va apparaître sur l'écran LCD).

 1. Éteignez la radio.
2. Connectez le câble à la prise jack ACC.
3. Appuyez et maintenez les touches [MODE &lt;] et [MODE &gt;] pendant
     que vous allumez la radio ("CLONE MODE" va apparaître sur
     l'afficheur).
4. Appuyez sur la touche [A](RCV) ("receiving" va apparaître sur l'écran LCD)
 1. Éteignez la radio.
2. Connectez le câble à la prise jack ACC.
3. Appuyez et maintenez les touches [MODE &lt;] et [MODE &gt;] pendant
     que vous allumez la radio ("CLONE MODE" va apparaître sur
     l'afficheur).
4. Appuyez sur la touche [C] ("RX" va apparaître sur l'écran LCD)
 1. Éteignez la radio.
2. Connectez le câble à la prise jack CAT/LINEAR.
3. Appuyez et maintenez les touches [MODE &lt;] et [MODE &gt;] tout
     en allumant la radio ("CLONE MODE" va apparaître sur
     l'afficheur).
4. <b>Après avoir cliqué sur OK</b>,
     appuyez sur la touche [C](SEND) pour envoyer l'image.
 1. Éteignez la radio.
2. Connectez le câble à la prise jack DATA.
3. Appuyez et maintenez la touche [V/M] à gauche durant l'allumage
     de la radio.
4. Tournez le rotacteur DIAL de droite pour sélectionner "CLONE START".
5. Appuyez sur la touche [SET]. L'affichage va disparaître
     un instant, puis la mention "CLONE" va apparaître.
6. <b>Après avoir cliqué sur OK</b>, appuyez la touche [V/M] à gauche
     pour envoyer l'image.
 1. Éteignez la radio.
2. Connectez le câble à la prise jack DATA.
3. Appuyez et maintenez la touche [V/M] à gauche durant l'allumage
     de la radio.
4. Tournez le rotacteur DIAL de droite pour sélectionner "CLONE START".
5. Appuyez sur la touche [SET]. L'affichage va disparaître
     un instant, puis la mention "CLONE" va apparaître.
6. Appuyez sur la touche [LOW] à gauche ("CLONE -RX-" va apparaître
     sur l'afficheur).

 1. Éteignez la radio.
2. Connectez le câble à la prise jack DATA.
3. Appuyez et maintenez la touche [FW] en allumant la radio
     ("CLONE" va apparaître sur l'afficheur).
4. <b>Après avoir cliqué sur OK</b>, appuyez sur la touche [BAND] pour envoyer l'image.
 1. Éteignez la radio.
2. Connectez le câble à la prise jack DATA.
3. Appuyez et maintenez la touche [FW] en allumant la radio
     ("CLONE" va apparaître sur l'afficheur).
4. Appuyez sur la touche [MODE] ("-WAIT-" va apparaître sur l'écran LCD).
 1. Éteignez la radio.
2. Connectez le câble à la prise DATA.
3. Appuyez et maintenez la touche [MHz(PRI)] en allumant
     la radio.
4. Tournez le bouton de sélection de fréquence jusque "F-7 CLONE".
5. Appuyez et maintenez la touche [BAND(SET)]. L'affichage
     va disparaître un instant puis l'indication "CLONE"
     va apparaître.
6. Appuyez sur la touche [LOW(ACC)] ("--RX--" will appear on the display).
 1. Éteignez la radio.
2. Connectez le câble à la prise DATA.
3. Appuyez et maintenez la touche [MHz(PRI)] en allumant
     la radio.
4. Tournez le bouton de sélection de fréquence jusque "F-7 CLONE".
5. Appuyez et maintenez la touche [BAND(SET)]. L'affichage
     va disparaître un instant puis l'indication "CLONE"
     va apparaître.
6. <b>Après avoir cliqué sur OK</b>, appuyez sur la touche [V/M(MW)] pour envoyer l'image.
 1. Éteignez la radio.
2. Connectez le câble à la prise DATA.
3. Appuyez et maintenez la touche [F] en allumant la radio
     ("CLONE" va apparaître sur l'écran LCD).
4. <b>Après avoir cliqué sur OK</b>, appuyez sur la touche [BAND] pour envoyer l'image.
 1. Éteignez la radio.
2. Connectez le câble à la prise DATA.
3. Appuyez et maintenez la touche [F] en allumant la radio
     ("CLONE" va apparaître sur l'écran LCD).
4. Appuyez sur la touche [Dx] ("-WAIT-" va apparaître sur l'écran LCD).
 1. Éteignez la radio.
2. Connectez le câble à la prise DATA.
3. Appuyez et maintenez la touche [MHz(SETUP)] en allumant la radio
      ("CLONE" va apparaître sur l'écran LCD).
4. <b>Après avoir cliqué sur OK</b>, appuyez sur la touche [REV(DW)]
     pour envoyer l'image.
 1. Éteignez la radio.
2. Connectez le câble à la prise DATA.
3. Appuyez et maintenez la touche [MHz(SETUP)] en allumant la radio
      ("CLONE" va apparaître sur l'écran LCD).
4. Appuyez sur la touche [MHz(SETUP)]
      ("-WAIT-" va apparaître sur l'écran LCD).
 1. Éteignez la radio.
2. Connectez le câble à la prise jack MIC.
3. Appuyez et maintenez la touche [MHz(SETUP)] en allumant la radio
      ("CLONE" va apparaître sur l'écran LCD).
4. Après avoir cliqué sur OK</b>, appuyez sur la touche [GM(AMS)] pour envoyer l'image.
 1. Éteignez la radio.
2. Connectez le câble à la prise jack MIC.
3. Appuyez et maintenez la touche [MHz(SETUP)] en allumant la radio
      ("CLONE" va apparaître sur l'écran LCD).
4. Appuyez sur la touche [MHz(SETUP)]
      ("-WAIT-" va apparaître sur l'écran LCD).
 1. Éteignez la radio.
2. Connectez le câble à la prise jack MIC/EAR.
3. Appuyez et maintenez la touche [F/W] en allumant la radio
      ("CLONE" va apparaître sur l'écran LCD).
4. Après avoir cliqué sur OK</b>, appuyez sur la touche [VFO(DW)SC] pour 
    recevoir l'image depuis la radio.
 1. Éteignez la radio.
2. Connectez le câble à la prise jack MIC/EAR.
3. Appuyez et maintenez la touche [F/W] en allumant la radio
      ("CLONE" va apparaître sur l'écran LCD).
4. Appuyez sur la touche [MR(SKP)SC] ("CLONE WAIT" va apparaître
      sur l'écran LCD).
5. Cliquez sur OK pour envoyer l'image vers la radio.
 1. Éteignez la radio.
2. Connectez le câble à la prise jack MIC/SP.
3. Appuyez et maintenez la touche [PTT] en allumant
      la radio.
4. <b>Après avoir cliqué sur OK</b>, appuyez sur la touche [PTT] pour envoyer l'image.
 1. Éteignez la radio.
2. Connectez le câble à la prise jack MIC/SP.
3. Appuyez et maintenez la touche [PTT] en allumant
      la radio.
4. Appuyez sur la touche [ONI] ("-WAIT-" va apparaître sur l'écran LCD).
5. Appuyez sur OK.
 1. Éteignez la radio.
2. Connectez le câble à la prise jack MIC/SP.
3. Appuyez et maintenez la touche [F/W] en allumant la radio
      ("CLONE" va apparaître sur l'écran LCD).
4. <b>Après avoir cliqué sur OK</b>, appuyez sur la touche [BAND] pour envoyer l'image.
 1. Éteignez la radio.
2. Connectez le câble à la prise jack MIC/SP.
3. Appuyez et maintenez la touche [F/W] en allumant la radio
      ("CLONE" va apparaître sur l'écran LCD).
4. Appuyez sur la touche [V/M] ("-WAIT-" va apparaître sur l'écran LCD).
 1. Éteignez la radio.
2. Connectez le câble à la prise jack MIC/SP.
3. Appuyez et maintenez la touche [MON-F] en allumant la radio
      ("CLONE" va apparaître sur l'écran LCD).
4. <b>Après avoir cliqué sur OK</b>, appuyez sur la touche [BAND] pour envoyer l'image.
 1. Éteignez la radio.
2. Connectez le câble à la prise jack MIC/SP.
3. Appuyez et maintenez la touche [MON-F] en allumant la radio
      ("CLONE" va apparaître sur l'écran LCD).
4. Appuyez sur la touche [V/M)] ("CLONE WAIT" va apparaître sur l'écran LCD).
 1. Éteignez la radio.
2. Connectez le câble à la prise jack MIC/SP.
3. Appuyez et maintenez la touche [MONI] en allumant
      la radio
4. Tournez le bouton de sélection pour choisir "F8 CLONE".
5. Appuyez brievement sur la touche [F/W].
6. <b>Après avoir cliqué sur OK</b>, appuyez sur la touche [PTT]
     pendant une seconde pour envoyer l'image.
 1. Éteignez la radio.
2. Connectez le câble à la prise jack MIC/SP.
3. Appuyez et maintenez la touche [MONI] en allumant
      la radio.
4. Tournez le bouton de sélection pour choisir "F8 CLONE".
5. Appuyez brievement sur la touche [F/W].
6. Appuyez sur la touche [MONI] ("--RX--" va apparaître sur l'écran LCD).
 1. Éteignez la radio.
2. Connectez le câble à la prise jack MIC/SP.
3. Appuyez et maintenez la touche [moni] en allumant la radio
4. Sélectionnez CLONE dans le menu puis appuyez sur F. La radio redémarre en mode clonage.
      ("CLONE" va apparaître sur l'écran LCD).
4. <b>Après avoir cliqué sur OK</b>, appuyez sur la touche [BAND] pour envoyer l'image.
    ("-TX-" va apparaître sur l'écran LCD). 
 1. Éteignez la radio.
2. Connectez le câble à la prise jack micro.
3. Appuyez et maintenez la touche [LOW(A/N)] en allumant la radio.
4. <b>Après avoir cliqué sur OK</b>, appuyez sur la touche [MHz(SET)] pour envoyer l'image.
 1. Éteignez la radio.
2. Connectez le câble à la prise jack micro.
3. Appuyez et maintenez la touche [LOW(A/N)] en allumant la radio.
4. Appuyez sur la touche [D/MR(MW)] ("-WAIT-" va apparaître sur l'écran LCD).
 1. Éteignez la radio.
2. Connectez le câble à la prise jack micro.
3. Appuyez et maintenez les touches [MHz], [LOW], et [D/MR]
      en allumant la radio.
4. <b>Après avoir cliqué sur OK</b>, appuyez sur la touche [MHz(SET)] pour envoyer l'image.
 1. Éteignez la radio.
2. Connectez le câble à la prise jack micro.
3. Appuyez et maintenez les touches [MHz], [LOW], et [D/MR]
      en allumant la radio.
4. Appuyez sur la touche [D/MR(MW)] ("-WAIT-" va apparaître sur l'écran LCD).
 1. Éteignez la radio.
2. Connectez le câble au connecteur HP/micro.
3. Assurez-vous que le connecteur est fermement branché.
4. Allumez la radio (le volume devra peut-être être reglé à 100 %).
5. Assurez-vous que la radio est réglée sur un canal sans activité.
6. Cliquez sur OK pour télécharger l'image depuis l'appareil.
 1. Éteignez la radio.
2. Connectez le câble au connecteur HP/micro.
3. Assurez-vous que le connecteur est fermement branché.
4. Allumez la radio (le volume devra peut-être être réglé à 100 %).
5. Assurez-vous que la radio est réglée sur un canal sans activité.
6. Cliquez sur OK pour télécharger l'image vers l'appareil.
 1. Éteignez la radio.
2. Connectez le câble au connecteur HP/micro.
3. Assurez-vous que le connecteur est fermement branché.
4. Allumez la radio.
5. Assurez-vous que la radio est réglée sur un canal sans activité.
6. Cliquez sur OK pour télécharger l'image depuis l'appareil.
 1. Éteignez la radio.
2. Connectez le câble au connecteur HP/micro.
3. Assurez-vous que le connecteur est fermement branché.
4. Allumez la radio.
5. Assurez-vous que la radio est réglée sur un canal sans activité.
6. Cliquez sur OK pour télécharger l'image vers l'appareil.
 1. Éteignez la radio.
2. Connectez le câble de données.
3. Preparez la radio pour le clonage.
4. <b>Après avoir cliqué sur OK</b>, appuyez la touche pour recevoir l'image.
 1. Éteignez la radio.
2. Connectez le câble de données.
3. Preparez la radio pour le clonage.
4. Appuyez la touche pour recevoir l'image.
 1. Éteignez la radio.
2. Connectez le micro puis maintenez sa touche [ACC] en allumant la radio
    ("CLONE" va apparaître sur l'écran LCD)
3. Remplacez le micro par le câble de programmation.
4. <b>Après avoir cliqué sur OK</b>, appuyez sur la touche [SET] pour envoyer l'image.
 1. Éteignez la radio.
2. Connectez le micro puis maintenez sa touche [ACC] en allumant la radio
    ("CLONE" va apparaître sur l'écran LCD)
3. Remplacez le micro par le câble de programmation.
4. Appuyez sur la touche [DISP/SS]
    ("R" va apparaître en bas à gauche de l'écran LCD).
 1. Éteignez la radio.
2. Détachez la façade.
3. Connectez le câble de données à la radio, utilisez le même connecteur
     que celui utilisé pour la façade, <b>pas le connecteur micro</b>.
4. Ciquez sur OK.
 1. Éteignez la radio.
3. Appuyez et maintenez la touche [moni] pendant que la radio s'allume.
4. Sélectionnez CLONE dans le menu, puis appuyez sur F. La radio redémarre en mode clonage.
     ("CLONE" va apparaître sur l'afficheur).
5. Appuyez sur la touche [moni] ("-RX-" va apparaître sur l'écran LCD).
 1. Éteignez la radio.
2. Connectez le câble à la prise DATA.
3. Détachez la batterie.
4. Appuyez et maintenez la touche [AMS] et le bouton d'allumage en 
reconnectant la batterie ("ADMS" va apparaître sur l'écran LCD).
5. Appuyer sur la touche [MODE] ("-WAIT-" va apparaître sur l'écran LCD). <b>Puis cliquez OK</b> 1. Éteignez la radio.
2. Connectez le câble à la prise DATA.
3. Détachez la batterie.
4. Appuyez et maintenez la touche [AMS] et le bouton d'allumage en 
reconnectant la batterie ("ADMS" va apparaître sur l'écran LCD).
5. <b>Après avoir cliqué sur OK</b>, appuyez sur la touche [BAND].
 1. Allumez la radio.
2. Connectez le câble au connecteur HP/MIC.
3. Assurez-vous que le connecteur est fermement branché.
4. Cliquez sur OK pour lire l'image depuis la radio.

Cela peut ne pas fonctionner si vous allumez la radio avec le câble déjà branché
 1. Allumez la radio.
2. Connectez le câble au connecteur HP/MIC.
3. Assurez-vous que le connecteur est fermement branché.
4. Cliquez sur OK pour télécharger l'image vers l'appareil.

Cela peut ne pas fonctionner si vous allumez la radio avec le câble déjà branché Une nouvelle version de CHIRP est disponible. Visitez le site internet dès que possible pour la télécharger! À propos À propos de CHIRP Tout Tous les fichiers Tous les formats supportés| Amateur Une erreur s'est produite Application des préférences Selon le système Modules disponibles Plan de fréquences Bandes Banques Corbeille Navigateur Établissement navigateur radio CHIRP doit être redémarré pour que la nouvelle sélection soit prise en compte Canada Modifier ce paramètre nécessite de rafraîchir tous les paramètres depuis l'image, ce qui va être fait maintenant. Les canaux avec TX et RX équivalents %s sont représentés par le mode de tonalité de "%s" Fichiers image Chirp Choix nécessaire Choisir code DTCS %s Choisir tonalité %s Choisir mode cross Choisir duplex Choisir le module à charger pour le problème %i: Ville Cliquez sur le commutateur "Special Channels" de l'éditeur de mémoire 
pour voir ou modifier les canaux EXT. Les canaux P-VFO 100-109
sont considérés comme paramètres.
Seul un sous-ensemble des 200 premiers paramètres radio
disponibles est suppporté dans cette version.
Ne tenez pas compte des bips de la radio durant le téléchargement.
 Clonage terminé, vérification des octets erronés Clonage Téléchargement depuis la radio - Lire Téléchargement vers la radio - Écrire Fermer Fermer fichier Mémoire groupement %i Mémoires groupement %i Commentaire Communiquer avec la radio Terminé Mode silence tonalité complexe ou non-standard (démarre l'outil de sélection de sélection de tonalité) Connectez votre câble d'interface au port PC à l'arrière
de l'unité 'TX/RX'. PAS au port de communication de la façade.
 Convertir en FM Copier Pays Mode cross Port personnalisé Personnalisation... Couper DTCS Polarité
DTCS Décodage DTMF Mémoire DV Danger Dec Supprimer Mode développeur Le mode développement est maintenant %s. CHIRP doit être redémarré pour que ceci prenne effet Comparaison mémoires brutes Code numérique Modes numériques Désactiver le rapport d'utilisation Désactivé Distance Ne plus demander pour %s Acceptez-vous les risques? Double-cliquer pour modifier le nom de la banque Télécharger Télécharger depuis la radio Télécharger depuis la radio... Instructions de téléchargement Pilote Informations sur les pilotes Messages du pilote Les répéteurs numériques bimodes supportant l'analogique seront présentés comme FM Duplex Éditer les details pour %i mémoires Editer les details pour la mémoire %i Activer l'édition automatique Activé Entrer la fréquence Saisir le décalage (MHz) Saisir la fréquence d'émission (MHz) Saisir un nouveau nom pour la banque %s: Saisir un port personnalisé: Mémoire %s effacée Erreur lors de l'application des préférences Erreur de communication avec la radio Pilote expérimental L'exportation ne peut écrire que des fichiers CSV Exporter vers un fichier CSV Exporter vers un fichier CSV... Extra Radio FM Base de données de répéteurs GRATUITE, qui fournit des informations
sur les répéteurs en Europe. Aucun compte n'est requis. Base de données LIBRE de relais proposant les informations
les plus à jour sur les relais européens. Un compte utilisateur
n'est pas requis. Échec du chargement du navigateur de la radio Échec d'analyse des résultats Caracteristiques Fichier inexistant: %s Fichiers Filtrer Filtrer les résultats avec emplacement correspondant a cette chaine de caractères Rechercher Rechercher le suivant Rechercher... Tâche radio %s terminée Suivez ces instructions pour télécharger la mémoire de la radio:
1 - Connectez votre câble d'interface
2 - Radio > Télécharger depuis la radio - Lire: NE PAS manipuler la radio
pendant le téléchargement!
3 - Déconnectez votre câble d'interface
 Suivez ces instructions pour télécharger la mémoire de la radio:
1 - Connectez votre câble d'interface
2 - Radio > Télécharger depuis la radio - Lire: ne modifiez aucun réglage
sur la façade de la radio!
3 - Déconnectez votre câble d'interface
 Suivez ces instructions pour télécharger la mémoire de la radio:
1 - Éteignez votre radio
2 - Connectez votre câble d'interface
3 - Allumez votre radio, volume @ 50%
4 - CHIRP Menu - Radio - Télécharger depuis la radio - Lire
 Suivez ces instructions pour télécharger votre configuration:
1 - Éteignez votre radio
2 - Connectez votre câble d'interface au jack haut-parleur
3 - Allumez votre radio
4 - Radio > Télécharger depuis la radio - Lire
5 - Déconnectez votre câble d'interface! Autrement il n'y aura
    pas d'audio du côté droit!
 Suivez ces instructions pour télécharger vos infos:
1 - Éteignez votre radio
2 - Connectez votre câble d'interface
3 - Allumez votre radio
4 - Téléchargez les données depuis votre radio
 Suivez ces instructions pour télécharger depuis votre radio:
1 - Éteignez votre radio
2 - Connectez votre câble d'interface
3 - Allumez votre radio (déverrouillez-la si elle est protégée par mot de passe)
4 - Cliquez sur OK pour démarrer
 Suivez ces instructions pour lire le contenu de votre radio:
1 - Éteignez votre radio
2 - Connectez votre câble d'interface
3 - Allumez votre radio
4 - Cliquez sur OK pour démarrer
 Suivez ces instructions pour télécharger vers la mémoire de la radio:
1 - Connectez votre câble d'interface
2 - Radio > Télécharger vers la radio - Écrire: NE PAS manipuler la radio
pendant le téléchargement!
3 - Déconnectez votre câble d'interface
 Suivez ces instructions pour télécharger vers la mémoire de la radio:
1 - Connectez votre câble d'interface
2 - Radio > Télécharger vers la radio - Écrire: ne modifiez aucun réglage
sur la façade de la radio!
3 - Déconnectez votre câble d'interface
 Suivez ces instructions pour télécharger vers la mémoire de la radio:
1 - Éteignez votre radio
2 - Connectez votre câble d'interface
3 - Allumez votre radio, volume @ 50%
4 - CHIRP Menu - Radio - Télécharger vers la radio - Écrire
 Suivez ces instructions pour télécharger votre configuration:
1 - Éteignez votre radio
2 - Connectez votre câble d'interface au jack haut-parleur
3 - Allumez votre radio
4 - Radio > Télécharger vers la radio - Écrire
5 - Déconnectez votre câble d'interface! Autrement il n'y aura
    pas d'audio du côté droit!
6 - Éteignez puis rallumez votre radio pour sortir du mode de clonage
 Suivez ces instructions pour télécharger vos infos:
1 - Éteignez votre radio
2 - Connectez votre câble d'interface
3 - Allumez votre radio
4 - Téléchargez les données vers votre radio
 Suivez ces instructions pour télécharger vers votre radio:
1 - Éteignez votre radio
2 - Connectez votre câble d'interface
3 - Allumez votre radio (déverrouillez-la si elle est protégée par mot de passe)
4 - Cliquez sur OK pour démarrer
 Suivez ces instructions pour écrire dans votre radio:
1 - Éteignez votre radio
2 - Connectez votre câble d'interface
3 - Allumez votre radio
4 - Cliquez sur OK pour démarrer
 Suivez ces instructions pour télécharger vos infos:
1 - Éteignez votre radio
2 - Connectez votre câble d'interface
3 - Allumez votre radio
4 - Téléchargez les données depuis votre radio
 Suivez ces instructions pour lire le contenu de votre radio:
1 - Éteignez votre radio
2 - Connectez votre câble d'interface
3 - Allumez votre radio
4 - Téléchargez les données depuis votre radio
 Suivez ces instructions pour télécharger vos infos:
1 - Éteignez votre radio
2 - Connectez votre câble d'interface
3 - Allumez votre radio
4 - Téléchargez les données vers votre radio
 Suivez ces instructions pour écrire dans votre radio:
1 - Éteignez votre radio
2 - Connectez votre câble d'interface
3 - Allumez votre radio
4 - Téléchargez les données vers votre radio
 Liste de valeurs vide trouvée pour %(name)s: %(value)r Fréquence Granularité fréquence en kHz GMRS Acquisition des préférences Aller à la mémoire Aller à la mémoire: Aller... Aide Aidez-moi... Hex Cacher les mémoires vides Commentaire pour humains (pas stocké dans la radio) Si sélectionné, trier les résultats selon la distance depuis ces coordonnées Importer Importer depuis un fichier... Importer les messages Import non recommandé Index Info Information Insérer une ligne avant Installer une icône sur le bureau? Intéragir avec le pilote Erreur interne du pilote Invalide %(value)s (utiliser degrés decimaux) Entrée invalide Code postal invalide Édition invalide: %s Fichier module invalide ou pas supporté Valeur invalide: %r Problème numero: LIVE Changer la langue Latitude La longueur doit être %i Limiter les bandes Limiter les modes Limiter les états Limiter les résultats à cette distance (km) depuis les coordonnées Radio en direct Chargement module... Chargement module pour un problème Chargement module pour un problème... Charger un module n'affectera pas les onglets ouverts. Il est recommandé (sauf instructions contraires) de fermer tous les onglets avant de charger un module. Charger des modules peut être extrêmement dangereux, causant des dommages à votre ordinateur, radio ou les deux. NE JAMAIS charger un module issu d'une source dont vous n'avez pas confiance, et plus spécialement d'ailleurs que du site internet principal de CHIRP (chirpmyradio.com). Charger un module depuis une autre source revient à donner un accès direct à votre ordinateur et à tout ce qu'il contient! Continuez malgré le risque? Chargement des préférences Texte logo 1 (12 caractères) Texte logo 2 (12 caractères) Longitude Mémoires La mémoire %i n'est pas supprimable Texte mémoire (stocké dans la radio) La mémoire doit être dans une banque pour être éditée Mémoire {num} pas dans la banque {bank} Mode Modèle Modes Module Module chargé Module chargé avec succès Plus d'un port trouvé: %s Descendre Monter Les déplacements sont désactivés lorsque la vue est triée Nouvelle fenêtre Nouvelle version disponible Pas de ligne vide dessous! Aucun module trouvé Aucun module trouvé pour le problème %i Aucun résultat Aucun résultat! Nombre Décalage Seulement certaines bandes Seulement certains modes Seuls les onglets des mémoires peuvent être exportés Uniquement les répéteurs en fonctionnement Ouvrir Ouvrir récent Ouvrir base de données Ouvrir un fichier Ouvrir un module Ouvrir le fichier de débogage Ouvrir dans une nouvelle fenêtre Ouvrir le repertoire de base de données Option Optionnel: -122.0000 Optionnel: 100 Optionnel: 45.0000 Optionnel: Comté, Hôpital, etc. Écraser les mémoires? Les canaux P-VFO 100-109 sont considérés comme paramètres.
Seul un sous-ensemble des plus de 130 paramètres radio disponibles
est pris en charge dans cette version.
 Analyse Coller Les mémoires collées vont écraser %s mémoires existantes Les mémoires collées vont écraser les mémoires %s Les mémoires collées vont écraser la mémoire %s La mémoire collee va écraser la mémoire %s Soyez certain de quitter CHIRP avant d'installer la nouvelle version! Suivez ces étapes avec attention:
1 - Allumez votre radio
2 - Connectez le câble d'interface à votre radio
3 - Cliquez sur le bouton de cette fenêtre pour démarrer le téléchargement
    (vous devriez voir une autre boîte de dialogue, cliquez sur OK)
4 - La radio va sonner et la LED clignoter
5 - Vous disposez de 10 secondes pour appuyer sur "MON" avant que
    le téléchargement vers la radio ne démarre
6 - Si tout se déroule bien la radio va sonner à la fin.
Après le clonage débranchez le câble puis éteignez et rallumez votre radio
pour revenir au fonctionnement normal.
 Suivez ces étapes avec attention:
1 - Allumez votre radio
2 - Connectez le câble d'interface à votre radio
3 - Cliquez sur le bouton de cette fenêtre pour démarrer le téléchargement
    (La radio va sonner et la LED clignoter)
4 - Ensuite appuyez sur le bouton "A" de votre radio pour démarrer le clonage
    (À la fin la radio va sonner)
 Veuillez noter que le mode developpeur est destiné à n'être utilisé que par les développeurs du projet CHIRP, ou en suivant les directives d'un developpeur. Il active des comportements et des fonctions qui peuvent endommager votre ordinateur et votre radio s'ils ne sont pas utilisés avec un EXTRÊME soin. Vous êtes prévenu! Continuer malgré tout? Patientez s'il vous plaît Branchez votre câble et cliquez sur OK Port Puissance Appuyez sur entrée pour inscrire ceci en mémoire Apercu avant impression Impression Propriétés Interroger %s Interroger source web RX DTCS Radio La radio n'a pas accusé reception du bloc %i Informations de la radio La radio a envoyé des données après le dernier bloc attendu, ceci se produit quand le modèle sélectionné n'est pas US mais que la radio est US. Sélectionnez le bon modèle et réessayez. RadioReference Canada nécessite une connexion avant une requête RadioReference.com est le plus grand fournisseur
de données de communications radio du monde
<small>Compte Premium requis</small> Recevoir code DTCS Fréquence de réception Rafraîchissement nécessaire Mémoire %s rafraîchie Recharger pilote Recharger pilote et fichier Renommer banque RepeaterBook est l'annuaire GRATUIT de radio amateur
le plus complet du monde. Rapport d'utilisation activé Le rapport d'utilisation aide le projet CHIRP à savoir quelles radios et systèmes d'exploitation sont utilisés pour concentrer les efforts sur ceux-là. Nous apprécierions vraiment que vous le laissiez actif. Êtes-vous certain de vouloir désactiver le rapport d'utilisation? Redémarrage nécessaire Restaurer %i onglets Restaurer %i onglets Restaurer les onglets au démarrage Préférences récupérées Enregistrer Enregistrer avant de fermer? Enregistrer fichier Préférences enregistrées Contrôles de scan (sauter, inclure, priorité, etc) Lists de scan Brouilleur Risque de sécurité Choix d'un plan de fréquences... Choisir bandes Choisir langue Choisir modes Choisir un plan de fréquences Service Préférences Décalage (ou fréquence de transmission) contrôlée par duplex Afficher les données de mémoires brutes Montrer l'emplacement du fichier de débogage Montrer les champs supplémentaires Montrer l'emplacement du fichier de débogage Ignorer Des mémoires sont incompatibles avec cette radio Des mémoires ne sont pas supprimables Tri %i mémoire Tri %i mémoires Tri croissant %i mémoire Tri croissant %i mémoires Tri décroissant %i mémoire Tri décroissant %i mémoires Tri par colonne: Tri des mémoires Tri État État/Province Réussi Polarité DTCS TX-RX (normale ou inversée) Le réseau mondial DMR-MARC Le pilote du FT-450D charge l'onglet des "Special Channels'
avec les mémoires de plage de balayage PMS (groupe 11), les canaux
60 metres (groupe 12), la mémoire QMB (STO/RCL), les mémoires HOME HF et
50 m et toutes les mémoires VFO A et B.
Il y a des mémoires VFO pour les dernières fréquences régléess sur
chaque bande. La dernière configuration de réglage de mémoire est également stockée.
Ces canaux spéciaux permettent une édition reduite des champs.
Ce pilote remplit également l'onglet "Other" de la fenêtre
Propriétés du canal mémoire. Cet onglet contient des valeurs pour
les paramètres de canal mémoire qui ne correspondent pas aux colonnes d'affichage
Chirp standard. Avec la prise en charge du FT450, l'interface graphique utilise
désormais le mode 'DIG' pour représenter les modes de données et une nouvelle colonne
'DATA MODE' pour sélectionner USER-U, USER-L et RTTY 
 Le pilote X3Plus est actuellement expérimental.
Il n'y a pas de problème connu mais vous devriez proceder avec précaution.
Sauvegardez une copie non-editée de votre premier
téléchargement dans un fichier image radio CHIRP (*.img).
 L’auteur de ce module n'est pas un développeur de CHIRP connu. Comme il peut comporter un risque de sécurité, il est recommandé de ne pas importer ce module. Importer tout de même? La procedure préconisée d'importation des mémoires consiste à ouvrir le fichier source et copier/coller les mémoires depuis celui-ci vers l'image cible. Si vous continuez avec cette fonction d'importation, CHIRP va remplacer toutes les mémoires du fichier actuellement ouvert avec celles de %(file)s. Souhaitez-vous ouvrir ce fichier pour copier/coller entre eux ou l'importer? Cette mémoire Ce pilote a été testé avec la v3 de l'ID-5100. Si votre radio n'est pas à jour merci d'aider en ouvrant un rapport de bogue avec un journal de débogage pour que nous puissions ajouter du support pour les autres révisions. Ce pilote est en développement et devrait être considéré comme expérimental. Cette image ne contient pas d'information sur le firmware. Elle a peut-être été générée avec une vieille version de CHIRP ou une version modifiée. Il est recommandé de télécharger une nouvelle image fraîche depuis la radio et de l'utiliser à l'avenir pour une meilleure sécurité et compatibilité. Cette radio fonctionne en mode live, ce qui signifie que les changements sont envoyés à la radio en temps réel dès qu'ils sont faits. Un téléchargement vers la radio n'est pas nécessaire! Ce fichier est indépendant d'une radio et ne peut pas être téléchargé directement vers une radio. Ouvrez une image radio (ou téléchargez-en une de la radio) et copiez/collez les entrées de cet onglet vers l'autre afin de pouvoir télécharger vers la radio Ceci est un pilote récent en version beta
 Ceci est un pilote récent en version beta - chargement à vos risques et périls
 Ce pilote est expérimental pour la Quansheng UV-K5. Il pourrait endommager votre radio, voire pire. À utilisez à vos risques et périls.

Avant d'essayer de faire des changements, veuillez télécharger l'image mémoire de la radio avec CHIRP et conservez-la précieusement. Elle pourra être utilisée pour remettre la radio à son état d'origine.

Certains détails ne sont pas encore implémentés Ceci est un support expérimental pour le BJ-9900 encore en cours de développement.
SVP assurez-vous de disposer d'une sauvegarde correcte avec le logiciel d'origine (OEM).
Envoyez aussi vos demandes d'améliorations ou rapports de bogues!
Vous avez été prévenu. Procedez à vos risques et périls! Cette mémoire et décaler tous vers le haut Cette mémoire et décaler le bloc vers le haut Cette option pourrait briser votre radio! Chaque radio a un ensemble de données de calibration unique. Télécharger les données de l'image vers la radio pourrait l'endommager physiquement si l'image provient d'une radio différente. À n'utiliser que si vous savez ce que vous faites et que acceptez les risque de destruction de votre radio! Cette radio rentre en mode programmation avec une méthode particulière,
même le logiciel d'origine fait plusieurs tentatives pour y parvenir.
Je vais essayer 8 fois (la plupart du temps ~3 fois suffisent) et cela pourrait
prendre quelques secondes. Si cela ne fonctionne pas, essayez encore quelques fois.
Si vous ne parvenez pas à entrer, vérifiez la radio et le câble.
 Ceci ne devrait être activé que si vous utilisez un firmware modifié supportant une plus large couverture en fréquences. L'activation de cette option empechera CHIRP d'appliquer les restrictions OEM et peut conduire à un comportement indéfini ou non réglementaire. À utiliser à vos risques et périls! Ceci chargera un module pour un problème signalé sur le site web Tonalité Mode tonalité Silence tonalité Mode silence tonalité Puissance de transmission Décalage transmission, mode séparé, ou désactivation transmission Tonalité transmission Code DTCS transmission/réception pour mode DTCS, ou bien code de transmission Modulation transmission/réception (FM, AM, SSB, etc) Tonalité transmission/réception pour mode TSQL, ou bien tonalité réception Pas de réglage Rechercher port USB Impossible de déterminer le port de votre câble. Vérifiez les pilotes et connexions. Impossible d'éditer une mémoire avant téléchargement de la radio Impossible de trouver la configuration de base %r Impossible d'importer lorsque la vue est triée Impossible d'ouvrir le presse-papier Impossible d'interroger Impossible de lire le dernier bloc. Ceci se produit fréquemment quand le modèle sélectionné est US mais que la radio n'est pas US (ou large-bande). Sélectionnez le bon modèle et reessayez. Impossible de montrer %s sur ce système Impossible de régler %s dans cette mémoire Impossible de télécharger ce fichier Etats-Unis Débranchez votre câble (si nécessaire) et cliquez sur OK Modèle non supporté %r Instruction de téléchargement Télécharger vers la radio Télécharger vers la radio... Memoire %s téléchargée Utiliser une police à largeur fixe Utiliser une plus grande police La valeur ne rentre pas dans %i bits La valeur doit être d'au moins %.4f La valeur ne doit pas dépasser %.4f La valeur doit comporter exactement %i décimales La valeur doit être zero ou positive Valeurs Fabricant Voir ATTENTION! Attention Attention: %s Bienvenue Souhaitez-vous que CHIRP installe une icône pour vous sur le bureau? Votre câble apparaît sur le port: bits octets octets chacun desactivé activé {bank} pleine 