��    !     $  �  ,"      �-  -   �-  9   �-     �-  ;   .  +   C.  )   o.  &   �.     �.  I   �.     )/     0/  6   ?/    v/  �   �0  �  *1  �   �2  �   M3  �   �3  �   �4    h5  �   6    t7  �   z8  �   t9  �   l:  �   h;    X<  o  d=  q  �>  �   F@  �   "A  p  �A  e  iC  �   �D    �E  �   �F  �   �G  �   nH  �   ^I  �   KJ  �   6K  �   L  	  M  �   &N  �   �N  �   �O  �   �P  �   [Q  �   <R  #  S    >T  M  JU  �   �V  �   SW  �   
X  �   �X    �Y    �Z  �   �[  �   �\  �   p]  m   �]  �   _^  �   >_  �   (`  �   �`    �a  �   �b  �   �c  �   �d  ^   �e  %   �e     "f     (f     4f     :f  	   >f     Hf     _f     }f     �f     �f     �f     �f     �f     �f     �f     �f     �f     �f     g     #g     /g     <g  <   Sg  y   �g     
h  ]   h  J   oh     �h     �h     �h     �h     �h     i     $i     :i  (   Hi     qi      vi    �i  ,   �j     �j     �j     �j     k     k  
   "k  (   -k  %   Vk     |k     �k     �k  Q   �k  k   �k     bl     pl     ul  
   }l     �l  	   �l     �l     �l     �l     �l     �l  	   �l     �l     �l     �l     �l     m     m  A   'm     im     {m     �m     �m     �m     �m     �m     �m     �m      	n     *n     3n     Gn     ^n     tn     {n     �n  C   �n     �n     �n     �n     o     2o     Io     Qo     ao     to     �o     �o  �   �o  �   Bp  (   q  +   Fq  �   rq     �q     r     r     3r     Rr     ar     yr     �r     �r     �r     �r     �r  $   �r     s     -s     Ms     [s     ls     rs  e   {s  u   �s     Wt     tt     �t     �t     �t     �t     �t     �t     �t  1   �t  	   u     &u  	   +u     5u     =u     Ju     ju  �   �u  �   Kv  �   w    �w  �   �x  �   �y  �   Nz  �   �z  �   �{  �   l|  8  *}  �   c~  �     �   �  �   W�  �   ��  �   ��  �   H�  .   �  	   �  &   %�     L�  +   i�  (   ��  -   ��     �     �     �     �     �     #�  
   (�     3�     7�  Y   K�  ,   ��  7   ҄     
�     �     $�     8�     H�     _�     n�     t�     y�     ��     ��     ��     ��  '   ׅ     ��     �     �     /�  "   ?�     b�     t�     ��     ��     ��     ��     ��     ��     Æ     І  4   ߆  	   �  
   �     )�     8�     O�  �   i�  v  ��     g�     x�  .   ��     ��     ͉  	   �     ��     �  :   �     R�     m�     ��  %   ��     Њ     ��     ��     ��     �     �     �  	   1�     ;�     X�  	   i�     s�  5   {�  
   ��     ��     ҋ     �     ��     
�  7   '�  
   _�     j�     v�     }�     ��  
   ��     ��     ��     Ō      ی     ��     �     �     $�     6�     B�     P�     _�     r�     ��     ��     ��     ��     ƍ     ԍ     �     ��     
�      $�     E�  �   Y�     ��     �     �     ��  3   �  *   :�  (   e�  &   ��  ?   ��  �   ��  �  ��  (  ^�    ��     ��  $   ��     ̔     �     �     �  !   ��     �     *�     3�  
   G�     R�     `�     o�     {�     ��     ��     ��     ��     ˕     ԕ     ܕ     �     ��     �  �   �  ;   Ŗ  t   �     v�     ��     ��  	   ��     ��     ��     ŗ     ֗     �     ��     �     �     6�  W   B�     ��     ��     ̘  �   ޘ     ��     ��     Ǚ     ݙ     ��     	�     �  	   #�     -�  +   <�  	   h�  	   r�     |�     ��     ��     ��     ��     ǚ     ٚ     ��     	�     �  :   �  9   U�     ��     ��     ��     ɛ     �  .   �     �     8�     I�  3   i�  5   ��     Ӝ     �     �     ��     ��     �     �  (   4�  �   ]�     �  �  �  �   �  �   ��  �   X�  ,   P�  ^  }�     ܤ  �   �  G   ��  �   �  |   ܦ  �   Y�  #   !�  =   E�  <  ��  �   ��  V   ��     ��     �  +  8�  &  d�  �   ��  r  z�  ,   ��     �  	   �     )�     6�     H�  /   W�     ��  <   ��  .   Ұ  6   �     8�  @   D�     ��  L   ��  ,   �     �  )   .�     X�     u�  �   ��  "   /�     R�     r�     ��     ��  /   ��     г     �     ��     �  6    �     W�     g�     z�     ��     ��     ��     ��     ٴ     ��  '   �     8�      V�     w�     ~�     ��     ��     ��     ��     ��  �   ��  7   d�  �   ��  �   S�     ��  !   �     (�     -�  
   3�     >�     G�     O�  r  ^�  4   ѹ  9   �     @�  A   V�  4   ��  )   ͺ  *   ��     "�  E   @�  	   ��     ��  G   ��  u  �  �   ]�    �  �   /�  �   ��  �   ��    p�  D  ��    ��  $  ��    �    .�    A�    Y�  /  e�  �  ��  �  A�    ��  �   ��  �  ��  �  e�  
  ��  1  ��     0�  �   1�    "�    3�    9�    G�    K�  9  e�  �   ��  �   {�    ]�  �   `�    T�  �   Y�  T  S�  0  ��  �  ��  �   w�  �   Y�  �   /�  �   #�  _  �  ^  p�  &  ��  %  ��  �   �  �   ��  1  E�  <  w�  �   ��  $  ��  N  ��  '  ��    '�    7�  J   E�  0   ��     ��     ��     ��     ��     ��     �  #   "�     F�     S�     n�     ��     ��     ��     ��     ��     ��     ��  1   ��     �     )�     9�  !   J�  G   l�  �   ��     N�  q   U�  T   ��          3     D     ]     o     �     �     �  )   �     �  1   �  J  . /   y 
   �    �    �    �    �    � 6    )   D    n    w    � n   � �   
    �    �    �    �    �    �    �    �    �          
       *    <    @    H    X    q b   �    �     �        -    ;    P    ]    f    � 1   �    �    �    �    �            3 S   G    �    � !   � $   �    � 	       %    ;    S '   r !   � �   � �   b	 2   I
 &   |
 �   �
    4 <   I D   � ,   �    �    	    "    /    ?    P    k    � #   �    � *   �    
        +    1 u   : �   � +   6 "   b ,   �    �    �    �    �    �    � B   � 	   A    K    Q    b    k '   x    � �   � �   � �   � 9  r �   � �   t �   N �   � �   � �   � _  � �    �   � �   � �   i �   ( �   � �   � 5   u 	   � 0   � #   � A   
  :   L  >   �     �     �     �     �     !    ! 
   !     !    $! ~   ;! B   �! G   �!    E"    M"    `"    s"    �"    �"    �"    �"    �"    �"    �"    #    "# :   <#    w#    �#    �#    �# +   �#    �#    $    $    ($ 
   /$    :$    V$    c$    o$    |$ :   �$ 
   �$    �$    �$    �$    % �   ,% �  �%    h'    �' <   �' !   �' !   �'    ( !   (    >( R   F( #   �( %   �( .   �( :   ) 1   M) 	   )    �) 	   �)    �)    �)    �)    �)    �)    *    * 	   !* T   +*    �*    �*    �*    �*    �* !   �* H   +    ^+    o+    �+    �+    �+ 
   �+    �+    �+    �+ 0   �+    ,    4,    9,    F,    c,    p,    ,    �,    �, ,   �, 	   �,    �,    -    -    %-    8-    K-    ^- !   y-    �- �   �-    S.    `.    i.    q. :   �. 3   �. 3   �. 1   $/ 5   V/ �   �/ Z  0   w2 J  �3 	   B5 #   L5     p5    �5    �5    �5 '   �5    �5    �5    �5 
   6    6    .6    F6    R6    _6 $   o6    �6 !   �6    �6    �6    �6 &   �6    7    17 �   F7 S   8 �   X8    �8    �8    9 
   9    '9    G9    M9    f9    |9    �9    �9 *   �9    �9 Z   �9    I:     f:    �: �   �:    s; )   �;    �;    �;    �;    �;    < 
   <    '< >   <<    {< 	   �<    �<    �<    �<    �<    �<    = *   =    G=    \=    e= W   r= :   �=    > !   >    :> )   R>    |> 2   �> #   �>    �> #   �> Q   ? U   b?    �?    �?    �?    �?    �?    �? *   @ *   1@ �   \@    A i  2A �   �D �   �E /  JF +   zG �  �G    :I   II L   KJ   �J �   �K �   TL *   EM V   pM ~  �M   FO V   OP %   �P )   �P Q  �P T  HR A  �S �  �T    iV    �V    �V    �V    �V    �V I   �V    )W U   >W 9   �W R   �W    !X K   6X    �X T   �X F   �X 1   .Y ;   `Y    �Y    �Y �   �Y )   �Z *   �Z     �Z    [    [ ;   ![    ][    w[    �[    �[ R   �[    \    .\    D\    X\    x\    �\    �\ !   �\ %   �\ 6   ] .   =] +   l]    �] 
   �] 
   �]    �] 
   �]    �] 	   �] �   �]     �^ �   �^ �   �_    }` $   �`    �`    �`    �`    �` 	   �`    �`    �   �   �      �  �  u   �   �  s   �  �       �       �  �      �              �  k   7       �  �  ^      e       b  +  =  q   �        �           �  �  �  �  F   �                     e      ~   �  �  �   1   �   _       N   �   �     �  @      �  �   Q      �   [       �  �   �   �       �  �        A   f  �          �  	   �      �   �   ]     �   K  �  d       `                     �   n         P    �  B     �   �   �  �         �      �   n   �      �     :     �       �   !      �   �  3  �   "       H   !   6  I             �   �  3   y       �   �       �  W       D      7  x      g  �   �    �   �  c  �       X  -     	  2  #  �  �      U  �  m  S            �   �      /  �  �  C   j     f   '  v  Z  �   �      N  �   �  L           `   �   G  �   ^   �     �      �  r       �   �  �      �           �  �           t   +   u  �  ,   �   �  �   {         �   '   �   �                             �  �  �          �       �      o   �   �   X   }  I      �      �  W  �     4  P   J    [  |   O   �  �  �   !  �    h  �  M   %  �   �        #      �   �   K   �  L      *  �         �  �   J                  8   )   �     �           �      ]  i          �       $  {      �  Y   O    �   
  �  t            �      �           �          M  �              �  :  9           �       Q       /      "          l   �              (   c       �  a  @   Z   �  �      �   6   �  �       �   A  �   4   �       �               .   �   j   �  �  �  -   �  g   0    ?  ~  �       �  �    �  �   �   �   �      �     �     ?   &      �        �  �  �   i       z  >   o  }   x   R  �         T  <       *   �  �   �      �              >            k  �  \  p      �           �   E  D   �   0           �                          a   b   �  �   C  	  �            �     R   �                    V  &   �   �           �   �  H  �   �          2   �   U   ;       �   �   �      �                 h      �  s      9   ,  (              �       �          l  �  �    1          �   <  8  �      m   �      5          �       d                      E   �  �   �         r    �  �    Y  �   �   �  
       =   �  �      �   �   �   �   |  �  G   �          �            �  �       \   �   p          q      F        �  �     �             �  �       �         �  T   �      �      �  �   �  v      �  �   �  �   w   �           �      �  $   )          �   ;          w  5  �  _    V       .  S   y  �              �       �  �   �     �   %   �      B          �   
  �  �     �    �      �  �         z          %(value)s must be between %(min)i and %(max)i %i Memories and shift all up %i Memories and shift all up %i Memory %i Memories %i Memory and shift block up %i Memories and shift block up %s has not been saved. Save before closing? (Describe what actually happened instead) (Describe what you expected to happen) (Describe what you were doing) (Has this ever worked before? New radio? Does it work with OEM software?) (none) ...and %i more 1. Connect programming cable to MIC jack.
2. Press OK. 1. Ensure your firmware version is 4_10 or higher
2. Turn radio off
3. Connect your interface cable
4. Turn radio on
5. Press and release PTT 3 times while holding MONI key
6. Supported baud rates: 57600 (default) and 19200
   (rotate dial while holding MONI to change)
7. Click OK
 1. Turn Radio off.
2. Connect data cable.
3. While holding "A/N LOW" button, turn radio on.
4. <b>After clicking OK</b>, press "SET MHz" to send image.
 1. Turn Radio off.
2. Connect data cable.
3. While holding "A/N LOW" button, turn radio on.
4. Press "MW D/MR" to receive image.
5. Make sure display says "-WAIT-" (see note below if not)
6. Click OK to dismiss this dialog and start transfer.
Note: if you don't see "-WAIT-" at step 5, try cycling
      power and pressing and holding red "*L" button to unlock
      radio, then start back at step 1.
 1. Turn Radio off.
2. Connect data cable.
3. While holding "F/W" button, turn radio on.
4. <b>After clicking OK</b>, press "RPT" to send image.
 1. Turn Radio off.
2. Connect data cable.
3. While holding "F/W" button, turn radio on.
4. Press "REV" to receive image.
5. Click OK to start transfer.
 1. Turn Radio off.
2. Connect data cable.
3. While holding "TONE" and "REV" buttons, turn radio on.
4. <b>After clicking OK</b>, press "TONE" to send image.
 1. Turn Radio off.
2. Connect data cable.
3. While holding "TONE" and "REV" buttons, turn radio on.
4. Press "REV" to receive image.
5. Make sure display says "CLONE RX" and green led is blinking
6. Click OK to start transfer.
 1. Turn radio off.
2. Connect cable
3. Press and hold in the MHz, Low, and D/MR keys on the radio while turning it on
4. Radio is in clone mode when TX/RX is flashing
5. <b>After clicking OK</b>, press the MHz key on the radio to send image.
    ("TX" will appear on the LCD). 
 1. Turn radio off.
2. Connect cable
3. Press and hold in the MHz, Low, and D/MR keys on the radio while turning it on
4. Radio is in clone mode when TX/RX is flashing
5. Press the Low key on the radio ("RX" will appear on the LCD).
6. Click OK. 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. <b>After clicking OK</b> here, press the [C.S.] key to
    send image.
 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. <b>After clicking OK</b>, press the [A] key to send image.
 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. Click OK here.
    ("Receiving" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. Press the [A](RCV) key ("receiving" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. Press the [C] key ("RX" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to CAT/LINEAR jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. <b>After clicking OK</b>,
     press the [C](SEND) key to send image.
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the "left" [V/M] key while turning the
     radio on.
4. Rotate the "right" DIAL knob to select "CLONE START".
5. Press the [SET] key. The display will disappear
     for a moment, then the "CLONE" notation will appear.
6. <b>After clicking OK</b>, press the "left" [V/M] key to
     send image.
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the "left" [V/M] key while turning the
     radio on.
4. Rotate the "right" DIAL knob to select "CLONE START".
5. Press the [SET] key. The display will disappear
     for a moment, then the "CLONE" notation will appear.
6. Press the "left" [LOW] key ("CLONE -RX-" will appear on
     the display).
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the [FW] key while turning the radio on
     ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [BAND] key to send image.
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the [FW] key while turning the radio on
     ("CLONE" will appear on the display).
4. Press the [MODE] key ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the [MHz(PRI)] key while turning the
     radio on.
4. Rotate the DIAL job to select "F-7 CLONE".
5. Press and hold in the [BAND(SET)] key. The display
     will disappear for a moment, then the "CLONE" notation
     will appear.
6. Press the [LOW(ACC)] key ("--RX--" will appear on the display).
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the [MHz(PRI)] key while turning the
 radio on.
4. Rotate the DIAL job to select "F-7 CLONE".
5. Press and hold in the [BAND(SET)] key. The display
 will disappear for a moment, then the "CLONE" notation
 will appear.
6. <b>After clicking OK</b>, press the [V/M(MW)] key to send image.
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold [DISP] key while turning on radio
     ("CLONE" will appear on radio LCD).
4. <b>After clicking OK here in Chirp</b>,
     press the [Send] screen button.
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold in [DISP] key while turning on radio
     ("CLONE" will appear on radio LCD).
4. Press [RECEIVE] screen button
     ("-WAIT-" will appear on radio LCD).
5. Finally, press OK button below.
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold in the [F] key while turning the radio on
     ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [BAND] key to send image.
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold in the [F] key while turning the radio on
     ("CLONE" will appear on the display).
4. Press the [Dx] key ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold in the [MHz(SETUP)] key while turning the radio
     on ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [REV(DW)] key
     to send image.
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold in the [MHz(SETUP)] key while turning the radio
     on ("CLONE" will appear on the display).
4. Press the [MHz(SETUP)] key
     ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC Jack.
3. Press and hold in the [MHz(SETUP)] key while turning the radio
     on ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [GM(AMS)] key
     to send image.
 1. Turn radio off.
2. Connect cable to MIC Jack.
3. Press and hold in the [MHz(SETUP)] key while turning the radio
     on ("CLONE" will appear on the display).
4. Press the [MHz(SETUP)] key
     ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC/EAR jack.
3. Press and hold in the [F/W] key while turning the radio on
    ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [VFO(DW)SC] key to receive
    the image from the radio.
 1. Turn radio off.
2. Connect cable to MIC/EAR jack.
3. Press and hold in the [F/W] key while turning the radio on
    ("CLONE" will appear on the display).
4. Press the [MR(SKP)SC] key ("CLONE WAIT" will appear
    on the LCD).
5. Click OK to send image to radio.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold [PTT] &amp; Knob while turning the
     radio on.
4. <b>After clicking OK</b>, press the [PTT] switch to send image.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold [PTT] &amp; Knob while turning the
     radio on.
4. Press the [MONI] switch ("WAIT" will appear on the LCD).
5. Press OK.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [F/W] key while turning the radio on
     ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [BAND] key to send image.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [F/W] key while turning the radio on
     ("CLONE" will appear on the display).
4. Press the [V/M] key ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [MON-F] key while turning the radio on
     ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [BAND] key to send image.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [MON-F] key while turning the radio on
     ("CLONE" will appear on the display).
4. Press the [V/M] key ("CLONE WAIT" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [MONI] switch while turning the
     radio on.
4. Rotate the DIAL job to select "F8 CLONE".
5. Press the [F/W] key momentarily.
6. <b>After clicking OK</b>, hold the [PTT] switch
     for one second to send image.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [MONI] switch while turning the
     radio on.
4. Rotate the DIAL job to select "F8 CLONE".
5. Press the [F/W] key momentarily.
6. Press the [MONI] switch ("--RX--" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [moni] key while turning the radio on.
4. Select CLONE in menu, then press F. Radio restarts in clone mode.
     ("CLONE" will appear on the display).
5. <b>After clicking OK</b>, briefly hold [PTT] key to send image.
    ("-TX-" will appear on the LCD). 
 1. Turn radio off.
2. Connect cable to mic jack.
3. Press and hold in the [LOW(A/N)] key while turning the radio on.
4. <b>After clicking OK</b>, press the [MHz(SET)] key to send image.
 1. Turn radio off.
2. Connect cable to mic jack.
3. Press and hold in the [LOW(A/N)] key while turning the radio on.
4. Press the [D/MR(MW)] key ("--WAIT--" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to mic jack.
3. Press and hold in the [MHz], [LOW], and [D/MR] keys
   while turning the radio on.
4. <b>After clicking OK</b>, press the [MHz(SET)] key to send image.
 1. Turn radio off.
2. Connect cable to mic jack.
3. Press and hold in the [MHz], [LOW], and [D/MR] keys
   while turning the radio on.
4. Press the [D/MR(MW)] key ("--WAIT--" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Turn radio on (volume may need to be set at 100%).
5. Ensure that the radio is tuned to channel with no activity.
6. Click OK to download image from device.
 1. Turn radio off.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Turn radio on (volume may need to be set at 100%).
5. Ensure that the radio is tuned to channel with no activity.
6. Click OK to upload image to device.
 1. Turn radio off.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Turn radio on.
5. Ensure that the radio is tuned to channel with no activity.
6. Click OK to download image from device.
 1. Turn radio off.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Turn radio on.
5. Ensure that the radio is tuned to channel with no activity.
6. Click OK to upload image to device.
 1. Turn radio off.
2. Connect data cable.
3. Prepare radio for clone.
4. <b>After clicking OK</b>, press the key to send image.
 1. Turn radio off.
2. Connect data cable.
3. Prepare radio for clone.
4. Press the key to receive the image.
 1. Turn radio off.
2. Connect mic and hold [ACC] on mic while powering on.
    ("CLONE" will appear on the display)
3. Replace mic with PC programming cable.
4. <b>After clicking OK</b>, press the [SET] key to send image.
 1. Turn radio off.
2. Connect mic and hold [ACC] on mic while powering on.
    ("CLONE" will appear on the display)
3. Replace mic with PC programming cable.
4. Press the [DISP/SS] key
    ("R" will appear on the lower left of LCD).
 1. Turn radio off.
2. Remove front head.
3. Connect data cable to radio, use the same connector where
     head was connected to, <b>not the mic connector</b>.
4. Click OK.
 1. Turn radio off.
3. Press and hold in the [moni] key while turning the radio on.
4. Select CLONE in menu, then press F. Radio restarts in clone mode.
     ("CLONE" will appear on the display).
5. Press the [moni] key ("-RX-" will appear on the LCD).
 1. Turn radio on.
2. Connect cable to DATA terminal.
3. Unclip battery.
4. Press and hold in the [AMS] key and power key while clipping 
 in back battery the ("ADMS" will appear on the display).
5. Press the [MODE] key ("-WAIT-" will appear on the LCD).
<b>Then click OK</b> 1. Turn radio on.
2. Connect cable to DATA terminal.
3. Unclip battery.
4. Press and hold in the [AMS] key and power key while clipping 
 in back battery the("ADMS" will appear on the display).
5. <b>After clicking OK</b>, press the [BAND] key.
 1. Turn radio on.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Click OK to download image from device.

It will may not work if you turn on the radio with the cable already attached
 1. Turn radio on.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Click OK to upload the image to device.

It will may not work if you turn on the radio with the cable already attached A new CHIRP version is available. Please visit the website as soon as possible to download it! AM mode does not allow duplex or tone About About CHIRP Agree All All Files All supported formats| Always start with recent list Amateur An error has occurred Applying settings Automatic from system Available modules Bandplan Bands Banks Bin Browser Bug number %s is closed Bug number not found Bug number: Bug subject: Building Radio Browser CHIRP must be restarted for the new selection to take effect Cached results can only be included for a proximity query with Latitude, Longitude, and Distance set. Please uncheck "%s" Canada Changing this setting requires refreshing the settings from the image, which will happen now. Channels with equivalent TX and RX %s are represented by tone mode of "%s" Chirp Image Files Choice Required Choose %s DTCS Code Choose %s Tone Choose Cross Mode Choose Diff Target Choose a recent model Choose duplex Choose the module to load from issue %i: City Click here for full license text Click on the "Special Channels" toggle-button of the memory
editor to see/set the EXT channels. P-VFO channels 100-109
are considered Settings.
Only a subset of the over 200 available radio settings
are supported in this release.
Ignore the beeps from the radio on upload and download.
 Clone completed, checking for spurious bytes Cloning Cloning from radio Cloning to radio Close Close String Close file Close string value with double-quote (") Cluster %i memory Cluster %i memories Comment Communicate with radio Complete Complex or non-standard tone squelch mode (starts the tone mode selection wizard) Connect your interface cable to the PC Port on the
back of the 'TX/RX' unit. NOT the Com Port on the head.
 Convert to FM Copy Country Cross Mode Custom Port Custom... Cut Cut %i memories DTCS DTCS
Polarity DTMF decode DV Memory Danger Ahead Dec Delete Delete memories Detailed information Developer Mode Developer state is now %s. CHIRP must be restarted to take effect Diff Raw Memories Diff against another editor Digital Code Digital Modes Disable reporting Disabled Distance Do not prompt again for %s Do you accept the risk? Double-click to change bank name Download Download from radio Download from radio... Download instructions Driver Driver information Driver messages Dual-mode digital repeaters that support analog will be shown as FM Duplex Edit %i memories Edit details for %i memories Edit details for memory %i Enable Automatic Edits Enabled Enter Frequency Enter Offset (MHz) Enter TX Frequency (MHz) Enter a new name for bank %s: Enter custom port: Enter details about this update. Be descriptive about what you were doing, what you expected to happen, and what actually happened. Enter information about the bug including a short but meaningful subject and information about the radio model (if applicable) in question. In the next step you will have a chance to add more details about the problem. Enter information to add to the bug here Enter the bug number that should be updated Enter your username and password for chirpmyradio.com. If you do not have an account click below to create one before proceeding. Erased memory %s Error applying filter Error applying settings Error communicating with radio Example: "foo" Example: ( expression ) Example: 123 Example: 146.52 Example: AND, OR Example: freq<146.0,148.0> Example: name="myrepeater Example: name~"myrepea" Exclude private and closed repeaters Experimental driver Export can only write CSV files Export to CSV Export to CSV... Extra FM Radio FREE repeater database, which provides information
about repeaters in Europe. No account is required. FREE repeater database, which provides most up-to-date
information about repeaters in Europe. No account is
required. Failed to load radio browser Failed to parse result Failed to send bug report: Features File a new bug File does not exist: %s Files Files: Filter Filter results with location matching this string Filter... Find Find Next Find... Finish Float Finish float value like: 146.52 Finished radio job %s Follow these instructions to download the radio memory:
1 - Connect your interface cable
2 - Radio > Download from radio: DO NOT mess with the radio
during download!
3 - Disconnect your interface cable
 Follow these instructions to download the radio memory:
1 - Connect your interface cable
2 - Radio > Download from radio: Don't adjust any settings
on the radio head!
3 - Disconnect your interface cable
 Follow these instructions to download the radio memory:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio, volume @ 50%
4 - CHIRP Menu - Radio - Download from radio
 Follow these instructions to download your config:
1 - Turn off your radio
2 - Connect your interface cable to the Speaker-2 jack
3 - Turn on your radio
4 - Radio > Download from radio
5 - Disconnect the interface cable! Otherwise there will be
    no right-side audio!
 Follow these instructions to download your info:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the download of your radio data
 Follow these instructions to download your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio (unlock it if password protected)
4 - Click OK to start
 Follow these instructions to read your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Click OK to start
 Follow these instructions to upload the radio memory:
1 - Connect your interface cable
2 - Radio > Upload to radio: DO NOT mess with the radio
during upload!
3 - Disconnect your interface cable
 Follow these instructions to upload the radio memory:
1 - Connect your interface cable
2 - Radio > Upload to radio: Don't adjust any settings
on the radio head!
3 - Disconnect your interface cable
 Follow these instructions to upload the radio memory:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio, volume @ 50%
4 - CHIRP Menu - Radio - Upload to radio
 Follow these instructions to upload your config:
1 - Turn off your radio
2 - Connect your interface cable to the Speaker-2 jack
3 - Turn on your radio
4 - Radio > Upload to radio
5 - Disconnect the interface cable, otherwise there will be
    no right-side audio!
6 - Cycle power on the radio to exit clone mode
 Follow these instructions to upload your info:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the upload of your radio data
 Follow these instructions to upload your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio (unlock it if password protected)
4 - Click OK to start
 Follow these instructions to write your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Click OK to start
 Follow this instructions to download your info:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the download of your radio data
 Follow this instructions to read your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the download of your radio data
 Follow this instructions to upload your info:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the upload of your radio data
 Follow this instructions to write your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the upload of your radio data
 Found empty list value for %(name)s: %(value)r Frequency Frequency %s is out of supported range Frequency granularity in kHz Frequency in this range must not be AM mode Frequency in this range requires AM mode Frequency outside TX bands must be duplex=off GMRS Getting settings Goto Memory Goto Memory: Goto... Help Help Me... Hex Hide empty memories Honor the CTCSS/DCS receive squelch configuration when enabled, else only carrier squelch Human-readable comment (not stored in radio) If set, sort results by distance from these coordinates Import Import %i memories Import from file... Import messages Import not recommended Include Cached Index Info Information Insert Row Above Install desktop icon? Interact with driver Internal driver error Invalid %(value)s (use decimal degrees) Invalid Entry Invalid ZIP code Invalid edit: %s Invalid locator Invalid or unsupported module file Invalid value: %r Issue number: LIVE Language Latitude Length must be %i Limit Bands Limit Modes Limit Status Limit prefixes Limit results to this distance (km) from coordinates Limit use Live Radio Load Module... Load module from issue Load module from issue... Loading a module will not affect open tabs. It is recommended (unless instructed otherwise) to close all tabs before loading a module. Loading modules can be extremely dangerous, leading to damage to your computer, radio, or both. NEVER load a module from a source you do not trust, and especially not from anywhere other than the main CHIRP website (chirpmyradio.com). Loading a module from another source is akin to giving them direct access to your computer and everything on it! Proceed despite this risk? Loading settings Locator Login failed: Check your username and password Logo string 1 (12 characters) Logo string 2 (12 characters) Longitude Manual edit of memory %i Memories Memories are read-only due to unsupported firmware version Memory %i is not deletable Memory field name (one of %s) Memory label (stored in radio) Memory must be in a bank to be edited Memory {num} not in bank {bank} Mode Model Modes Module Module Loaded Module loaded successfully More Info More than one port found: %s Move %i memories Move Down Move Up Move operations are disabled while the view is sorted New Window New version available No empty rows below! No example for %s No modules found No modules found in issue %i No more space available; some memories were not applied No results No results! Number Offset Offset/
TX Freq One of: %s Only certain bands Only certain modes Only certain prefixes Only memory tabs may be exported Only working repeaters Open Open Recent Open Stock Config Open a file Open a module Open debug log Open in new window Open repeaters only Open stock config directory Operator Option Optional: -122.0000 Optional: 100 Optional: 20.0000 Optional: 45.0000 Optional: 52.0000 Optional: AA00 - AA00aa11 Optional: County, Hospital, etc. Overwrite memories? P-VFO channels 100-109 are considered Settings.
Only a subset of the over 130 available radio settings
are supported in this release.
 Parsing Password Paste Paste memories Pasted memories will overwrite %s existing memories Pasted memories will overwrite memories %s Pasted memories will overwrite memory %s Pasted memory will overwrite memory %s Please be sure to quit CHIRP before installing the new version! Please check the bug number and try again. If you do actually want to report against this bug, please comment on it via the website and ask for it to be re-opened. Please follow this steps carefully:
1 - Turn on your radio
2 - Connect the interface cable to your radio
3 - Click the button on this window to start download
    (you may see another dialog, click ok)
4 - Radio will beep and led will flash
5 - You will get a 10 seconds timeout to press "MON" before
    data upload start
6 - If all goes right radio will beep at end.
After cloning remove the cable and power cycle your radio to
get into normal mode.
 Please follow this steps carefully:
1 - Turn on your radio
2 - Connect the interface cable to your radio.
3 - Click the button on this window to start download
    (Radio will beep and led will flash)
4 - Then press the "A" button in your radio to start cloning.
    (At the end radio will beep)
 Please note that developer mode is intended for use by developers of the CHIRP project, or under the direction of a developer. It enables behaviors and functions that can damage your computer and your radio if not used with EXTREME care. You have been warned! Proceed anyway? Please wait Plug in your cable and then click OK Polish repeaters database Port Power Prefixes Press enter to set this in memory Print Preview Printing Prolific USB device Properties Property Name Property Value QTH Locator Query %s Query Source Query string is invalid Query syntax OK Query syntax help Querying RX DTCS Radio Radio did not ack block %i Radio information Radio model: Radio sent data after the last awaited block, this happens when the selected model is a non-US but the radio is a US one. Please choose the correct model and try again. RadioReference Canada requires a login before you can query RadioReference.com is the world's largest
radio communications data provider
<small>Premium account required</small> Receive DTCS code Receive frequency Recent Recent... Recommend using 55.2 Redo Refresh required Refreshed memory %s Reload Driver Reload Driver and File Remove Remove selected model from list Rename bank RepeaterBook is Amateur Radio's most comprehensive,
worldwide, FREE repeater directory. Report or update a bug... Reporting a new bug: %r Reporting enabled Reporting helps the CHIRP project know which radio models and OS platforms to spend our limited efforts on. We would really appreciate if you left it enabled. Really disable reporting? Restart Required Restore %i tab Restore %i tabs Restore tabs on start Results from other areas Retrieved settings Save Save before closing? Save file Saved settings Scan control (skip, include, priority, etc) Scanlists Scrambler Security Risk Select Bandplan... Select Bands Select Language Select Modes Select a bandplan Select a tab and memory to diff Select prefixes Service Settings Settings are read-only due to unsupported firmware version Shift amount (or transmit frequency) controlled by duplex Show Raw Memory Show debug log location Show extra fields Show image backup location Skip Some memories are incompatible with this radio Some memories are not deletable Sort %i memories Sort %i memory Sort %i memories Sort %i memory ascending Sort %i memories ascending Sort %i memory descending Sort %i memories descending Sort by column: Sort memories Sorting State State/Province Success Successfully sent bug report: TX-RX DTCS polarity (normal or reversed) Tested and mostly works, but may give you issues
when using lesser common radio variants.
Proceed at your own risk, and let us know about issues! The DMR-MARC Worldwide Network The FT-450D radio driver loads the 'Special Channels' tab
with the PMS scanning range memories (group 11), 60meter
channels (group 12), the QMB (STO/RCL) memory, the HF and
50m HOME memories and all the A and B VFO memories.
There are VFO memories for the last frequency dialed in
each band. The last mem-tune config is also stored.
These Special Channels allow limited field editing.
This driver also populates the 'Other' tab in the channel
memory Properties window. This tab contains values for
those channel memory settings that don't fall under the
standard Chirp display columns. With FT450 support, the gui now
uses the mode 'DIG' to represent data modes and a new column
'DATA MODE' to select USER-U, USER-L and RTTY 
 The X3Plus driver is currently experimental.
There are no known issues but you should proceed with caution.
Please save an unedited copy of your first successful
download to a CHIRP Radio Images (*.img) file.
 The author of this module is not a recognized CHIRP developer. It is recommended that you not load this module as it could pose a security risk. Proceed anyway? The debug log file is not available when CHIRP is run interactively from the command-line. Thus, this tool will not upload what you expect. It is recommended that you quit now and run CHIRP non-interactively (or with stdin redirected to /dev/null) The following information will be submitted: The recommended procedure for importing memories is to open the source file and copy/paste memories from it into your target image. If you continue with this import function, CHIRP will replace all memories in your currently-open file with those in %(file)s. Would you like to open this file to copy/paste memories across, or proceed with the import? This Memory This driver has been tested with v3 of the ID-5100. If your radio is not fully updated please help by opening a bug report with a debug log so we can add support for the other revisions. This driver is in development and should be considered as experimental. This image is missing firmware information. It may have been generated with an old or modified version of CHIRP. It is advised that you download a fresh image from your radio and use that going forward for the best safety and compatibility. This is a live-mode radio, which means changes are sent to the radio in real-time as you make them. Upload is not necessary! This is a radio-independent file and cannot be uploaded directly to a radio. Open a radio image (or download one from a radio) and then copy/paste items from this tab into that one in order to upload This is an early stage beta driver
 This is an early stage beta driver - upload at your own risk
 This is an experimental driver for the Quansheng UV-K5. It may harm your radio, or worse. Use at your own risk.

Before attempting to do any changes please download the memory image from the radio with chirp and keep it. This can be later used to recover the original settings. 

some details are not yet implemented This is experimental support for BJ-9900 which is still under development.
Please ensure you have a good backup with OEM software.
Also please send in bug and enhancement requests!
You have been warned. Proceed at your own risk! This is the ticket number for an already-created issue on the chirpmyradio.com website This memory and shift all up This memory and shift block up This option may break your radio! Each radio has a unique set of calibration data and uploading the data from the image will cause physical harm to the radio if it is from a different piece of hardware. Do not use this unless you know what you are doing and accept the risk of destroying your radio! This radio has a tricky way of enter into program mode,
even the original software has a few tries to get inside.
I will try 8 times (most of the time ~3 will doit) and this
can take a few seconds, if don't work, try again a few times.
If you can get into it, please check the radio and cable.
 This should only be enabled if you are using modified firmware that supports wider frequency coverage. Enabling this will cause CHIRP not to enforce OEM restrictions and may lead to undefined or unregulated behavior. Use at your own risk! This tool will upload details about your system to an existing issue on the CHIRP tracker. It requires your username and password for chirpmyradio.com in order to work. Information about your system, including your debug log, config file, and any open image files will be uploaded. An attempt will be made to redact any personal information before it leaves your system. This will load a module from a website issue Tone Tone Mode Tone Squelch Tone squelch mode Transmit Power Transmit shift, split mode, or transmit inhibit Transmit tone Transmit/receive DTCS code for DTCS mode, else transmit code Transmit/receive modulation (FM, AM, SSB, etc) Transmit/receive tone for TSQL mode, else receive tone Tuning Step Type a simple search string or a formatted query and press enter USB Port Finder Unable to determine port for your cable. Check your drivers and connections. Unable to edit memory before radio is loaded Unable to find stock config %r Unable to import while the view is sorted Unable to open the clipboard Unable to query Unable to read last block. This often happens when the selected model is US but the radio is a non-US one (or widebanded). Please choose the correct model and try again. Unable to reveal %s on this system Unable to set %s on this memory Unable to upload this file Undo United States Unplug your cable (if needed) and then click OK Unsupported model %r Update an existing bug Updating bug %s Upload instructions Upload is disabled due to unsupported firmware version Upload to radio Upload to radio... Uploaded memory %s Use fixed-width font Use larger font Username Value does not fit in %i bits Value must be at least %.4f Value must be at most %.4f Value must be exactly %i decimal digits Value must be zero or greater Value to search memory field for Values Vendor View WARNING! Warning Warning: %s Welcome With this option checked, a proximity search will include cached results from other localities if they are in range, match other filter parameters, and have been downloaded before. Would you like CHIRP to install a desktop icon for you? You have opened multiple issues within the last week. CHIRP limits the number of issues you can open to avoid abuse. If you really need to open another, please do so via the website. Your Prolific-based USB device will not work without reverting to an older version of the driver. Visit the CHIRP website to read more about how to resolve this? Your QTH Locator Your cable appears to be on port: bits bytes bytes each disabled enabled {bank} is full Project-Id-Version: CHIRP
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-08 12:48+0100
Last-Translator: Giovanni Scafora IK5TWZ <scafora.giovanni@gmail.com>
Language-Team: CHIRP Italian Translation
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.5
 %(value)s deve essere compreso tra %(min)i e %(max)i %i memoria e sposta tutto su %i memorie e sposta tutto su %i memoria %i memorie %i memoria e sposta il blocco su %i memorie e sposta il blocco su %s non è stato salvato. Salvarlo prima di chiudere? (Descrivi ciò che è realmente accaduto) (Descrivi cosa ti aspettavi che accadesse) (Descrivi cosa stavi facendo) (Ha mai funzionato prima? Radio nuova? Funziona con il software OEM?) (nessuno) ...ed altre %i 1. Collega il cavo di programmazione alla presa MIC.
2. Fai clic su OK. 1. Assicurarsi che la versione del firmware sia 4_10 o superiore
2. Spegnere la radio
3. Collegare il cavo dell'interfaccia
4. Accendere la radio
5. Premere e rilasciare il PTT 3 volte mentre si tiene premuto il tasto MONI
6. Velocità di trasmissione supportate: 57600 (predefinito) e 19200
   (per cambiare, ruotare il selettore, tenendo premuto MONI)
7. Fare clic su OK
 1. Spegnere la radio.
2. Collegare il cavo dati.
3. Tenendo premuto il pulsante "A/N LOW", accendere la radio.
4. <b>Dopo aver fatto clic su OK</b>, premere "SET MHz" per inviare l'immagine.
 1. Spegnere la radio.
2. Collegare il cavo dati.
3. Tenendo premuto il pulsante "A/N LOW", accendere la radio.
4. Premere "MW D/MR" per acquisire l'immagine.
5. Assicurarsi che il display indichi "-WAIT-" (in caso contrario, vedere la nota seguente)
6. Fare clic su OK per chiudere la finestra di dialogo ed avviare il trasferimento.
Nota: se al punto 5 non viene visualizzato il messaggio "-WAIT-",
      provare a spegnere la radio, tenendo premuto il pulsante
      rosso "*L" per sbloccarla, quindi ricominciare dal punto 1.
 1. Spegnere la radio.
2. Collegare il cavo dati.
3. Tenendo premuto il pulsante "F/W", accendere la radio.
4. <b>Dopo aver fatto clic su OK</b>, premere "RPT" per inviare l'immagine.
 1. Spegnere la radio.
2. Collegare il cavo dati.
3. Tenendo premuto il pulsante "F/W", accendere la radio.
4. Premere "REV" per acquisire l'immagine.
5. Fare clic su OK per avviare il trasferimento.
 1. Spegnere la radio.
2. Collegare il cavo dati.
3. Tenendo premuto i pulsanti "TONE" e "REV", accendere la radio.
4. <b>Dopo aver fatto clic su OK</b>, premere "TONE" per inviare l'immagine.
 1. Spegnere la radio.
2. Collegare il cavo dati.
3. Tenendo premuto i pulsanti "TONE" e "REV", accendere la radio.
4. Premere "REV" per acquisire l'immagine.
5. Assicurarsi che il display indichi "CLONE RX" e che il led verde lampeggi.
6. Fare clic su OK per avviare il trasferimento.
 1. Spegnere la radio.
2. Collegare il cavo dati.
3. Tenere premuti i tasti MHz, Low e D/MR sulla radio mentre la si accende.
4. La radio è in modalità clone quando TX/RX lampeggia.
5. <b>Dopo aver fatto clic su OK</b>, premere il tasto MHz sulla radio per inviare l'immagine.
    (sul display apparirà la scritta "TX"). 
 1. Spegnere la radio.
2. Collegare il cavo dati.
3. Tenere premuti i tasti MHz, Low e D/MR sulla radio mentre la si accende.
4. La radio è in modalità clone quando TX/RX lampeggia.
5. Premere il tasto Low sulla radio (sul display apparirà la scritta "RX").
6. Fare clic su OK. 1. Spegnere la radio.
2. Collegare il cavo dati alla presa ACC.
3. Tenere premuti i tasti [MODE &lt;] e [MODE &gt;] mentre
     si accende la radio (sul display apparirà la scritta
     "CLONE MODE").
4. <b>Dopo aver fatto clic su OK</b>, premere il tasto [C.S.] per
    inviare l'immagine.
 1. Spegnere la radio.
2. Collegare il cavo dati alla presa ACC.
3. Tenere premuti i tasti [MODE &lt;] e [MODE &gt;] mentre
     si accende la radio (sul display apparirà la scritta
     "CLONE MODE").
4. <b>Dopo aver fatto clic su OK</b>, premere il tasto [A] per inviare l'immagine.
 1. Spegnere la radio.
2. Collegare il cavo dati alla presa ACC.
3. Tenere premuti i tasti [MODE &lt;] e [MODE &gt;] mentre
     si accende la radio (sul display apparirà la scritta
     "CLONE MODE").
4. Fare clic su OK.
    (Sul display apparirà la scritta "Receiving").
 1. Spegnere la radio.
2. Collegare il cavo dati alla presa ACC.
3. Tenere premuti i tasti [MODE &lt;] e [MODE &gt;] mentre
     si accende la radio (sul display apparirà la scritta
     "CLONE MODE").
4. Premere il tasto [A](RCV) (sul display apparirà la scritta "Receiving").
 1. Spegnere la radio.
2. Collegare il cavo dati alla presa ACC.
3. Tenere premuti i tasti [MODE &lt;] e [MODE &gt;] mentre
     si accende la radio (sul display apparirà la scritta
     "CLONE MODE").
4. Premere il tasto [C] (sul display apparirà la scritta "RX").
 1. Spegnere la radio.
2. Collegare il cavo dati alla presa CAT/LINEAR.
3. Tenere premuti i tasti [MODE &lt;] e [MODE &gt;] mentre
     si accende la radio (sul display apparirà la scritta
     "CLONE MODE").
4. <b>Dopo aver fatto clic su OK</b>,
     premere il tasto [C](SEND) per inviare l'immagine.
 1. Spegnere la radio.
2. Collegare il cavo dati alla presa DATA.
3. Tenere premuto il tasto "sinistro" [V/M] mentre si accende
     la radio.
4. Ruotare la manopola "destra" del selettore e selezionare "CLONE START".
5. Premere il tasto [SET]. Il display scomparirà per un attimo,
     quindi apparirà la notazione "CLONE".
6. <b>Dopo aver fatto clic su OK</b>, premere il tasto "sinistro" [V/M] per
     inviare l'immagine.
 1. Spegnere la radio.
2. Collegare il cavo dati alla presa DATA.
3. Tenere premuto il tasto "sinistro" [V/M] mentre si accende
     la radio.
4. Ruotare la manopola "destra" del selettore e selezionare "CLONE START".
5. Premere il tasto [SET]. Il display scomparirà per un attimo,
     quindi apparirà la notazione "CLONE".
6. Premere il tasto "sinistro" [LOW] (sul display apparirà la scritta
     "CLONE -RX-").
 1. Spegnere la radio.
2. Collegare il cavo dati alla presa DATA.
3. Tenere premuto il tasto [FW] mentre si accende la radio.
     (sul display apparirà la scritta "CLONE").
4. <b>Dopo aver fatto clic su OK</b>, premere il tasto [BAND] per inviare l'immagine.
 1. Spegnere la radio.
2. Collegare il cavo dati alla presa DATA.
3. Tenere premuto il tasto [FW] mentre si accende la radio.
     (sul display apparirà la scritta "CLONE").
4. Premere il tasto [MODE] (sul display apparirà la scritta "-WAIT").
 1. Spegnere la radio.
2. Collegare il cavo dati alla presa DATA.
3. Tenere premuto il tasto [MHz(PRI)] mentre si accende
     la radio.
4. Ruotare il selettore e selezionare "F-7 CLONE".
5. Tenere premuto il tasto [BAND(SET)]. Il display
     scomparirà per un attimo, quindi apparirà la notazione
     "CLONE".
6. Premere il tasto [LOW(ACC)] (sul display apparirà la scritta "--RX--").
 1. Spegnere la radio.
2. Collegare il cavo alla presa DATA.
3. Tenere premuto il tasto [MHz(PRI)] mentre si accende
 la radio.
4. Ruotare il selettore e selezionare "F-7 CLONE".
5. Premere e tenere premuto il tasto [BAND(SET)]. Il display
 scomparirà per un momento, quindi apparirà la notazione
 "CLONE".
6. <b>Dopo aver fatto clic su OK</b>, premere il tasto [V/M(MW)] per inviare l'immagine.
 1. Spegnere la radio.
2. Collegare il cavo al terminale DATA.
3. Tenere premuto il tasto [DISP] mentre si accende la radio
     (sul LCD della radio apparirà "CLONE").
4. <b>Dopo aver fatto clic su OK qui su Chirp</b>,
    premere il pulsante [Send] sullo schermo.
 1. Spegnere la radio.
2. Collegare il cavo al terminale DATA.
3. Tenere premuto il tasto [DISP] mentre si accende la radio
     (sul LCD della radio apparirà "CLONE").
4. Premere il pulsante [RECEIVE] sullo schermo
     (sul LCD della radio apparirà "WAIT").
5, Infine, premere il pulsante OK in basso.
 1. Spegnere la radio.
2. Collegare il cavo al terminale DATA.
3. Tenere premuto il tasto [F] mentre si accende la radio.
     (sul display apparirà la scritta "CLONE").
4. <b>Dopo aver fatto clic su OK</b>, premere il tasto [BAND] per inviare l'immagine.
 1. Spegnere la radio.
2. Collegare il cavo al terminale DATA.
3. Tenere premuto il tasto [F] mentre si accende la radio.
     (sul display apparirà la scritta "CLONE").
4. Premere il tasto [Dx] (sul display apparirà la scritta "-WAIT-").
 1. Spegnere la radio.
2. Collegare il cavo al terminale DATA.
3. Tenere premuto il tasto [MHz(SETUP)] mentre si accende la radio
     (sul display apparirà la scritta "CLONE").
4. <b>Dopo aver fatto clic su OK</b>, premere il tasto [REV(DW)]
     per inviare l'immagine.
 1. Spegnere la radio.
2. Collegare il cavo al terminale DATA.
3. Tenere premuto il tasto [MHz(SETUP)] mentre si accende la radio
     (sul display apparirà la scritta "CLONE").
4. Premere il tasto [MHz(SETUP)]
     (sul display apparirà la scritta "-WAIT-").
 1. Spegnere la radio.
2. Collegare il cavo alla presa MIC.
3. Tenere premuto il tasto [MHz(SETUP)] mentre si accende la radio
     (sul display apparirà la scritta "CLONE").
4. <b>Dopo aver fatto clic su OK</b>, premere il tasto [GM(AMS)]
     per inviare l'immagine.
 1. Spegnere la radio.
2. Collegare il cavo alla presa MIC.
3. Tenere premuto il tasto [MHz(SETUP)] mentre si accende la radio.
     (sul display apparirà la scritta "CLONE").
4. Premere il tasto [MHz(SETUP)]
     (sul display apparirà la scritta "-WAIT-").
 1. Spegnere la radio.
2. Collegare il cavo alla presa MIC/EAR.
3. Tenere premuto il tasto [F/W] mentre si accende la radio.
    (sul display apparirà la scritta "CLONE").
4. <b>Dopo aver fatto clic su OK</b>, premere il tasto [VFO(DW)SC] per acquisire
    l'immagine dalla radio.
 1. Spegnere la radio.
2. Collegare il cavo alla presa MIC/EAR.
3. Tenere premuto il tasto [F/W] mentre si accende la radio.
    (sul display apparirà la scritta "CLONE").
4. Premere il tasto [MR(SKP)SC] (sul display apparirà la scritta
 
    "CLONE WAIT").
5. Fare clic su OK per inviare l'immagine alla radio.
 1. Spegnere la radio.
2. Collegare il cavo alla presa MIC/SP.
3. Tenere premuto il tasto [PTT] &amp; mentre si accende
     la radio.
4. <b>Dopo aver fatto clic su OK</b>, premere il tasto [PTT] per inviare l'immagine.
 1. Spegnere la radio.
2. Collegare il cavo alla presa MIC/SP.
3. Tenere premuto il tasto [PTT] &amp; mentre si accende
     la radio.
4. Premere l'interruttore [MONI] (sul display apparirà la scritta "WAIT").
5. Premere OK.
 1. Spegnere la radio.
2. Collegare il cavo alla presa MIC/SP.
3. Tenere premuto il tasto [F/W] mentre si accende la radio.
     (sul display apparirà la scritta "CLONE").
4. <b>Dopo aver fatto clic su OK</b>, premere il tasto [BAND] per inviare l'immagine.
 1. Spegnere la radio.
2. Collegare il cavo alla presa MIC/SP.
3. Tenere premuto il tasto [F/W] mentre si accende la radio.
     (sul display apparirà la scritta "CLONE").
4. Premere il tasto [V/M] (sul display apparirà la scritta "-WAIT-").
 1. Spegnere la radio.
2. Collegare il cavo alla presa MIC/SP.
3. Tenere premuto il tasto [MON-F] mentre si accende la radio.
     (sul display apparirà la scritta "CLONE").
4. <b>Dopo aver fatto clic su OK</b>, premere il tasto [BAND] per inviare l'immagine.
 1. Spegnere la radio.
2. Collegare il cavo alla presa MIC/SP.
3. Tenere premuto il tasto [MON-F] mentre si accende la radio.
     (sul display apparirà la scritta "CLONE").
4. Premere il tasto [V/M] (sul display apparirà la scritta "CLONE WAIT").
 1. Spegnere la radio.
2. Collegare il cavo alla presa MIC/SP.
3. Tenere premuto l'interruttore [MONI] mentre si accende
     la radio.
4. Ruotare il selettore e selezionare "F8 CLONE".
5. Premere momentaneamente il tasto [F/W].
6. <b>Dopo aver fatto clic su OK</b>, tenere premuto il tasto [PTT]
     per un secondo per inviare l'immagine.
 1. Spegnere la radio.
2. Collegare il cavo alla presa MIC/SP.
3. Tenere premuto l'interruttore [MONI] mentre si accende
     la radio.
4. Ruotare il selettore e selezionare "F8 CLONE".
5. Premere momentaneamente il tasto [F/W].
6. Premere il selettore [MONI] (sul display apparirà la scritta "--RX--").
 1. Spegnere la radio.
2. Collegare il cavo alla presa MIC/SP.
3. Tenere premuto il tasto [moni] mentre si accende la radio.
4. Selezionare CLONE nel menu, quindi premere F. La radio si riavvia in modalità clone.
     (sul display apparirà la scritta "CLONE").
5. <b>Dopo aver fatto clic su OK</b>, tenere brevemente premuto il tasto [PTT] per inviare l'immagine.
    (sul display apparirà la scritta "-TX-"). 
 1. Spegnere la radio.
2. Collegare il cavo alla presa del microfono.
3. Tenere premuto il tasto [LOW(A/N)] mentre si accende la radio.
4. <b>Dopo aver fatto clic su OK</b>, premere il tasto [MHz(SET)] per inviare l'immagine.
 1. Spegnere la radio.
2. Collegare il cavo alla presa del microfono.
3. Tenere premuto il tasto [LOW(A/N)] mentre si accende la radio.
4. Premere il tasto [D/MR(MW)] (sul display apparirà la scritta "--WAIT--").
 1. Spegnere la radio.
2. Collegare il cavo alla presa del microfono.
3. Premere e tenere premuti i tasti [MHz], [LOW] e [D/MR]
   mentre si accende la radio.
4. <b>Dopo fatto clic su OK</b>, premere il tasto [MHz(SET)] per inviare l'immagine.
 1. Spegnere la radio.
2. Collegare il cavo alla presa del microfono.
3. Premere e tenere premuti i tasti [MHz], [LOW] e [D/MR]
   mentre si accende la radio.
4. Premere il tasto [D/MR(MW)] (sul display apparirà la scritta "--WAIT--").
 1. Spegnere la radio.
2. Collegare il cavo al connettore microfono/spkr.
3. Assicurarsi che il connettore sia saldamente collegato.
4. Accendere la radio (potrebbe essere necessario impostare il volume al 100%).
5. Assicurarsi che la radio sia sintonizzata su un canale privo di attività.
6. Fare clic su OK per scaricare l'immagine dal dispositivo.
 1. Spegnere la radio.
2. Collegare il cavo al connettore microfono/spkr.
3. Assicurarsi che il connettore sia saldamente collegato.
4. Accendere la radio (potrebbe essere necessario impostare il volume al 100%).
5. Assicurarsi che la radio sia sintonizzata su un canale privo di attività.
6. Fare clic su OK per caricare l'immagine sul dispositivo.
 1. Spegnere la radio.
2. Collegare il cavo al connettore microfono/spkr.
3. Assicurarsi che il connettore sia saldamente collegato.
4. Accendere la radio.
5. Assicurarsi che la radio sia sintonizzata su un canale privo di attività.
6. Fare clic su OK per scaricare l'immagine dal dispositivo.
 1. Spegnere la radio.
2. Collegare il cavo al connettore microfono/spkr.
3. Assicurarsi che il connettore sia saldamente collegato.
4. Accendere la radio.
5. Assicurarsi che la radio sia sintonizzata su un canale privo di attività.
6. Fare clic su OK per caricare l'immagine sul dispositivo.
 1. Spegnere la radio.
2. Collegare il cavo dati.
3. Preparare la radio per il clone.
4. <b>Dopo aver fatto clic su OK</b>, premere il tasto per inviare l'immagine.
 1. Spegnere la radio.
2. Collegare il cavo dati.
3. Preparare la radio per il clone.
4. Premere il tasto per acquisire l'immagine.
 1. Spegnere la radio.
2. Collegare il microfono e tenere premuto [ACC] sul microfono durante l'accensione.
    (sul display apparirà la scritta "CLONE").
3. Sostituire il microfono con il cavo di programmazione del PC.
4. <b>Dopo aver fatto clic su OK</b>, premere il tasto [SET] per inviare l'immagine.
 1. Spegnere la radio.
2. Collegare il microfono e tenere premuto [ACC] sul microfono durante l'accensione.
    (sul display apparirà la scritta "CLONE").
3. Sostituire il microfono con il cavo di programmazione del PC.
4. Premere il tasto [DISP/SS]
    (sul display, in basso a sinistra, apparirà la scritta "R").
 1. Spegnere la radio.
2. Rimuovere la parte anteriore.
3. Collegare il cavo dati alla radio, utilizzando lo stesso connettore
     del pannello frontale, <b>non al connettore del microfono</b>.
4. Fare clic su OK.
 1. Spegnere la radio.
3. Tenere premuto il tasto [moni] mentre si accende la radio.
4. Selezionare CLONE nel menu, quindi premere F. La radio si riavvia in modalità clone.
     (sul display apparirà la scritta "CLONE").
5. Premere il tasto [moni] (sul display apparirà la scritta "-RX-").
 1. Accendere la radio.
2. Collegare il cavo al terminale DATA.
3. Staccare la batteria.
4. Tenere premuto il tasto [AMS] e il tasto di accensione mentre si aggancia
 la batteria posteriore (sul display apparirà la scritta "ADMS").
5. Premere il tasto [MODE] (sul display apparirà la scritta "-WAIT-").
<b>Quindi, fare clic su OK</b> 1. Accendere la radio.
2. Collegare il cavo al terminale DATA.
3. Staccare la batteria.
4. Tenere premuto il tasto [AMS] e il tasto di accensione mentre si aggancia
 la batteria posteriore (sul display apparirà la scritta "ADMS").
5. <b>Dopo aver fatto clic su OK</b>, premere il tasto [BAND].
 1. Accendere la radio.
2. Collegare il cavo al connettore microfono/spkr.
3. Assicurarsi che il connettore sia saldamente collegato.
4. Fare clic su OK, per scaricare l'immagine dal dispositivo.

Potrebbe non funzionare, se si accende la radio con il cavo già collegato
 1. Accendere la radio.
2. Collegare il cavo al connettore microfono/spkr.
3. Assicurarsi che il connettore sia saldamente collegato.
4. Fare clic su OK, per caricare l'immagine sul dispositivo.

Potrebbe non funzionare, se si accende la radio con il cavo già collegato È disponibile una nuova versione di CHIRP. Visita il sito per scaricarla! La modalità AM non consente il duplex o il tono Informazioni Informazioni su CHIRP Sono d'accordo Tutto Tutti i file Tutti i formati supportati| Inizia sempre con un elenco recente Radioamatore Si è verificato un errore Applicazione delle impostazioni Automatico dal sistema Moduli disponibili Bandplan Bande Banchi Bin Browser La segnalazione del bug numero %s è stata chiusa Il numero del bug non esiste Numero del bug: Oggetto del bug: Creazione del browser della radio Per rendere effettiva la nuova selezione, è necessario riavviare CHIRP I risultati della cache possono essere inclusi solo per una query di prossimità con latitudine, longitudine e distanza impostate. Deselezionare “%s” Canada Per modificare questa impostazione è necessario aggiornare le impostazioni dall'immagine, cosa che avverrà ora. I canali con TX e RX equivalenti %s sono rappresentati dalla modalità di tono "%s". File immagine di Chirp Scelta richiesta Scegli il codice DTCS %s Scegli il Tono %s Scegli la modalità cross Scegli un tipo di diff Scegli un modello recente Scegli il duplex Scegli il modulo da caricare dal sito %i: Città Fare clic qui per il testo completo della licenza Fare clic sul pulsante "Special Channels" dell'editor di memoria
per vedere/impostare i canali EXT. I canali P-VFO 100-109
sono considerati impostazioni.
Solo un sottoinsieme delle oltre 200 impostazioni radio disponibili
sono supportate in questa versione.
Ignorare i segnali acustici della radio durante l'upload e il download.
 Clonazione completata, controllo dei byte spuri Clonazione Clonazione dalla radio Clonazione verso la radio Chiudi Chiudi la stringa Chiudi il file Chiudi il valore della stringa con un doppio apice (") Raggruppa %i memoria Raggruppa %i memorie Commento Comunicazione con la radio Completo Modalità squelch toni complessi o non standard (avvia la procedura guidata di selezione della modalità toni) Collegare il cavo dell'interfaccia alla porta del PC
situata sul retro dell'unita 'TX/RX'. NON alla porta COM del pannello frontale.
 Converti in FM Copia Nazione Modalità Cross Porta personalizzata Personalizza... Taglia Taglia %i memorie DTCS DTCS
Polarità Decodifica DTMF Memoria DV Pericolo in vista Dec Elimina Elimina memorie Informazioni dettagliate Modalità sviluppatore Lo stato dello sviluppatore è ora impostato su %s. Per avere effetto, CHIRP deve essere riavviato Diff memorie raw Diff rispetto ad un altro editor Codice digitale Modi digitali Disabilita notifiche Disabilitata Distanza Non mostrarlo più per %s Accetti il rischio? Fare doppio clic per modificare il nome del banco Scarica Scarica dalla radio Scarica dalla radio... Istruzioni per il download Driver Informazioni sul driver Messaggi del driver I ripetitori digitali dual-mode che supportano l'analogico saranno mostrati come FM Duplex Modifica %i memorie Modifica i dettagli di %i memorie Modifica i dettagli della memoria %i Abilita modifiche automatiche Abilitato Inserire la frequenza Inserire l'offset (MHz) Inserire la frequenza TX (MHz) Inserire un nuovo nome per il banco %s: Inserire la porta personalizzata: Inserisci i dettagli di questo aggiornamento. Sii descrittivo su ciò che stavi facendo, su ciò che ti aspettavi accadesse e su ciò che è effettivamente accaduto. Immetti le informazioni sul bug, compreso un oggetto breve ma significativo e le informazioni sul modello della radio (se applicabile) in questione. Nella fase successiva sarà possibile aggiungere ulteriori dettagli sul problema. Inserisci qui le informazioni da aggiungere al bug Digita il numero del bug da aggiornare Inserisci il nome utente e la password di chirpmyradio.com. Se non disponi di un account, fai clic qui sotto per crearne uno prima di procedere. Memoria %s eliminata Si è verificato un errore durante l'applicazione del filtro Si è verificato un errore durante l'applicazione delle impostazioni Errore durante la comunicazione con la radio Esempio: "pippo" Esempio: ( espressione ) Esempio: 123 Esempio: 146.52 Esempio: AND, OR Esempio: freq<146.0,148.0> Esempio: nome="mioripetitore" Esempio: nome~"mioripetitore" Escludi ripetitori privati e chiusi Driver sperimentale L'esportazione può scrivere solo file CSV Esporta in CSV Esporta in CSV... Extra Radio FM Database GRATUITO sui ripetitori, che fornisce informazioni
sui ripetitori in Europa. Non è richiesto alcun account. Database GRATUITO di ripetitori, che fornisce le
informazioni più aggiornate sui ripetitori in Europa.
Non è necessario un account. Impossibile caricare il browser della radio Impossibile elaborare il risultato Impossibile inviare la segnalazione del bug: Funzionalità Segnala un nuovo bug Il file non esiste: %s File File: Filtro Filtra i risultati con i luoghi che corrispondono a questa stringa Filtro... Trova Trova successivo Trova... Float finale Il valore finale della float è: 146.52 Lavoro radio completato %s Seguire le seguenti istruzioni per scaricare la memoria della radio:
1 - Collegare il cavo dati dell'interfaccia
2 - Radio > Scarica dalla radio: NON intervenire sulla radio
durante il download!
3 - Scollegare il cavo dati dell'interfaccia
 Seguire le seguenti istruzioni per scaricare la memoria della radio:
1 - Collegare il cavo dati dell'interfaccia
2 - Radio > Scarica dalla radio: non regolare le impostazioni
della radio!
3 - Scollegare il cavo dati dell'interfaccia
 Seguire le seguenti istruzioni per scaricare la memoria della radio:
1 - Spegnere la radio
2 - Collegare il cavo dati dell'interfaccia
3 - Accendere la radio, volume al 50%.
4 - Menu CHIRP - Radio - Scarica dalla radio
 Seguire le seguenti istruzioni per scaricare la configurazione:
1 - Spegnere la radio
2 - Collegare il cavo dati dell'interfaccia alla presa Speaker-2
3 - Accendere la radio
4 - Radio > Scarica dalla radio
5 - Scollegare il cavo dati dell'interfaccia! In caso contrario, non ci sarà
    l'audio del lato destro!
 Seguire le seguenti istruzioni per scaricare le informazioni:
1 - Spegnere la radio
2 - Collegare il cavo dati dell'interfaccia
3 - Accendere la radio
4 - Effettuare il download dei dati della radio
 Seguire le seguenti istruzioni per scaricare dalla radio:
1 - Spegnere la radio
2 - Collegare il cavo dati dell'interfaccia
3 - Accendere la radio (sbloccarla se protetta da password)
4 - Fare clic su OK per iniziare
 Seguire le seguenti istruzioni per leggere la radio:
1 - Spegnere la radio
2 - Collegare il cavo dati dell'interfaccia
3 - Accendere la radio
4 - Fare clic su OK per iniziare
 Seguire le seguenti istruzioni per caricare la memoria della radio:
1 - Collegare il cavo dati dell'interfaccia
2 - Radio > Carica sulla radio: NON intervenire sulla radio
durante il caricamento!
3 - Scollegare il cavo dati dell'interfaccia
 Seguire le seguenti istruzioni per caricare la memoria della radio:
1 - Collegare il cavo dati dell'interfaccia
2 - Radio > Carica sulla radio: Non modificare le impostazioni
sulla radio!
3 - Scollegare il cavo dati dell'interfaccia
 Seguire le seguenti istruzioni per caricare la memoria della radio:
1 - Spegnere la radio
2 - Collegare il cavo dati dell'interfaccia
3 - Accendere la radio, volume al 50%.
4 - Menu CHIRP - Radio - Carica sulla radio
 Seguire le istruzioni per caricare la configurazione:
1 - Spegnere la radio
2 - Collegare il cavo dati dell'interfaccia alla presa Speaker-2
3 - Accendere la radio
4 - Radio > Carica sulla radio
5 - Scollegare il cavo dati dell'interfaccia, altrimenti non ci sarà
    l'audio del lato destro!
6 - Accendere la radio per uscire dalla modalità clone.
 Seguire le istruzioni per caricare le informazioni:
1 - Spegnere la radio
2 - Collegare il cavo dati dell'interfaccia
3 - Accendere la radio
4 - Effettuare il caricamento dei dati sulla radio
 Seguire le seguenti istruzioni per caricare i dati sulla radio:
1 - Spegnere la radio
2 - Collegare il cavo dati dell'interfaccia
3 - Accendere la radio (sbloccarla se protetta da password)
4 - Fare clic su OK per iniziare
 Seguire le seguenti istruzioni per scrivere sulla radio:
1 - Spegnere la radio
2 - Collegare il cavo dati dell'interfaccia
3 - Accendere la radio
4 - Fare clic su OK per iniziare
 Seguire le istruzioni per scaricare le informazioni:
1 - Spegnere la radio
2 - Collegare il cavo dati dell'interfaccia
3 - Accendere la radio
4 - Effettuare il download dei dati della radio
 Seguire queste istruzioni per leggere i dati dalla radio:
1 - Spegnere la radio
2 - Collegare il cavo dati dell'interfaccia
3 - Accendere la radio
4 - Effettuare il download dei dati dalla radio
 Seguire le istruzioni per caricare le informazioni:
1 - Spegnere la radio
2 - Collegare il cavo dati dell'interfaccia
3 - Accendere la radio
4 - Effettuare il caricamento dei dati sulla radio
 Seguire queste istruzioni per scrivere i dati sulla radio:
1 - Spegnere la radio
2 - Collegare il cavo dati dell'interfaccia
3 - Accendere la radio
4 - Effettuare il caricamento dei dati sulla radio
 Trovato valore di lista vuoto per %(name)s: %(value)r Frequenza La frequenza %s non rientra nel range supportato Granularità della frequenza in kHz La frequenza in questo intervallo non deve essere in modalità AM La frequenza in questo intervallo richiede la modalità AM La frequenza al di fuori delle bande TX deve essere duplex=off GMRS Acquisire le impostazioni Vai alla memoria Vai alla memoria: Vai... Aiuto Aiutami... Hex Nascondi memorie vuote Rispetta la configurazione dello squelch di ricezione CTCSS/DCS quando è abilitata, altrimenti solo lo squelch della portante Commento leggibile dall'essere umano (non memorizzato nella radio) Se impostata, ordina i risultati secondo la distanza da tali coordinate Importa Importa %i memorie Importa da file... Importa messaggi Importazione non consigliata Includi la cache Indice Informazioni Informazioni Inserisci riga sopra Installare l'icona sul desktop? Interagisci con il driver Errore interno del driver Il valore %(value)s non è valido (usare i gradi decimali) Valore non valido Codice postale non valido Modifica non valida: %s Locator non valido File del modulo non valido o non supportato Valore non valido: %r Numero: In diretta live Lingua Latitudine La lunghezza deve essere %i Limita bande Limita modi Limita stato Limita i prefissi Limita i risultati a questa distanza (km) dalle coordinate Limita uso Radio in diretta live Carica modulo... Carica modulo dal sito Carica modulo dal sito... Il caricamento di un modulo non influisce sulle schede aperte. Si consiglia (salvo istruzioni diverse) di chiudere tutte le schede, prima di caricare un modulo. Il caricamento dei moduli può essere estremamente pericoloso e causare danni al computer, alla radio o a entrambi. Non caricate MAI un modulo da una fonte di cui non vi fidate, soprattutto se non dal sito web principale di CHIRP (chirpmyradio.com). Caricare un modulo da un'altra fonte equivale a dargli accesso diretto al vostro computer e a tutto ciò che contiene! Vuoi procedere nonostante questo rischio? Caricamento delle impostazioni Locator Accesso non riuscito: controlla il nome utente e la password Stringa del logo 1 (12 caratteri) Stringa del logo 2 (12 caratteri) Longitudine Modifica manuale della memoria %i Memorie Le memorie sono di sola lettura a causa della versione del firmware non supportata La memoria %i non si può eliminare Nome del campo di memoria (uno di %s) Etichetta di memoria (memorizzata nella radio) La memoria deve trovarsi in un banco per essere modificata La memoria {num} non è presente nel banco {bank} Modalità Modello Modalità Modulo Modulo caricato Modulo caricato con successo Maggiori informazioni Trovata più di una porta: %s Sposta %i memorie Sposta giù Sposta su Le operazioni di spostamento sono disabilitate quando la visualizzazione è ordinata Nuova finestra Nuova versione disponibile Nessuna riga vuota in basso! Nessun esempio per %s Nessun modulo trovato Nessun modulo trovato nel sito %i Non c'è più spazio disponibile; alcune memorie non sono state inserite Nessun risultato Nessun risultato! Numero Offset Offset/
TX Freq Uno di: %s Solo alcune bande Solo alcune modalità Solo alcuni prefissi È possibile esportare solo le schede di memoria Solo ripetitori funzionanti Apri Apri recente Apri configurazione standard Apri un file Apri un modulo Apri registro di debug Apri in nuova finestra Apri solo ripetitori Apri directory delle configurazioni standard Operatore Opzione Opzionale: -122.0000 Opzionale: 100 Opzionale: 20.0000 Opzionale: 45.0000 Opzionale: 52.0000 Opzionale: AA00 - AA00aa11 Opzionale: contea, ospedale, ecc. Sovrascrivere le memorie? I canali P-VFO 100-109 sono considerati impostazioni.
In questa versione, è supportato solo un sottoinsieme
delle oltre 130 impostazioni radio disponibili.
 Elaborazione Password Incolla Incolla memorie Le memorie incollate sovrascriveranno %s memorie esistenti Le memorie incollate sovrascriveranno le memorie %s Le memorie incollate sovrascriveranno la memoria %s La memoria incollata sovrascriverà la memoria %s Chiudere CHIRP prima di installare la nuova versione! Controlla il numero del bug e riprova. Se vuoi effettivamente segnalare questo bug, commentalo tramite il sito web e chiedi che venga riaperto. Seguire attentamente questa procedura:
1 - Accendere la radio
2 - Connettere il cavo  dell'interfaccia alla radio
3 - Fare clic sul pulsante di questa finestra per avviare il download
    (potrebbe apparire un'altra finestra di dialogo, fare clic su OK)
4 - La radio emetterà un segnale acustico ed il led lampeggerà
5 - Verrà visualizzato un timeout di 10 secondi per premere "MON" prima
    dell'inizio del caricamento dei dati.
6 - Se tutto va bene, la radio emetterà un segnale acustico al termine.
Dopo la clonazione, rimuovere il cavo e spegnere la radio, per
passare alla modalità normale.
 Seguire attentamente questa procedura:
1 - Accendere la radio
2 - Connettere il cavo  dell'interfaccia alla radio
3 - Fare clic sul pulsante di questa finestra per avviare il download
    (La radio emetterà un segnale acustico ed il led lampeggerà)
4 - Quindi premere il pulsante "A" della radio, per avviare la clonazione.
    (Al termine, la radio emetterà un segnale acustico)
 Si noti che la modalità sviluppatore è destinata all'uso da parte degli sviluppatori del progetto CHIRP o sotto la direzione di uno sviluppatore. Essa abilita comportamenti e funzioni che possono danneggiare il computer e la radio se non vengono utilizzati con ESTREMA attenzione. Siete stati avvertiti! Vuoi procedere comunque? Attendere Collegare il cavo e fare clic su OK Database dei ripetitori polacchi Porta Potenza Prefissi Premere invio per impostarlo in memoria Anteprima di stampa Stampa in corso Dispositivo USB Prolific Proprietà Nome della proprietà Valore della proprietà QTH Locator Interroga %s Interroga fonte La stringa della query non è valida La sintassi della query è OK Aiuto per la sintassi delle query Interrogazione DTCS RX Radio La radio non ha accettato il blocco %i Informazioni sulla radio Modello della radio: La radio ha inviato i dati dopo l'ultimo blocco atteso; ciò accade quando il modello selezionato non è statunitense ma la radio è statunitense. Scegliere il modello corretto e riprovare. RadioReference Canada richiede un login prima di poter effettuare le interrogazioni RadioReference.com è il più grande fornitore
del mondo di dati sulle comunicazioni radio
<small>È necessario un account premium</small> Ricezione del codice DTCS Frequenza di ricezione Recente Recente... Si consiglia di utilizzare 55.2 Rifai Aggiornamento necessario Memoria aggiornata %s Ricarica il driver Ricarica il driver e il file Elimina Elimina il modello selezionato dall'elenco Rinomina banco RepeaterBook è l'elenco mondiale e GRATUITO
più completo di ripetitori per radioamatori. Segnala o aggiorna un bug... Segnalazione di un nuovo bug: %r Reporting abilitato Le segnalazioni aiutano il progetto CHIRP a sapere su quali modelli di radio e piattaforme OS spendere i nostri limitati sforzi. Vi saremmo grati se le lasciaste abilitate. Vuoi davvero disabilitare le segnalazioni? Riavvio richiesto Ripristina %i scheda Ripristina %i schede Ripristina schede all'avvio Risultati da altre aree Impostazioni recuperate Salva Salvare prima di chiudere? Salva file Impostazioni salvate Controllo della scansione (salto, inclusione, priorità, ecc.) Elenco delle scansioni Scrambler Rischio di sicurezza Seleziona bandplan... Seleziona le bande Seleziona la lingua Seleziona le modalità Seleziona un blandplan Seleziona una scheda e la memoria per diff Seleziona i prefissi Servizio Impostazioni Le impostazioni sono di sola lettura a causa della versione del firmware non supportata Shift (o frequenza di trasmissione) controllato dal duplex Mostra memoria raw Mostra posizione del log di debug Mostra campi aggiuntivi Mostra posizione del backup dell'immagine Ignora Alcune memorie sono incompatibili con questa radio Alcune memorie non sono eliminabili Ordina %i memorie Ordina %i memoria Ordina %i memorie Ordina %i memoria in modalità crescente Ordina %i memorie in modalità crescente Ordina %i memoria in modalità decrescente Ordina %i memorie in modalità decrescente Ordina per colonna: Ordina memorie Ordinamento Stato Stato/Provincia Successo Segnalazione del bug inviata con successo: Polarità DTCS TX-RX (normale o invertita) Testato e per lo più funzionante, ma potrebbe dare problemi
quando si utilizzano varianti di radio meno comuni.
Procedete a vostro rischio e pericolo e fateci sapere se ci sono problemi! La rete mondiale DMR-MARC Il driver della radio FT-450D carica la scheda 'Special Channels'
con le memorie della gamma di scansione PMS (gruppo 11), i canali
60 metri (gruppo 12), la memoria QMB (STO/RCL), le memorie HF e
50 metri e tutte le memorie del VFO A e B.
Sono presenti memorie del VFO dell'ultima frequenza selezionata in
ogni banda. Viene memorizzata anche l'ultima configurazione della sintonia.
Questi canali speciali consentono una modifica limitata del campo.
Questo driver popola anche la scheda "Altro" nella finestra
delle proprietà della memoria del canale. Questa scheda contiene i valori
di memoria del canale che non rientrano nelle colonne di visualizzazione
standard del display Chirp. Con il supporto dell'FT450, la gui adesso
utilizza la modalità 'DIG' per rappresentare i modi di trasmissione dati e una
nuova colonna 'DATA MODE' per selezionare USER-U, USER-L e RTTY. 
 Il driver X3Plus è attualmente sperimentale.
Non ci sono problemi noti, ma è necessario procedere con cautela.
Si consiglia di salvare una copia non modificata del primo download
riuscito in un file CHIRP Radio Images (*.img).
 L'autore di questo modulo non è uno sviluppatore CHIRP riconosciuto. Si raccomanda di non caricare questo modulo, perché potrebbe rappresentare un rischio per la sicurezza. Vuoi procedere comunque? Il file di log di debug non è disponibile quando CHIRP viene eseguito interattivamente dalla riga di comando. Pertanto, questo strumento non caricherà ciò che ci si aspetterebbe. Si consiglia di uscire ora ed eseguire CHIRP in modalità non interattiva (oppure con lo stdin reindirizzato a /dev/null) Saranno trasmesse le seguenti informazioni: La procedura consigliata per importare le memorie consiste nell'aprire il file di origine e nel copiare/incollare le memorie da esso nell'immagine di destinazione. Se si prosegue con questa funzione di importazione, CHIRP sostituirà tutte le memorie del file attualmente aperto con quelle presenti in %(file)s. Si desidera aprire il file per copiare/incollare le memorie o procedere con l'importazione? Questa memoria Questo driver è stato testato con la versione 3 dell'ID-5100. Se la vostra radio non è completamente aggiornata, vi preghiamo di aiutarci, aprendo una segnalazione di bug con un log di debug, in modo da poter aggiungere il supporto per le altre revisioni. Questo driver è in fase di sviluppo e deve essere considerato sperimentale. Questa immagine manca di informazioni sul firmware. Potrebbe essere stata generata con una versione vecchia o modificata di CHIRP. Si consiglia di scaricare una nuova immagine dalla radio e di utilizzarla in futuro, per garantire la massima sicurezza e compatibilità. Questa è una radio in modalità live, il che significa che le modifiche vengono inviate alla radio in tempo reale mentre vengono apportate. Il caricamento non è necessario! Questo è un file indipendente dalla radio e non può essere caricato direttamente su una radio. Aprire un'immagine della radio (o scaricarne una da una radio) e quindi copiare/incollare gli elementi di questa scheda in quella per caricarla Questo è un driver beta in fase iniziale
 Si tratta di un driver beta in fase iniziale: caricatelo a vostro rischio e pericolo.
 Questo è un driver sperimentale per il Quansheng UV-K5. Potrebbe danneggiare la radio, o peggio. Usatelo a vostro rischio e pericolo.

Prima di effettuare qualsiasi modifica, scaricare l'immagine della memoria dalla radio con chirp e conservarla. Questa può essere utilizzata in seguito per recuperare le impostazioni originali.

Alcuni dettagli non sono ancora stati implementati Si tratta di un supporto sperimentale per il BJ-9900, ancora in fase di sviluppo.
Assicurarsi di avere un buon backup con il software OEM.
Si prega, inoltre, di inviare richieste di bug e miglioramenti!
Siete stati avvertiti. Procedete a vostro rischio e pericolo! Questo è il numero di ticket di un problema già creato sul sito web chirpmyradio.com Questa memoria e sposta tutto in alto Questa memoria e sposta il blocco in alto Questa opzione potrebbe causare danni alla radio! Ogni radio ha un set unico di dati di calibrazione e, il caricamento dei dati dall'immagine, causerà danni fisici alla radio se proviene da un hardware diverso. Non utilizzare questa opzione, a meno che non si sappia cosa si stia facendo e si accetti il rischio di distruggere la radio! Questa radio ha un modo complesso per entrare in modalità programma,
anche il software originale richiede alcuni tentativi per entrare.
Proverò 8 volte (la maggior parte delle volte ne bastano circa 3) e questo
può richiedere alcuni secondi, se non funziona, riprovare più volte.
Se riuscite a entrare, controllate la radio ed il cavo.
 Questa opzione deve essere attivata solo se si utilizza un firmware modificato che supporta una copertura di frequenza più ampia. L'attivazione di questa opzione fa sì che CHIRP non applichi le restrizioni OEM e può portare ad un comportamento non definito o non regolamentato. Utilizzare a proprio rischio e pericolo! Questo strumento carica i dettagli del vostro sistema in un problema esistente sul tracker di CHIRP. Per funzionare, richiede il nome utente e la password di chirpmyradio.com. Verranno caricate informazioni sul sistema, compresi il log di debug, il file di configurazione e qualsiasi file immagine aperto. Si cercherà di eliminare tutte le informazioni personali prima che lascino il sistema. Caricherà un modulo dal sito Tono Modalità tono Squelch del tono Modalità squelch del tono Potenza di trasmissione Shift della trasmissione, modalità split o inibizione della trasmissione Tono di trasmissione Trasmette/riceve il codice DTCS per la modalità DTCS, altrimenti trasmette il codice Trasmissione/ricezione di modulazione (FM, AM, SSB, ecc.) Tono di trasmissione/ricezione per la modalità TSQL, altrimenti tono di ricezione Passo della sintonia Digita una semplice stringa di ricerca o una query formattata e premi invio Trova porta USB Impossibile determinare la porta per il cavo. Controllare i driver e i collegamenti. Impossibile modificare la memoria prima di averla caricata dalla radio Impossibile trovare la configurazione standard %r Impossibile importare mentre la visualizzazione è ordinata Impossibile aprire gli appunti Impossibile eseguire la ricerca Impossibile leggere l'ultimo blocco. Questo accade spesso, quando il modello selezionato è US, ma la radio è un modello non US (o widebanded). Scegliere il modello corretto e riprovare. Impossibile rivelare %s su questo sistema Impossibile impostare %s su questa memoria Impossibile caricare questo file Annulla Stati Uniti Scollegare il cavo (se necessario) e quindi fare clic su OK Modello non supportato %r Aggiorna un bug esistente Aggiornamento del bug %s Istruzioni per il caricamento Il caricamento è disabilitato a causa di una versione del firmware non supportata Carica sulla radio Carica sulla radio... Memoria caricata %s Usa carattere a larghezza fissa Usa carattere più grande Nome utente Il valore non rientra in %i bit Il valore deve essere almeno %.4f Il valore deve essere al massimo %.4f Il valore deve essere di esattamente %i cifre decimali Il valore deve essere maggiore o uguale a zero Valore da ricercare nel campo della memoria Valori Produttore Visualizza ATTENZIONE! Attenzione Attenzione: %s Benvenuto Con questa opzione selezionata, una ricerca di prossimità includerà i risultati memorizzati nella cache di altre località, se presenti nel range, se corrispondono ad altri parametri del filtro e se sono stati scaricati in precedenza. Installare un'icona sul desktop? Hai aperto più segnalazioni nell'ultima settimana. Per evitare abusi, CHIRP limita il numero di segnalazioni che è possibile aprire. Se hai davvero bisogno di aprirne un altro, fallo tramite il sito. Il dispositivo USB basato su Prolific non funzionerà senza tornare a una versione precedente del driver. Visitare il sito web di CHIRP, per saperne di più su come risolvere questo problema? Il tuo QTH Locator Il cavo sembra collegato alla porta: bit byte byte ciascuno disabilitato abilitato {bank} è pieno 