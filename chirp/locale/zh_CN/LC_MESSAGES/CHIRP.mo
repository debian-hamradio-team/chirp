��    �     <  �  \      x(  -   y(  9   �(     �(  ;   �(  +   3)  )   _)  &   �)     �)  I   �)     *      *  6   /*    f*  �   �+  �  ,  �   �-  �   =.  �   �.  �   t/    X0  �   o1    d2  �   j3  �   d4  �   \5  �   X6    H7  o  T8  q  �9  �   6;  �   <  p  �<  e  Y>  �   �?  �   �@  �   uA  �   eB  �   RC  �   =D  �   %E  	  #F  �   -G  �   �G  �   �H  �   �I  �   bJ  �   CK  #  !L    EM  M  QN  �   �O  �   ZP  �   Q  �   �Q    �R    �S  �   �T  �   �U  �   wV  m   �V  �   fW  �   EX  �   /Y  �   �Y    �Z  �   �[  �   �\  �   �]  ^   �^  %   _     )_     /_     ;_     A_  	   E_     O_     f_     �_     �_     �_     �_     �_     �_     �_     �_     �_     �_     �_     `     `     +`  <   B`     `  ]   �`  J   �`     /a     Aa     Qa     ea     ta     �a     �a  (   �a     �a      �a    �a  ,   c     Ec     Mc     `c     qc  
   wc  %   �c     �c     �c     �c  Q   �c  k   "d     �d     �d     �d  
   �d     �d  	   �d     �d     �d     �d     �d  	   �d     �d     e     e     e     $e  A   3e     ue     �e     �e     �e     �e     �e     �e     �e      �e     f     #f     7f     Nf     df     kf     ~f  C   �f     �f     �f     �f     g     (g     0g     @g     Sg     lg     �g  �   �g  �   !h  (   �h  +   %i  �   Qi     �i     �i     �i  $   j     @j     Tj     tj     �j     �j     �j  e   �j  u   k     ~k     �k     �k     �k     �k     �k     �k     l     
l  1   l     Cl  	   Hl     Rl     Zl  �   pl  �   ;m  �   n    �n  �   �o  �   �p  �   >q  �   �q  �   �r  �   \s  8  t  �   Su  �   �u  �   �v  �   Gw  �   �w  �   �x  �   8y  .   �y  	   z  &   z     <z  +   Yz  (   �z  -   �z     �z     �z     �z     �z     {     {  
   {     #{     '{  Y   ;{  ,   �{  7   �{     �{     |     |     %|     <|     B|     G|     S|     d|     z|     �|  '   �|     �|     �|     �|  "   �|      }     2}     @}     E}     N}     W}     i}     u}     �}  4   �}  	   �}  
   �}     �}     �}     �}  �   ~  v  �~     �  .   '�     V�     t�  	   ��     ��  :   ��     ��     ��  %   �     @�     `�     e�     k�     q�     x�     ��  	   ��     ��  	   ȁ     ҁ  5   ځ  
   �     �     1�     F�     W�  7   t�  
   ��     ��     Â     ʂ     т     �      ��     �     /�     4�     @�     R�     ^�     l�     {�     ��     ��     ��     Ń     ك     �      ��     �  �   .�     ��     ��     Ƅ  3   ̄  *    �  (   +�  &   T�  ?   {�  �  ��  (  ��    ��     ��  $   ɉ     �     �  !   ��     �     )�     2�  
   F�     Q�     Z�     g�     o�     u�     ��     ��  �   ��  ;   X�  t   ��     	�     �     -�  	   4�     >�     S�     d�     x�     ��     ��     ��     Č  W   Ќ     (�     B�     Z�  �   l�     %�     6�     U�     k�     ~�     ��  	   ��     ��  +   ��  	   ݎ  	   �     �     ��     �     �     /�     <�     N�     V�  :   _�  9   ��     ԏ     �     ��     �     )�  .   .�     ]�     }�  3   ��  5   ѐ     �     �     %�     -�     3�     B�     J�  (   h�  �   ��     #�  �  B�  �   �  �   �  �   ��  ,   ��  ^  ��     �  �   �  G   י  �   �  |   �  �   ��  #   U�  =   y�  <  ��  �   ��  V   ٞ     0�     M�  +  l�  &  ��  �   ��  r  ��  ,   !�     N�  	   S�     ]�     j�     |�  /   ��     ��  <   ɤ  .   �  6   5�     l�     x�  L   ��  ,   ե     �  )   !�     K�     h�  �   x�  "   "�     E�     e�     ��  /   ��     ��     ӧ     �     ��  6   �     E�     U�     h�     }�     ��     ��     ��     Ш  '   �     �     1�     8�     ?�     D�     M�     U�     a�  7   i�  �   ��  �   X�  !   ��     �     !�  
   '�     2�     ;�     C�  J  R�  .   ��     ̬     �     ��  9   �  !   N�  !   p�     ��  ]   ��  	   �     �  3   -�    a�  �   g�  �  �  �   ��  �   /�  �   Ȳ  �   g�  �   N�  �   7�  �   '�  �   �  �   �  �   ��  �   ��  
  o�  -  z�    ��  �   ý  �   ��    M�    b�  �   u�  �   ;�  �   ��  �   ��  �   ��  �   �  �   U�  �   0�  �   #�  �   ��  �   x�  �   E�  �   �  �   ��  �   ��  �   ��  L  p�  �   ��  �   f�  �   �  �   ��  �   y�  �   U�  �   .�  �   ��  r   ��  [   
�  �   f�  �   I�  �   
�  �   ��  �   ��  �   ��  �   w�  �   i�  @   Z�  &   ��     ��     ��     ��     ��     ��     ��     �     &�     -�     :�     G�     T�     a�     n�     u�     |�     ��     ��     ��     ��     ��  $   ��  	   �  E   �  :   T�     ��     ��     ��     ��     ��     ��     �  (   $�     M�  *   T�    �  3   ��     ��     ��     ��     ��     ��     ��     �     �  	   )�  Q   3�  g   ��     ��     ��     �     �     -�     =�     J�     Q�     V�     b�  	   n�     x�     ��     ��     ��     ��  B   ��     ��     �     �      �  	   -�     7�     >�     R�     h�     ��     ��     ��     ��     ��     ��     ��  0   ��     %�  "   ,�     O�     o�  	   ��     ��     ��     ��  "   ��     ��  u   �  �   w�  )   L�     v�  �   ��     �     ,�     B�  !   [�     }�     ��     ��     ��     ��     ��  R   ��  N   G�     ��     ��     ��     ��     ��     ��     
�  	   �     �  '   "�     J�     Q�  	   a�     k�  �   ��  �   7�  �   ��  �   ��  �   w�  �   �  �   ��  �   Z�  �   ��  �   ��  	  ^�  �   h�  �   ��  �   ��  {   5�  {   ��  ~   -�  {   ��  +   (�     T�     [�     x�  &   ��  &   ��  1   ��     �     �     &�     3�  	   C�     M�     T�     a�     n�  K   ��  0   ��     �     �     $�     7�     D�     T�     [�     b�     i�     |�     ��     ��  1   ��     ��     �     �  !   .�     P�     ]�     m�     t�     ��     ��     ��     ��     ��  '   ��     ��     �     �     ,�     C�  x   ]�  |  ��     S�  0   c�  "   ��  "   ��     ��     ��  3   ��     �  $   3�  -   X�  #   ��     ��     ��     ��     ��     ��     ��     ��     ��     �     �     �     8�     Q�     ^�     t�     ��  0   ��  	   ��     ��     ��     ��     ��     
�     �     9�     L�     S�     `�     s�     ��     ��     ��     ��     ��     ��     ��     ��     	          :     P     �     �     �  +   �  !    !   3 !   U 0   w �  � #  � �   � 	   � '   �    �    �    �    �    �    
     	   %    /    ?    Q    X    n    { �   � .   4 h   c    �    �    � 	   �    �            / !   H    j !   q    � ^   �    	    	    5	 �   H	    �	    
    (
    A
    Q
    X
    n
    {
 0   �
    �
 	   �
    �
    �
    �
        "    /    H    O 3   V 0   �    � !   �    � !   	    + !   2    T    p    �    �    �    �    �    �    �    �    � .    �   @    � �  � �   � �   Y �   �    � )  � 	   � �    <   � �   " `   � �   9 (    =   0   n �   � 9   E        �   � %  � �   � Q  � +    	   .    8    H    X    n *   {    � B   � 0   � B   *    m    z E   � *   �    �         :     P  �   ]      !    !    >!    T! 6   [!    �!    �!    �!    �! 0   �!    "    "    1"    D" 	   W"    a"    ~"    �" (   �"    �"    �"     #    # 	   #    #    #    +# 7   2# �   j# �   $    �$    �$    �$    �$ 	   �$ 	   %    %    �  f  �  �      �      �  �   �  �     w   �   c  M  �   �   �  �         �   y   �  .       �  �   G   �        �   �   =  �   V  Y           -   g         +   q  h                   F           >  f   �       �   *  �  �       �          �   �   �         -      �   o   �  �      s              �      �  6   �   �      �   ?  �  �   |   �   �  �   F  !   �   \   �   4  �  Q       �     !  �   �      �  �      ]         �   g   $            ^       �      �   �   �   /   M   A   P  �   p   �       �       �  �  y      �           �         S     8   ;  %   9  �   T   X       �  �   �      z     �       �  �   u   �   �   @   �  �     �       \  l  :      �       �  �    �           �   �  �         z           �   )       {  7   5   a     �   �  Y         1       �      �       #  �  (       	              *   �  �   r   �  �   
  `   �   _     P       �   E   J      e  �         h  |          H       u      �   H  �  �          �  �      c   �       A      �      �  L   Z  �  �  �  [   �  �   �       _  �         K    t   �  I          �      �   	  B   �   �   �   �   �  w  l   �  '  n      �   a      k   �   �   E  <       j  �   �  �   �   L      ,  b   �              �  �   3   �   C  �   '   �  @         �  T  U       �   �  �           �  �   5  �    K   �      �          �    �       �   �   q   �      m      ~   i  &             �       "  �  R  �  n       D       &          �   �  �  ]    �   �           ;                  m   b  +  �          j   �  =   �   Z      9       k      3  �  x  �   .  �  �   �      �  �   �              �   0  t      /            �   �       �      �      �  �    �   V   �   �   d   �   2  �   �  D  �       �  �              <  >   1      �     B  "   �             v   �  `      [  i   ?           7  �  ~  �       �   �  %      6  I       �  �   �  �       )              s       �  d        �   �   J   G          }           �       Q              �  e   R   �   {   W  �  �       �   �         r          �  U  �             $   O           �          W   �      �      �      �  ,       N   ^      �   �   �   p          �  �       �   N  �           4   :         �      S   X  �      C   �      �  �      (        �       }  #   �       �   x   �       8  0   �  
   �         �   2                   O  o      v  �   �   %(value)s must be between %(min)i and %(max)i %i Memories and shift all up %i Memories and shift all up %i Memory %i Memories %i Memory and shift block up %i Memories and shift block up %s has not been saved. Save before closing? (Describe what actually happened instead) (Describe what you expected to happen) (Describe what you were doing) (Has this ever worked before? New radio? Does it work with OEM software?) (none) ...and %i more 1. Connect programming cable to MIC jack.
2. Press OK. 1. Ensure your firmware version is 4_10 or higher
2. Turn radio off
3. Connect your interface cable
4. Turn radio on
5. Press and release PTT 3 times while holding MONI key
6. Supported baud rates: 57600 (default) and 19200
   (rotate dial while holding MONI to change)
7. Click OK
 1. Turn Radio off.
2. Connect data cable.
3. While holding "A/N LOW" button, turn radio on.
4. <b>After clicking OK</b>, press "SET MHz" to send image.
 1. Turn Radio off.
2. Connect data cable.
3. While holding "A/N LOW" button, turn radio on.
4. Press "MW D/MR" to receive image.
5. Make sure display says "-WAIT-" (see note below if not)
6. Click OK to dismiss this dialog and start transfer.
Note: if you don't see "-WAIT-" at step 5, try cycling
      power and pressing and holding red "*L" button to unlock
      radio, then start back at step 1.
 1. Turn Radio off.
2. Connect data cable.
3. While holding "F/W" button, turn radio on.
4. <b>After clicking OK</b>, press "RPT" to send image.
 1. Turn Radio off.
2. Connect data cable.
3. While holding "F/W" button, turn radio on.
4. Press "REV" to receive image.
5. Click OK to start transfer.
 1. Turn Radio off.
2. Connect data cable.
3. While holding "TONE" and "REV" buttons, turn radio on.
4. <b>After clicking OK</b>, press "TONE" to send image.
 1. Turn Radio off.
2. Connect data cable.
3. While holding "TONE" and "REV" buttons, turn radio on.
4. Press "REV" to receive image.
5. Make sure display says "CLONE RX" and green led is blinking
6. Click OK to start transfer.
 1. Turn radio off.
2. Connect cable
3. Press and hold in the MHz, Low, and D/MR keys on the radio while turning it on
4. Radio is in clone mode when TX/RX is flashing
5. <b>After clicking OK</b>, press the MHz key on the radio to send image.
    ("TX" will appear on the LCD). 
 1. Turn radio off.
2. Connect cable
3. Press and hold in the MHz, Low, and D/MR keys on the radio while turning it on
4. Radio is in clone mode when TX/RX is flashing
5. Press the Low key on the radio ("RX" will appear on the LCD).
6. Click OK. 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. <b>After clicking OK</b> here, press the [C.S.] key to
    send image.
 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. <b>After clicking OK</b>, press the [A] key to send image.
 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. Click OK here.
    ("Receiving" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. Press the [A](RCV) key ("receiving" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to ACC jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. Press the [C] key ("RX" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to CAT/LINEAR jack.
3. Press and hold in the [MODE &lt;] and [MODE &gt;] keys while
     turning the radio on ("CLONE MODE" will appear on the
     display).
4. <b>After clicking OK</b>,
     press the [C](SEND) key to send image.
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the "left" [V/M] key while turning the
     radio on.
4. Rotate the "right" DIAL knob to select "CLONE START".
5. Press the [SET] key. The display will disappear
     for a moment, then the "CLONE" notation will appear.
6. <b>After clicking OK</b>, press the "left" [V/M] key to
     send image.
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the "left" [V/M] key while turning the
     radio on.
4. Rotate the "right" DIAL knob to select "CLONE START".
5. Press the [SET] key. The display will disappear
     for a moment, then the "CLONE" notation will appear.
6. Press the "left" [LOW] key ("CLONE -RX-" will appear on
     the display).
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the [FW] key while turning the radio on
     ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [BAND] key to send image.
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the [FW] key while turning the radio on
     ("CLONE" will appear on the display).
4. Press the [MODE] key ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the [MHz(PRI)] key while turning the
     radio on.
4. Rotate the DIAL job to select "F-7 CLONE".
5. Press and hold in the [BAND(SET)] key. The display
     will disappear for a moment, then the "CLONE" notation
     will appear.
6. Press the [LOW(ACC)] key ("--RX--" will appear on the display).
 1. Turn radio off.
2. Connect cable to DATA jack.
3. Press and hold in the [MHz(PRI)] key while turning the
 radio on.
4. Rotate the DIAL job to select "F-7 CLONE".
5. Press and hold in the [BAND(SET)] key. The display
 will disappear for a moment, then the "CLONE" notation
 will appear.
6. <b>After clicking OK</b>, press the [V/M(MW)] key to send image.
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold in the [F] key while turning the radio on
     ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [BAND] key to send image.
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold in the [F] key while turning the radio on
     ("CLONE" will appear on the display).
4. Press the [Dx] key ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold in the [MHz(SETUP)] key while turning the radio
     on ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [REV(DW)] key
     to send image.
 1. Turn radio off.
2. Connect cable to DATA terminal.
3. Press and hold in the [MHz(SETUP)] key while turning the radio
     on ("CLONE" will appear on the display).
4. Press the [MHz(SETUP)] key
     ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC Jack.
3. Press and hold in the [MHz(SETUP)] key while turning the radio
     on ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [GM(AMS)] key
     to send image.
 1. Turn radio off.
2. Connect cable to MIC Jack.
3. Press and hold in the [MHz(SETUP)] key while turning the radio
     on ("CLONE" will appear on the display).
4. Press the [MHz(SETUP)] key
     ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC/EAR jack.
3. Press and hold in the [F/W] key while turning the radio on
    ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [VFO(DW)SC] key to receive
    the image from the radio.
 1. Turn radio off.
2. Connect cable to MIC/EAR jack.
3. Press and hold in the [F/W] key while turning the radio on
    ("CLONE" will appear on the display).
4. Press the [MR(SKP)SC] key ("CLONE WAIT" will appear
    on the LCD).
5. Click OK to send image to radio.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold [PTT] &amp; Knob while turning the
     radio on.
4. <b>After clicking OK</b>, press the [PTT] switch to send image.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold [PTT] &amp; Knob while turning the
     radio on.
4. Press the [MONI] switch ("WAIT" will appear on the LCD).
5. Press OK.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [F/W] key while turning the radio on
     ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [BAND] key to send image.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [F/W] key while turning the radio on
     ("CLONE" will appear on the display).
4. Press the [V/M] key ("-WAIT-" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [MON-F] key while turning the radio on
     ("CLONE" will appear on the display).
4. <b>After clicking OK</b>, press the [BAND] key to send image.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [MON-F] key while turning the radio on
     ("CLONE" will appear on the display).
4. Press the [V/M] key ("CLONE WAIT" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [MONI] switch while turning the
     radio on.
4. Rotate the DIAL job to select "F8 CLONE".
5. Press the [F/W] key momentarily.
6. <b>After clicking OK</b>, hold the [PTT] switch
     for one second to send image.
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [MONI] switch while turning the
     radio on.
4. Rotate the DIAL job to select "F8 CLONE".
5. Press the [F/W] key momentarily.
6. Press the [MONI] switch ("--RX--" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to MIC/SP jack.
3. Press and hold in the [moni] key while turning the radio on.
4. Select CLONE in menu, then press F. Radio restarts in clone mode.
     ("CLONE" will appear on the display).
5. <b>After clicking OK</b>, briefly hold [PTT] key to send image.
    ("-TX-" will appear on the LCD). 
 1. Turn radio off.
2. Connect cable to mic jack.
3. Press and hold in the [LOW(A/N)] key while turning the radio on.
4. <b>After clicking OK</b>, press the [MHz(SET)] key to send image.
 1. Turn radio off.
2. Connect cable to mic jack.
3. Press and hold in the [LOW(A/N)] key while turning the radio on.
4. Press the [D/MR(MW)] key ("--WAIT--" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to mic jack.
3. Press and hold in the [MHz], [LOW], and [D/MR] keys
   while turning the radio on.
4. <b>After clicking OK</b>, press the [MHz(SET)] key to send image.
 1. Turn radio off.
2. Connect cable to mic jack.
3. Press and hold in the [MHz], [LOW], and [D/MR] keys
   while turning the radio on.
4. Press the [D/MR(MW)] key ("--WAIT--" will appear on the LCD).
 1. Turn radio off.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Turn radio on (volume may need to be set at 100%).
5. Ensure that the radio is tuned to channel with no activity.
6. Click OK to download image from device.
 1. Turn radio off.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Turn radio on (volume may need to be set at 100%).
5. Ensure that the radio is tuned to channel with no activity.
6. Click OK to upload image to device.
 1. Turn radio off.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Turn radio on.
5. Ensure that the radio is tuned to channel with no activity.
6. Click OK to download image from device.
 1. Turn radio off.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Turn radio on.
5. Ensure that the radio is tuned to channel with no activity.
6. Click OK to upload image to device.
 1. Turn radio off.
2. Connect data cable.
3. Prepare radio for clone.
4. <b>After clicking OK</b>, press the key to send image.
 1. Turn radio off.
2. Connect data cable.
3. Prepare radio for clone.
4. Press the key to receive the image.
 1. Turn radio off.
2. Connect mic and hold [ACC] on mic while powering on.
    ("CLONE" will appear on the display)
3. Replace mic with PC programming cable.
4. <b>After clicking OK</b>, press the [SET] key to send image.
 1. Turn radio off.
2. Connect mic and hold [ACC] on mic while powering on.
    ("CLONE" will appear on the display)
3. Replace mic with PC programming cable.
4. Press the [DISP/SS] key
    ("R" will appear on the lower left of LCD).
 1. Turn radio off.
2. Remove front head.
3. Connect data cable to radio, use the same connector where
     head was connected to, <b>not the mic connector</b>.
4. Click OK.
 1. Turn radio off.
3. Press and hold in the [moni] key while turning the radio on.
4. Select CLONE in menu, then press F. Radio restarts in clone mode.
     ("CLONE" will appear on the display).
5. Press the [moni] key ("-RX-" will appear on the LCD).
 1. Turn radio on.
2. Connect cable to DATA terminal.
3. Unclip battery.
4. Press and hold in the [AMS] key and power key while clipping 
 in back battery the ("ADMS" will appear on the display).
5. Press the [MODE] key ("-WAIT-" will appear on the LCD).
<b>Then click OK</b> 1. Turn radio on.
2. Connect cable to DATA terminal.
3. Unclip battery.
4. Press and hold in the [AMS] key and power key while clipping 
 in back battery the("ADMS" will appear on the display).
5. <b>After clicking OK</b>, press the [BAND] key.
 1. Turn radio on.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Click OK to download image from device.

It will may not work if you turn on the radio with the cable already attached
 1. Turn radio on.
2. Connect cable to mic/spkr connector.
3. Make sure connector is firmly connected.
4. Click OK to upload the image to device.

It will may not work if you turn on the radio with the cable already attached A new CHIRP version is available. Please visit the website as soon as possible to download it! AM mode does not allow duplex or tone About About CHIRP Agree All All Files All supported formats| Always start with recent list Amateur An error has occurred Applying settings Automatic from system Available modules Bandplan Bands Banks Bin Browser Bug number not found Bug number: Bug subject: Building Radio Browser CHIRP must be restarted for the new selection to take effect Canada Changing this setting requires refreshing the settings from the image, which will happen now. Channels with equivalent TX and RX %s are represented by tone mode of "%s" Chirp Image Files Choice Required Choose %s DTCS Code Choose %s Tone Choose Cross Mode Choose a recent model Choose duplex Choose the module to load from issue %i: City Click here for full license text Click on the "Special Channels" toggle-button of the memory
editor to see/set the EXT channels. P-VFO channels 100-109
are considered Settings.
Only a subset of the over 200 available radio settings
are supported in this release.
Ignore the beeps from the radio on upload and download.
 Clone completed, checking for spurious bytes Cloning Cloning from radio Cloning to radio Close Close file Cluster %i memory Cluster %i memories Comment Communicate with radio Complete Complex or non-standard tone squelch mode (starts the tone mode selection wizard) Connect your interface cable to the PC Port on the
back of the 'TX/RX' unit. NOT the Com Port on the head.
 Convert to FM Copy Country Cross Mode Custom Port Custom... Cut DTCS DTCS
Polarity DTMF decode DV Memory Danger Ahead Dec Delete Detailed information Developer Mode Developer state is now %s. CHIRP must be restarted to take effect Diff Raw Memories Digital Code Digital Modes Disable reporting Disabled Distance Do not prompt again for %s Do you accept the risk? Double-click to change bank name Download Download from radio Download from radio... Download instructions Driver Driver information Driver messages Dual-mode digital repeaters that support analog will be shown as FM Duplex Edit details for %i memories Edit details for memory %i Enable Automatic Edits Enabled Enter Frequency Enter Offset (MHz) Enter TX Frequency (MHz) Enter a new name for bank %s: Enter custom port: Enter details about this update. Be descriptive about what you were doing, what you expected to happen, and what actually happened. Enter information about the bug including a short but meaningful subject and information about the radio model (if applicable) in question. In the next step you will have a chance to add more details about the problem. Enter information to add to the bug here Enter the bug number that should be updated Enter your username and password for chirpmyradio.com. If you do not have an account click below to create one before proceeding. Erased memory %s Error applying settings Error communicating with radio Exclude private and closed repeaters Experimental driver Export can only write CSV files Export to CSV Export to CSV... Extra FM Radio FREE repeater database, which provides information
about repeaters in Europe. No account is required. FREE repeater database, which provides most up-to-date
information about repeaters in Europe. No account is
required. Failed to load radio browser Failed to parse result Failed to send bug report: Features File a new bug File does not exist: %s Files Files: Filter Filter results with location matching this string Find Find Next Find... Finished radio job %s Follow these instructions to download the radio memory:
1 - Connect your interface cable
2 - Radio > Download from radio: DO NOT mess with the radio
during download!
3 - Disconnect your interface cable
 Follow these instructions to download the radio memory:
1 - Connect your interface cable
2 - Radio > Download from radio: Don't adjust any settings
on the radio head!
3 - Disconnect your interface cable
 Follow these instructions to download the radio memory:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio, volume @ 50%
4 - CHIRP Menu - Radio - Download from radio
 Follow these instructions to download your config:
1 - Turn off your radio
2 - Connect your interface cable to the Speaker-2 jack
3 - Turn on your radio
4 - Radio > Download from radio
5 - Disconnect the interface cable! Otherwise there will be
    no right-side audio!
 Follow these instructions to download your info:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the download of your radio data
 Follow these instructions to download your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio (unlock it if password protected)
4 - Click OK to start
 Follow these instructions to read your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Click OK to start
 Follow these instructions to upload the radio memory:
1 - Connect your interface cable
2 - Radio > Upload to radio: DO NOT mess with the radio
during upload!
3 - Disconnect your interface cable
 Follow these instructions to upload the radio memory:
1 - Connect your interface cable
2 - Radio > Upload to radio: Don't adjust any settings
on the radio head!
3 - Disconnect your interface cable
 Follow these instructions to upload the radio memory:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio, volume @ 50%
4 - CHIRP Menu - Radio - Upload to radio
 Follow these instructions to upload your config:
1 - Turn off your radio
2 - Connect your interface cable to the Speaker-2 jack
3 - Turn on your radio
4 - Radio > Upload to radio
5 - Disconnect the interface cable, otherwise there will be
    no right-side audio!
6 - Cycle power on the radio to exit clone mode
 Follow these instructions to upload your info:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the upload of your radio data
 Follow these instructions to upload your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio (unlock it if password protected)
4 - Click OK to start
 Follow these instructions to write your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Click OK to start
 Follow this instructions to download your info:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the download of your radio data
 Follow this instructions to read your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the download of your radio data
 Follow this instructions to upload your info:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the upload of your radio data
 Follow this instructions to write your radio:
1 - Turn off your radio
2 - Connect your interface cable
3 - Turn on your radio
4 - Do the upload of your radio data
 Found empty list value for %(name)s: %(value)r Frequency Frequency %s is out of supported range Frequency granularity in kHz Frequency in this range must not be AM mode Frequency in this range requires AM mode Frequency outside TX bands must be duplex=off GMRS Getting settings Goto Memory Goto Memory: Goto... Help Help Me... Hex Hide empty memories Honor the CTCSS/DCS receive squelch configuration when enabled, else only carrier squelch Human-readable comment (not stored in radio) If set, sort results by distance from these coordinates Import Import from file... Import messages Import not recommended Index Info Information Insert Row Above Install desktop icon? Interact with driver Internal driver error Invalid %(value)s (use decimal degrees) Invalid Entry Invalid ZIP code Invalid edit: %s Invalid or unsupported module file Invalid value: %r Issue number: LIVE Language Latitude Length must be %i Limit Bands Limit Modes Limit Status Limit results to this distance (km) from coordinates Limit use Live Radio Load Module... Load module from issue Load module from issue... Loading a module will not affect open tabs. It is recommended (unless instructed otherwise) to close all tabs before loading a module. Loading modules can be extremely dangerous, leading to damage to your computer, radio, or both. NEVER load a module from a source you do not trust, and especially not from anywhere other than the main CHIRP website (chirpmyradio.com). Loading a module from another source is akin to giving them direct access to your computer and everything on it! Proceed despite this risk? Loading settings Login failed: Check your username and password Logo string 1 (12 characters) Logo string 2 (12 characters) Longitude Memories Memories are read-only due to unsupported firmware version Memory %i is not deletable Memory label (stored in radio) Memory must be in a bank to be edited Memory {num} not in bank {bank} Mode Model Modes Module Module Loaded Module loaded successfully More Info More than one port found: %s Move Down Move Up Move operations are disabled while the view is sorted New Window New version available No empty rows below! No modules found No modules found in issue %i No more space available; some memories were not applied No results No results! Number Offset Only certain bands Only certain modes Only memory tabs may be exported Only working repeaters Open Open Recent Open Stock Config Open a file Open a module Open debug log Open in new window Open repeaters only Open stock config directory Option Optional: -122.0000 Optional: 100 Optional: 45.0000 Optional: County, Hospital, etc. Overwrite memories? P-VFO channels 100-109 are considered Settings.
Only a subset of the over 130 available radio settings
are supported in this release.
 Parsing Password Paste Pasted memories will overwrite %s existing memories Pasted memories will overwrite memories %s Pasted memories will overwrite memory %s Pasted memory will overwrite memory %s Please be sure to quit CHIRP before installing the new version! Please follow this steps carefully:
1 - Turn on your radio
2 - Connect the interface cable to your radio
3 - Click the button on this window to start download
    (you may see another dialog, click ok)
4 - Radio will beep and led will flash
5 - You will get a 10 seconds timeout to press "MON" before
    data upload start
6 - If all goes right radio will beep at end.
After cloning remove the cable and power cycle your radio to
get into normal mode.
 Please follow this steps carefully:
1 - Turn on your radio
2 - Connect the interface cable to your radio.
3 - Click the button on this window to start download
    (Radio will beep and led will flash)
4 - Then press the "A" button in your radio to start cloning.
    (At the end radio will beep)
 Please note that developer mode is intended for use by developers of the CHIRP project, or under the direction of a developer. It enables behaviors and functions that can damage your computer and your radio if not used with EXTREME care. You have been warned! Proceed anyway? Please wait Plug in your cable and then click OK Port Power Press enter to set this in memory Print Preview Printing Prolific USB device Properties Query %s Query Source RX DTCS Radio Radio did not ack block %i Radio information Radio model: Radio sent data after the last awaited block, this happens when the selected model is a non-US but the radio is a US one. Please choose the correct model and try again. RadioReference Canada requires a login before you can query RadioReference.com is the world's largest
radio communications data provider
<small>Premium account required</small> Receive DTCS code Receive frequency Recent Recent... Recommend using 55.2 Refresh required Refreshed memory %s Reload Driver Reload Driver and File Remove Remove selected model from list Rename bank RepeaterBook is Amateur Radio's most comprehensive,
worldwide, FREE repeater directory. Report or update a bug... Reporting a new bug: %r Reporting enabled Reporting helps the CHIRP project know which radio models and OS platforms to spend our limited efforts on. We would really appreciate if you left it enabled. Really disable reporting? Restart Required Restore %i tab Restore %i tabs Restore tabs on start Retrieved settings Save Save before closing? Save file Saved settings Scan control (skip, include, priority, etc) Scanlists Scrambler Security Risk Select Bandplan... Select Bands Select Language Select Modes Select a bandplan Service Settings Settings are read-only due to unsupported firmware version Shift amount (or transmit frequency) controlled by duplex Show Raw Memory Show debug log location Show extra fields Show image backup location Skip Some memories are incompatible with this radio Some memories are not deletable Sort %i memory Sort %i memories Sort %i memory ascending Sort %i memories ascending Sort %i memory descending Sort %i memories descending Sort by column: Sort memories Sorting State State/Province Success Successfully sent bug report: TX-RX DTCS polarity (normal or reversed) Tested and mostly works, but may give you issues
when using lesser common radio variants.
Proceed at your own risk, and let us know about issues! The DMR-MARC Worldwide Network The FT-450D radio driver loads the 'Special Channels' tab
with the PMS scanning range memories (group 11), 60meter
channels (group 12), the QMB (STO/RCL) memory, the HF and
50m HOME memories and all the A and B VFO memories.
There are VFO memories for the last frequency dialed in
each band. The last mem-tune config is also stored.
These Special Channels allow limited field editing.
This driver also populates the 'Other' tab in the channel
memory Properties window. This tab contains values for
those channel memory settings that don't fall under the
standard Chirp display columns. With FT450 support, the gui now
uses the mode 'DIG' to represent data modes and a new column
'DATA MODE' to select USER-U, USER-L and RTTY 
 The X3Plus driver is currently experimental.
There are no known issues but you should proceed with caution.
Please save an unedited copy of your first successful
download to a CHIRP Radio Images (*.img) file.
 The author of this module is not a recognized CHIRP developer. It is recommended that you not load this module as it could pose a security risk. Proceed anyway? The debug log file is not available when CHIRP is run interactively from the command-line. Thus, this tool will not upload what you expect. It is recommended that you quit now and run CHIRP non-interactively (or with stdin redirected to /dev/null) The following information will be submitted: The recommended procedure for importing memories is to open the source file and copy/paste memories from it into your target image. If you continue with this import function, CHIRP will replace all memories in your currently-open file with those in %(file)s. Would you like to open this file to copy/paste memories across, or proceed with the import? This Memory This driver has been tested with v3 of the ID-5100. If your radio is not fully updated please help by opening a bug report with a debug log so we can add support for the other revisions. This driver is in development and should be considered as experimental. This image is missing firmware information. It may have been generated with an old or modified version of CHIRP. It is advised that you download a fresh image from your radio and use that going forward for the best safety and compatibility. This is a live-mode radio, which means changes are sent to the radio in real-time as you make them. Upload is not necessary! This is a radio-independent file and cannot be uploaded directly to a radio. Open a radio image (or download one from a radio) and then copy/paste items from this tab into that one in order to upload This is an early stage beta driver
 This is an early stage beta driver - upload at your own risk
 This is an experimental driver for the Quansheng UV-K5. It may harm your radio, or worse. Use at your own risk.

Before attempting to do any changes please download the memory image from the radio with chirp and keep it. This can be later used to recover the original settings. 

some details are not yet implemented This is experimental support for BJ-9900 which is still under development.
Please ensure you have a good backup with OEM software.
Also please send in bug and enhancement requests!
You have been warned. Proceed at your own risk! This is the ticket number for an already-created issue on the chirpmyradio.com website This memory and shift all up This memory and shift block up This option may break your radio! Each radio has a unique set of calibration data and uploading the data from the image will cause physical harm to the radio if it is from a different piece of hardware. Do not use this unless you know what you are doing and accept the risk of destroying your radio! This radio has a tricky way of enter into program mode,
even the original software has a few tries to get inside.
I will try 8 times (most of the time ~3 will doit) and this
can take a few seconds, if don't work, try again a few times.
If you can get into it, please check the radio and cable.
 This should only be enabled if you are using modified firmware that supports wider frequency coverage. Enabling this will cause CHIRP not to enforce OEM restrictions and may lead to undefined or unregulated behavior. Use at your own risk! This tool will upload details about your system to an existing issue on the CHIRP tracker. It requires your username and password for chirpmyradio.com in order to work. Information about your system, including your debug log, config file, and any open image files will be uploaded. An attempt will be made to redact any personal information before it leaves your system. This will load a module from a website issue Tone Tone Mode Tone Squelch Tone squelch mode Transmit Power Transmit shift, split mode, or transmit inhibit Transmit tone Transmit/receive DTCS code for DTCS mode, else transmit code Transmit/receive modulation (FM, AM, SSB, etc) Transmit/receive tone for TSQL mode, else receive tone Tuning Step USB Port Finder Unable to determine port for your cable. Check your drivers and connections. Unable to edit memory before radio is loaded Unable to find stock config %r Unable to import while the view is sorted Unable to open the clipboard Unable to query Unable to read last block. This often happens when the selected model is US but the radio is a non-US one (or widebanded). Please choose the correct model and try again. Unable to reveal %s on this system Unable to set %s on this memory Unable to upload this file United States Unplug your cable (if needed) and then click OK Unsupported model %r Update an existing bug Updating bug %s Upload instructions Upload is disabled due to unsupported firmware version Upload to radio Upload to radio... Use fixed-width font Use larger font Username Value does not fit in %i bits Value must be at least %.4f Value must be at most %.4f Value must be exactly %i decimal digits Value must be zero or greater Values Vendor View WARNING! Warning Warning: %s Welcome Would you like CHIRP to install a desktop icon for you? You have opened multiple issues within the last week. CHIRP limits the number of issues you can open to avoid abuse. If you really need to open another, please do so via the website. Your Prolific-based USB device will not work without reverting to an older version of the driver. Visit the CHIRP website to read more about how to resolve this? Your cable appears to be on port: bits bytes bytes each disabled enabled {bank} is full Project-Id-Version: CHIRP
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-11-11 02:28+0800
Last-Translator: DuckSoft, BH2UEP <realducksoft@gmail.com>
Language-Team: 
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 3.5
 %(value)s 必须在 %(min)i 和 %(max)i 之间 %i 个存储并将所有上移 %i 个存储 %i 个存储并上移区块 %s 还没有被保存。要在关闭程序前保存吗？ （描述实际发生的情况） （描述期望发生的情况） （描述你做了什么） （这之前曾经奏效过吗？是新电台吗？使用原厂软件能正常工作吗？） （无） ...以及其他 %i 个 1. 将编程线连接到 MIC 插孔。
2. 按 OK。 1. 确保你的固件版本是 4_10 或更高
2. 关闭电台
3. 连接你的接口线
4. 打开电台
5. 按住 MONI 键不放，点按 PTT 键 3 次
6. 支持的波特率：57600（默认）和 19200
   （按住 MONI 键旋转旋钮以更改）
7. 点击 OK
 1. 关闭电台。
2. 连接数据线。
3. 在按住 "A/N LOW" 键的同时，打开电台。
4. <b>点击 OK 后</b>，按 "SET MHz" 键发送镜像。
 1. 关闭电台。
2. 连接数据线。
3. 在按住 "A/N LOW" 键的同时，打开电台。
4. 按 "MW D/MR" 键接收镜像。
5. 确保显示屏显示 "-WAIT-"（如果没有，请参见下面的说明）
6. 点击 OK 关闭此对话框并开始传输。
说明：如果在第 5 步看不到 "-WAIT-"，请尝试重新开关电源
      并按住红色 "*L" 键解锁电台，然后从第 1 步开始。
 1. 关闭电台。
2. 连接数据线。
3. 在按住 "F/W" 键的同时，打开电台。
4. <b>点击 OK 后</b>，按 "RPT" 键发送镜像。
 1. 关闭电台。
2. 连接数据线。
3. 在按住 "F/W" 键的同时，打开电台。
4. 按 "REV" 键接收镜像。
5. 点击 OK 开始传输。
 1. 关闭电台。
2. 连接数据线。
3. 在按住 "TONE" 和 "REV" 键的同时，打开电台。
4. <b>点击 OK 后</b>，按 "TONE" 键发送镜像。
 1. 关闭电台。
2. 连接数据线。
3. 在按住 "TONE" 和 "REV" 键的同时，打开电台。
4. 按 "REV" 键接收镜像。
5. 确保显示屏显示 "CLONE RX" 并且绿色指示灯在闪烁
6. 点击 OK 开始传输。
 1. 关闭电台。
2. 连接数据线。
3. 按住电台上的 MHz、Low、D/MR 三个键的同时，打开电台。
4. 当 TX/RX 闪烁时，电台处于克隆模式
5. <b>点击 OK 后</b>，按电台上的 MHz 键发送镜像。
 1. 关闭电台。
2. 连接数据线。
3. 按住电台上的 MHz、Low、D/MR 三个键的同时，打开电台。
4. 当 TX/RX 闪烁时，电台处于克隆模式
5. 按电台上的 Low 键（LCD 上会显示 "RX"）。
6. 点击 OK。 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 [MODE &lt;] 和 [MODE &gt;] 两个键的同时，打开电台
    （显示屏上会显示 "CLONE MODE"）。
4. <b>点击 OK 后</b>，按电台上的 [C.S.] 键发送镜像。
 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 [MODE &lt;] 和 [MODE &gt;] 两个键的同时，打开电台
    （显示屏上会显示 "CLONE MODE"）。
4. <b>点击 OK 后</b>，按电台上的 [A] 键发送镜像。
 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 [MODE &lt;] 和 [MODE &gt;] 两个键的同时，打开电台
 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 [MODE &lt;] 和 [MODE &gt;] 两个键的同时，打开电台
    （显示屏上会显示 "CLONE MODE"）。
4. 按电台上的 [A](RCV) 键（LCD 上会显示 "receiving"）。
 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 [MODE &lt;] 和 [MODE &gt;] 两个键的同时，打开电台
    （显示屏上会显示 "CLONE MODE"）。
4. 按电台上的 [C] 键（LCD 上会显示 "RX"）。
 1. 关闭电台。
2. 连接数据线到 CAT/LINEAR。
3. 在按住电台上的 [MODE &lt;] 和 [MODE &gt;] 两个键的同时，打开电台
    （显示屏上会显示 "CLONE MODE"）。
4. <b>点击 OK 后</b>，
    按电台上的 [C](SEND) 键发送镜像。
 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 "left" [V/M] 键的同时，打开电台。
4. 旋转 "right" DIAL 旋钮选择 "CLONE START"。
5. 按 [SET] 键。显示屏会消失一会儿，然后会显示 "CLONE"。
6. <b>点击 OK 后</b>，按 "left" [V/M] 键发送镜像。
 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 "left" [V/M] 键的同时，打开电台。
4. 旋转 "right" DIAL 旋钮选择 "CLONE START"。
5. 按 [SET] 键。短暂熄屏后显示 "CLONE"。
6. 按 "left" [LOW] 键（显示屏上会显示 "CLONE -RX-"）。
 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 [FW] 键的同时，打开电台
    （显示屏上会显示 "CLONE"）。
4. <b>点击 OK 后</b>，按 [BAND] 键发送镜像。
 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 [FW] 键的同时，打开电台
    （显示屏上会显示 "CLONE"）。
4. 按 [MODE] 键（LCD 上会显示 "-WAIT-"）。
 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 [MHz(PRI)] 键的同时，打开电台。
4. 旋转 DIAL 旋钮选择 "F-7 CLONE"。
5. 在按住 [BAND(SET)] 键。短暂熄屏后显示 "CLONE"。
6. 按 [LOW(ACC)] 键（显示屏上会显示 "--RX--"）。
 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 [MHz(PRI)] 键的同时，打开电台。
4. 旋转 DIAL 旋钮选择 "F-7 CLONE"。
5. 在按住 [BAND(SET)] 键。短暂熄屏后显示 "CLONE"。
6. <b>点击 OK 后</b>，按 [V/M(MW)] 键发送镜像。
 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 [F] 键的同时，打开电台
    （显示屏上会显示 "CLONE"）。
4. <b>点击 OK 后</b>，按 [BAND] 键发送镜像。
 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 [F] 键的同时，打开电台
    （显示屏上会显示 "CLONE"）。
4. 按 [Dx] 键（LCD 上会显示 "-WAIT-"）。
 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 [MHz(SETUP)] 键的同时，打开电台
    （显示屏上会显示 "CLONE"）。
4. <b>点击 OK 后</b>，按 [REV(DW)] 键
    发送镜像。
 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 [MHz(SETUP)] 键的同时，打开电台
    （显示屏上会显示 "CLONE"）。
4. 按 [MHz(SETUP)] 键
    （LCD 上会显示 "-WAIT-"）。
 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 [MHz(SETUP)] 键的同时，打开电台
    （显示屏上会显示 "CLONE"）。
4. <b>点击 OK 后</b>，按 [GM(AMS)] 键
    发送镜像。
 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 [MHz(SETUP)] 键的同时，打开电台
    （显示屏上会显示 "CLONE"）。
4. 按 [MHz(SETUP)] 键
    （LCD 上会显示 "-WAIT-"）。
 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 [F/W] 键的同时，打开电台
    （显示屏上会显示 "CLONE"）。
4. <b>点击 OK 后</b>，按 [VFO(DW)SC] 键
    接收电台的镜像。
 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 [F/W] 键的同时，打开电台
    （显示屏上会显示 "CLONE"）。
4. 按 [MR(SKP)SC] 键（LCD 上会显示 "CLONE WAIT"）。
5. 点击 OK 发送镜像到电台。
 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 [PTT] 和旋钮的同时，打开电台。
4. <b>点击 OK 后</b>，按 [PTT] 键发送镜像。
 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 [PTT] 和旋钮的同时，打开电台。
4. 按 [MONI] 键（LCD 上会显示 "WAIT"）。
5. 点击 OK。
 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 [F/W] 键的同时，打开电台
    （显示屏上会显示 "CLONE"）。
4. <b>点击 OK 后</b>，按 [BAND] 键
    发送镜像。
 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 [F/W] 键的同时，打开电台
    （显示屏上会显示 "CLONE"）。
4. 按 [V/M] 键（LCD 上会显示 "-WAIT-"）。
 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 [MON-F] 键的同时，打开电台
    （显示屏上会显示 "CLONE"）。
4. <b>点击 OK 后</b>，按 [BAND] 键
    发送镜像。
 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 [MON-F] 键的同时，打开电台
    （显示屏上会显示 "CLONE"）。
4. 按 [V/M] 键（LCD 上会显示 "CLONE WAIT"）。
 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 [MONI] 键的同时，打开电台。
4. 旋转 DIAL 旋钮选择 "F8 CLONE"。
5. 短按 [F/W] 键。
6. <b>点击 OK 后</b>，按住 [PTT] 键
    一秒钟发送镜像。
 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 [MONI] 键的同时，打开电台。
4. 旋转 DIAL 旋钮选择 "F8 CLONE"。
5. 短按 [F/W] 键。
6. 按 [MONI] 键（LCD 上会显示 "--RX--"）。
 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 [moni] 键的同时，打开电台。
4. 在菜单中选择 CLONE，然后按 F 键。
    电台会重新启动进入克隆模式
    （显示屏上会显示 "CLONE"）。
5. <b>点击 OK 后</b>，短按 [PTT] 键
    发送镜像（LCD 上会显示 "-TX-"）。
 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 [LOW(A/N)] 键的同时，打开电台。
4. <b>点击 OK 后</b>，按 [MHz(SET)] 键发送镜像。
 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 [LOW(A/N)] 键的同时，打开电台。
4. 按 [D/MR(MW)] 键（LCD 上会显示 "--WAIT--"）。
 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 [MHz]、[LOW] 和 [D/MR] 键的同时，打开电台。
4. <b>点击 OK 后</b>，按 [MHz(SET)] 键发送镜像。
 1. 关闭电台。
2. 连接数据线。
3. 在按住电台上的 [MHz]、[LOW] 和 [D/MR] 键的同时，打开电台。
4. 按 [D/MR(MW)] 键（LCD 上会显示 "--WAIT--"）。
 1. 关闭电台。
2. 连接数据线。
3. 确保连接器连接牢固。
4. 打开电台（音量可能需要设置为 100%）。
5. 确保电台调到没有活动的频道。
6. 点击 OK 下载设备中的镜像。
 1. 关闭电台。
2. 连接数据线。
3. 确保连接器连接牢固。
4. 打开电台（音量可能需要设置为 100%）。
5. 确保电台调到没有活动的频道。
6. 点击 OK 上传镜像到设备。
 1. 关闭电台。
2. 连接数据线。
3. 确保连接器连接牢固。
4. 打开电台。
5. 确保电台调到没有活动的频道。
6. 点击 OK 下载设备中的镜像。
 1. 关闭电台。
2. 连接数据线。
3. 确保连接器连接牢固。
4. 打开电台。
5. 确保电台调到没有活动的频道。
6. 点击 OK 上传镜像到设备。
 1. 关闭电台。
2. 连接数据线。
3. 准备克隆电台。
4. <b>点击 OK 后</b>，按键发送镜像。
 1. 关闭电台。
2. 连接数据线。
3. 准备克隆电台。
4. 按键接收镜像。
 1. 关闭电台。
2. 连接麦克风，同时按住麦克风上的 [ACC] 键打开电台。
    （显示屏上会显示 "CLONE"）
3. 用 PC 编程线替换麦克风。
4. <b>点击 OK 后</b>，按 [SET] 键发送镜像。
 1. 关闭电台。
2. 连接麦克风，同时按住麦克风上的 [ACC] 键打开电台。
    （显示屏上会显示 "CLONE"）
3. 用 PC 编程线替换麦克风。
4. 按 [DISP/SS] 键
 1. 关闭电台。
2. 拆下前面板。
3. 连接数据线到电台，使用与前面板连接的相同连接器，
     <b>不是麦克风连接器</b>。
4. 点击 OK。
 1. 关闭电台。
3. 按住 [moni] 键打开电台。
4. 在菜单中选择 CLONE，然后按 F。电台会重新启动进入克隆模式。
     （显示屏上会显示 "CLONE"）
5. 按 [moni] 键（显示屏上会显示 "-RX-"）
 1. 打开电台。
2. 连接数据线到 DATA 端口。
3. 拆下电池。
4. 同时按住 [AMS] 键和电源键，然后插上电池（显示屏上会显示 "ADMS"）。
5. 按 [MODE] 键（显示屏上会显示 "-WAIT-"）。
<b>然后点击 OK</b> 1. 打开电台。
2. 连接数据线到 DATA 端口。
3. 拆下电池。
4. 同时按住 [AMS] 键和电源键，然后插上电池（显示屏上会显示 "ADMS"）。
5. <b>点击 OK 后</b>，按 [BAND] 键。
 1. 打开电台。
2. 将数据线连接至麦克风和录音机接口。
3. 确保数据线与电台间连接牢固。
4. 点击 "确定 "从设备下载映像。

如果在已连接数据线的情况下打开电台，则可能无法进行
 1. 打开电台。
2. 将数据线连接至麦克风和录音机接口。
3. 确保数据线与电台间连接牢固。
4. 点击 "确定 "上传映像到设备。

如果在已连接数据线的情况下打开电台，则可能无法进行 新的 CHIRP 版本已经推出。请尽快访问网站下载！ AM制式下不支持双工或亚音频 关于 关于CHIRP 同意 全选 全部文件 所有支持的格式| 总是以最近的列表启动 业余 发生错误 应用设置 系统自动 可用模块 频带计划 频带 存储 条目 数据浏览器 Bug 编号未找到 Bug 编号： Bug 标题： 正在构建电台浏览器 CHIRP 必须重新启动才能生效 加拿大 更改此设置需要从映像中刷新设置，这将立即发生。 收发一致的 %s 频道使用 "%s" 亚音频模式表示 CHIRP 映像文件 需要选择 选择 %s DTCS 接收代码 选择 %s 亚音频 收发使用不同亚音频 选择一个最近的型号 选择双工 选择要从 issue %i 加载的模块： 城市 点击这里查看完整的许可证文本 点击记忆编辑器的“特殊频道”切换按钮以查看/设置 EXT 频道。
P-VFO 频道 100-109 被认为是设置。
在此版本中，仅支持 200 多个可用的电台设置中的一部分。
上传和下载时，请忽略电台发出的哔哔声。
 下载完成，正在检查是否有多余的字节 正在下载 从电台下载 上传到电台 关闭 关闭文件 Cluster %i 个记忆 备注 与电台通信 已完成 复杂或非标准的亚音频静噪模式（启动亚音频模式选择向导） 将接口电缆连接到“TX/RX”单元背面的 PC 端口。
请注意不是头部的 Com 端口。
 转换到 FM 复制 国家或地区 收发使用不同亚音频 自定义端口 自定义... 剪切 DTCS DTCS
极性 DTMF 解码 该存储 前方危险 探测 删除 详细信息 开发者模式 开发者状态现在是 %s。CHIRP 必须重新启动才能生效 对比原始存储 数字代码 数字模式 禁用报告 已禁用 距离 不再对 %s 提示 您接受风险吗？ 双击以更改存储区名称 下载 从电台下载 从电台下载…… 显示指引 驱动程序 驱动程序信息 驱动程序消息 支持模拟的双模数字中继将显示为 FM 双工 修改 %i 个存储的详细信息 修改存储 %i 的详细信息 启用自动编辑 已启用 频率 输入频差（MHz） 输入发射频率（MHz） 输入存储区 %s 的新名称： 输入自定义端口： 输入有关此更新的详细信息。描述您正在做什么，您期望发生什么以及实际发生了什么。 输入有关 Bug 的信息，包括一个简短但有意义的主题以及有关所讨论的无线电型号的信息（如果适用）。在下一步中，您将有机会添加有关问题的更多详细信息。 在此处输入要添加到 Bug 的信息 输入要更新的 Bug 编号 输入您在 chirpmyradio.com 的用户名和密码。如果您没有帐户，请在继续之前单击下面的链接创建一个。 已擦除存储 %s 应用设置时出错 与电台通信时出错 排除私有和已关闭的中继 实验性的驱动程序 只能导出CSV文件 以CSV文件格式导出 以CSV文件格式导出…… 更多 FM电台 免費中繼器資料庫，提供有關歐
洲中繼器的資訊。無需帳戶。 免费中继数据库，提供欧洲中继的最新信息。不需要帐户。 无法加载电台浏览器 无法解析结果 无法发送 Bug 报告： 特性 提交新 Bug 文件不存在：%s 文件 文件： 过滤 过滤位置匹配此字符串的结果 查找 查找下一个 查找... 已完成电台作业 %s 请按照以下指示下载电台存储：
1 - 连接您的接口电缆
2 - 电台 > 从电台下载：在下载期间不要对电台进行任何操作！
3 - 断开接口电缆
 请按照以下指示下载电台存储：
1 - 连接您的接口电缆
2 - 电台 > 从电台下载：不要调整电台头上的任何设置！
3 - 断开接口电缆
 请按照以下指示下载电台存储：
1 - 关闭您的电台
2 - 连接您的接口电缆
3 - 打开您的电台，音量设置为 50%
4 - CHIRP 菜单 - 电台 - 从电台下载
 请按照以下指示下载您的配置：
1 - 关闭您的电台
2 - 将接口电缆连接到扬声器-2插孔
3 - 打开您的电台
4 - 电台 > 从电台下载
5 - 断开接口电缆！否则将没有右侧音频！
 请按照以下指示下载您的信息：
1 - 关闭您的电台
2 - 连接您的接口电缆
3 - 打开您的电台
4 - 下载您的电台数据
 请按照以下指示下载您的电台：
1 - 关闭您的电台
2 - 连接您的接口电缆
3 - 打开您的电台（如果有密码保护，请解锁）
4 - 单击“确定”开始
 请按照以下指示读取您的电台：
1 - 关闭您的电台
2 - 连接您的接口电缆
3 - 打开您的电台
4 - 单击“确定”开始
 请按照以下指示上传电台存储：
1 - 连接您的接口电缆
2 - 电台 > 上传到电台：上传期间不要操作电台！
3 - 断开接口电缆
 请按照以下指示上传电台存储：
1 - 连接您的接口电缆
2 - 电台 > 上传到电台：不要调整电台头上的任何设置！
3 - 断开接口电缆
 请按照以下指示上传电台存储：
1 - 关闭您的电台
2 - 连接您的接口电缆
3 - 打开您的电台，音量设置为 50%
4 - CHIRP 菜单 - 电台 - 上传到电台
 请按照以下指示上传您的配置：
1 - 关闭您的电台
2 - 将您的接口电缆连接到扬声器 2 插孔
3 - 打开您的电台
4 - 电台 > 上传到电台
5 - 断开接口电缆，否则将没有右侧音频！
6 - 重启电台以退出克隆模式
 请按照以下指示上传您的信息：
1 - 关闭您的电台
2 - 连接您的接口电缆
3 - 打开您的电台
4 - 上传您的电台数据
 请按照以下说明来上传信息到您的电台：
1 - 关闭电台
2 - 连接数据线
3 - 打开电台（如果有密码保护，请解锁）
4 - 单击 "确定 "开始
 请按照以下说明来写入信息到您的电台：
1 - 关闭电台
2 - 连接数据线
3 - 打开电台
4 - 单击 "确定 "开始
 请按照以下说明来下载您的信息：
1 - 关闭电台
2 - 连接数据线
3 - 打开电台
4 - 下载电台数据
 请按照以下说明来浏览您的电台：
1 - 关闭电台
2 - 连接数据线
3 - 打开电台
4 - 下载电台数据
 请按照以下说明来上传到您的电台：
1 - 关闭电台
2 - 连接数据线
3 - 打开电台
4 - 上传电台数据
 请按照以下说明来写入您的电台：
1 - 关闭电台
2 - 连接数据线
3 - 打开电台
4 - 上传电台数据
 发现 %(name)s 的空列表值：%(value)r 频率 频率 %s 超出支持范围 频率粒度（kHz） 此范围内的频率不能是AM模式 此范围内的频率需要是AM模式 超出发射频段的频率必须是双工=关闭 GMRS 正在获取设置 前往存储 前往存储： 前往... 帮助 帮助我... 十六进制 隐藏无数据的内容 当启用时，遵守CTCSS/DCS接收静噪配置，否则只有载波静噪 人类可读的注释（不存储在电台中） 设置后按距离排序 导入 从文件导入... 导入消息 不推荐导入 索引 信息 信息 上方插入一行 要安装桌面图标吗？ 与驱动程序交互 内部驱动程序错误 无效的 %(value)s（请使用十进制度数） 无效项目 无效的邮政编码 编辑无效：%s 无效或不支持的模块文件 %r 值无效 问题编号： 实时 更改语言 (Change Language) 纬度 长度必须是 %i 限制频段 限制模式 限制状态 限制结果到这个距离（公里） 限制使用方式 实时电台 加载模块... 从 issue 加载模块 从 issue 加载模块... 加载模块不会影响打开的标签页。建议（除非另有说明）在加载模块之前关闭所有标签页。 加载模块可能极其危险，可能导致您的计算机、无线电或两者都受损。绝对不要从您不信任的来源加载模块，尤其不要从主要的 CHIRP 网站（chirpmyradio.com）之外的任何地方加载模块。从其他来源加载模块等同于让他们直接访问您的计算机和上面的所有内容！尽管存在这种风险，是否继续？ 加载设置中 登录失败：请检查您的用户名和密码 Logo 字符串 1（12 个字符） Logo 字符串 2（12 个字符） 经度 存储 由于不支持的固件版本，存储是只读的 存储 %i 不能删除 存储标签（存储在电台中） 存储必须在一个存储区中才能编辑 存储 {num} 不在存储区 {bank} 制式 型号 制式 模块 模块已加载 模块加载成功 更多信息 找到多个端口：%s 下移 上移 排序启用时不能移动 创建一个新的窗口 有新版本 下方已无空行！ 未找到模块 在 issue %i 中未找到模块 没有更多空间可用；一些存储未应用 无结果 无结果！ 编号 频差 仅特定频段 仅特定制式 只有存储标签可以导出 仅活跃中继台 打开 打开最近 打开预设配置 打开文件 打开模块 打开调试日志 在新窗口中打开 仅打开中继台 打开预设配置目录 選項 可选：-122.0000 可选：100 可选：45.0000 可选：县，医院，等等. 要覆盖存储吗？ P-VFO 频道 100-109 被认为是设置。
在这个版本中，仅支持超过 130 个可用的电台设置中的一部分。
 解析 密码 粘贴 粘贴的存储将覆盖 %s 个现有存储 粘贴的存储将覆盖存储 %s 粘贴的存储将覆盖存储 %s 粘贴的存储将覆盖存储 %s 请确保在安装新版本之前退出 CHIRP！ 请仔细遵循以下步骤：
1 - 打开您的电台
2 - 将接口电缆连接到您的电台
3 - 单击此窗口上的按钮以开始下载
    (您可能会看到另一个对话框，请单击确定)
4 - 电台将发出蜂鸣声并闪烁灯光
5 - 在数据上传开始之前，您将获得 10 秒的超时时间来按下“MON”
6 - 如果一切顺利，电台将在结束时发出蜂鸣声。
克隆后，请拔掉电缆并重新启动电台，以进入正常模式。
 请仔细遵循以下步骤：
1 - 打开您的电台
2 - 将接口电缆连接到您的电台。
3 - 单击此窗口上的按钮以开始下载
    (电台将发出蜂鸣声并闪烁灯光)
4 - 然后按下电台上的“A”按钮开始克隆。
    (在结束时电台将发出蜂鸣声)
 请注意，开发者模式是为 CHIRP 项目的开发人员使用，或在开发人员的指导下使用。如果不小心使用，它会启用可能会损坏您的计算机和电台的行为和功能。您已经被警告！是否继续？ 请等待 插入您的电缆，然后单击确定 端口 功率 设置存储时出错 打印预览 正在打印 Prolific USB 设备 属性 查询 %s 查询数据源 DTCS 接收代码 电台 电台未确认块 %i 电台信息 电台型号： 电台在最后一个等待的块之后发送了数据，这是因为所选的型号是非美国型号，但电台是美国型号。请选择正确的型号并重试。 RadioReference Canada 需要登录才能查询 RadioReference.com 是世界上最大的无线电通信数据提供商
<small>需要高级账户</small> 接收 DTCS 代码 接收频率 最近 最近... 建议使用 55.2 需要刷新 刷新了存储 %s 重新加载驱动程序 重新加载驱动程序和文件 删除 从列表中删除所选的型号 重命名存储区 RepeaterBook 是业余无线电最全面的、全球范围内的、免费的中继台目录。 报告或更新 Bug... 正在报告新 Bug：%r 启用报告功能 报告有助于 CHIRP 项目了解哪些电台型号和操作系统平台值得我们有限的努力。如果您保持它启用，我们将不胜感激。真的要禁用报告吗？ 需要重新启动 恢复 %i 个标签页 启动时恢复标签页 已取得设置 保存 在关闭前保存？ 保存文件 已保存设置 扫描控制（跳过、包含、优先级等） 扫描列表 加密器 安全风险 选择频段计划... 选择频段 更改语言 (Change Language) 选择制式 选择一个频段计划 服务 设置 由于不支持的固件版本，设置是只读的 由双工控制的移动量（或发射频率） 显示原始存储 打开调试日志所在文件夹 显示更多其他内容 打开镜像备份所在文件夹 跳过 某些内容与此电台不兼容 某些内容不可被删除 将 %i 个存储排序 将 %i 个存储升序排列 将 %i 个存储降序排列 排序列： 排序存储 排序 州 州/省 成功 成功发送错误报告： 发射-接收 DTCS 极性（正常或反转） 已测试并基本可用，但在使用较少见的电台变种时可能会出现问题。
请自行决定是否继续，如有问题请告知我们！ DMR-MARC 全球网络 FT-450D 电台驱动程序加载"特殊频道"选项卡，包括 PMS 扫描范围存储器（组 11）、60 米频道（组 12）、QMB（STO/RCL）存储器、HF 和50 米 HOME 存储器以及所有 A 和 B VFO 存储器。
每个波段都有最后拨号的频率的 VFO 存储器。还存储了最后的 mem-tune 配置。这些特殊频道允许有限的字段编辑。
此驱动程序还填充了通道存储器属性窗口中的"其他"选项卡。此选项卡包含不属于标准 Chirp 显示列的那些通道存储器设置的值。
随着 FT450 的支持，GUI 现在使用模式"DIG"来表示数据模式，并添加了一个新列"数据模式"来选择 USER-U、USER-L 和 RTTY
 X3Plus 驱动程序目前处于实验阶段。
没有已知问题，但您应谨慎操作。
请将首次成功下载的未编辑副本保存为 CHIRP 电台镜像 (*.img) 文件。
 此模块的作者不是 CHIRP 开发人员。建议您不要加载此模块，因为可能会带来安全风险。是否继续？ 当从命令行交互式运行 CHIRP 时，调试日志文件不可用。因此，此工具不会上传您期望的内容。建议您现在退出并以非交互方式运行 CHIRP（或将 stdin 重定向到 /dev/null） 将提交以下信息： 导入存储的推荐方法是打开源文件，然后将存储从源文件复制/粘贴到目标镜像中。如果继续使用此导入功能，CHIRP 将使用 %(file)s 中的存储替换当前打开文件中的所有存储。您想要打开此文件以复制/粘贴记忆，还是继续导入？ 该内容 此驱动程序已经与 ID-5100 的 v3 版本进行了测试。如果您的电台没有完全更新，请通过打开一个带有调试日志的错误报告来帮助我们，以便我们可以为其他版本添加支持。 此驱动程序正在开发中，应被视为实验性的。 此镜像缺少固件信息。可能是使用旧版或修改版的 CHIRP 生成的。建议您从电台下载一个新镜像，并将其用于以后的最佳安全性和兼容性。 这是一个实时模式电台，这意味着更改将实时发送到电台。不需要上传！ 这是一个与电台无关的文件，不能直接上传到电台。打开一个电台镜像（或从电台下载一个），然后将此选项卡中的项目复制/粘贴到该选项卡中，以便上传 这是一个早期测试版驱动程序
 这是一个早期测试版驱动程序 - 上传风险自负
 这是泉盛 UV-K5 的实验性驱动程序。它可能会损坏您的电台，甚至会更糟。使用风险请自负。

在尝试去进行任何修改之前，请从电台中下载并保存内存映像。您以后可以用它恢复设置。

某些细节功能尚未实现 这是仍在开发中的 BJ-9900 的实验性支持。
请确保您使用 OEM 软件进行了良好的备份。
同时请发送错误和增强请求！
您已被警告。请自行承担风险！ 这是 chirpmyradio.com 网站上已创建问题的票号 此存储并上移所有 此存储并上移区块 此选项可能会损坏您的电台！每台电台都有一组独特的校准数据，如果上传的数据来自不同的硬件设备，将会对电台造成物理损害。除非您知道自己在做什么并接受破坏电台的风险，否则不要使用此选项！ 这台电台进入编程模式的方式有点棘手，
即使原始软件也需要尝试多次才能进入。
我将尝试 8 次（大多数情况下 ~3 次就可以），这可能需要几秒钟，如果不起作用，请再试几次。
如果还是无法进入，请检查电台和电缆。
 只有当您使用了扩频的修改固件时才应启用此选项。启用此选项将导致 CHIRP 不强制执行 OEM 限制，并可能导致未定义或不受监管的行为。使用风险自负！ 此工具将向 CHIRP 跟踪器上的现有问题上传有关您的系统的详细信息。为了正常工作，它需要您在 chirpmyradio.com 上的用户名和密码。将上传有关您的系统的信息，包括调试日志、配置文件和任何打开的镜像文件。在发送之前，我们将尝试删除任何个人信息。 这将从网站 issue 中加载一个模块 亚音频 亚音频制式 亚音频静噪 亚音频静噪模式 发射功率 频差发射、分频模式或发射禁止 发射亚音频 DTCS 模式下的发射/接收 DTCS 代码，否则为发射代码 发射/接收调制方式（FM、AM、SSB 等） TSQL 模式下的发射/接收亚音频，否则为接收亚音频 调谐间隔 USB 端口查找器 无法确定您的电缆端口。检查您的驱动程序和连接。 无法在加载电台之前编辑存储器 无法找到预设配置 %r 无法在视图排序时导入 无法打开剪贴板 无法查询 无法读取最后一个块。这通常发生在所选模型为美国，但电台是非美国的（或扩频的）情况下。请选择正确的模型并重试。 无法在此系统上显示 %s 无法在此内容上设置 %s 无法上传此文件 美国 拔掉您的电缆（如果需要）然后点击确定 不支持的型号 %r 更新现有 Bug 正在更新 Bug %s 上传说明 由于不支持的固件版本，上传被禁用 上传到电台 上传到电台…… 使用等宽字体 使用大号字体 用户名 数值无法用 %i 位表示 数值必须至少为 %.4f 数值必须至多为 %.4f 数值必须正好为 %i 位十进制数 数值必须为零或更大 数值 厂商 查看 警告！ 警告 警告：%s 欢迎 您是否希望 CHIRP 为您安装一个桌面图标？ 您在过去一周内打开了多个 issue。CHIRP 限制您可以打开的问题数量以避免滥用。如果您确实需要打开另一个问题，请通过网站打开。 您的基于 Prolific 的 USB 设备将无法工作，除非回滚到旧版本的驱动程序。访问 CHIRP 网站以了解更多关于如何解决此问题的信息？ 您的电缆似乎在端口： 比特 字节 字节每个 已禁用 已启用 {bank} 已满 