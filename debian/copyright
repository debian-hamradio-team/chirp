Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: chirp
Upstream-Contact: Dan Smith <dsmith@danplanet.com>
Source: https://chirp.danplanet.com/
Copyright: GPL-3+

Files: *
Copyright: 2012 Dan Smith <dsmith@danplanet.com>
License: GPL-3+

Files: debian/*
Copyright: 2017-2025 Dave Hibberd <hibby@debian.org>
	   2019-2022 Christoph Berg <myon@debian.org>
	   2021 Paul Tagliamonte <paultag@debian.org>
	   2016-2019 Ana Custura <acute@debian.org>
	   2015-2017 Iain Learmonth <irl@debian.org>
	   2013 Christopher Knadle <Chris.Knadle@coredump.us>
           2011 Steve Conklin AI4QR <steve@conklinhouse.com>
License: GPL-3+

Files: chirp/bitwise.py
 chirp/bitwise_grammar.py
 chirp/chirp_common.py
 chirp/cli/main.py
 chirp/detect.py
 chirp/drivers/alinco.py
 chirp/drivers/anytone.py
 chirp/drivers/baofeng_uv3r.py
 chirp/drivers/ft2800.py
 chirp/drivers/ft2900.py
 chirp/drivers/ft50.py
 chirp/drivers/ft60.py
 chirp/drivers/ft7100.py
 chirp/drivers/ft7800.py
 chirp/drivers/ftm350.py
 chirp/drivers/generic_csv.py
 chirp/drivers/ic208.py
 chirp/drivers/ic2100.py
 chirp/drivers/ic2200.py
 chirp/drivers/ic2720.py
 chirp/drivers/ic2820.py
 chirp/drivers/ic9x.py
 chirp/drivers/ic9x_icf.py
 chirp/drivers/ic9x_icf_ll.py
 chirp/drivers/ic9x_ll.py
 chirp/drivers/icf.py
 chirp/drivers/icq7.py
 chirp/drivers/ict70.py
 chirp/drivers/ict8.py
 chirp/drivers/icv86.py
 chirp/drivers/icw32.py
 chirp/drivers/icx8x.py
 chirp/drivers/id31.py
 chirp/drivers/id51.py
 chirp/drivers/id800.py
 chirp/drivers/id880.py
 chirp/drivers/idrp.py
 chirp/drivers/kenwood_d7.py
 chirp/drivers/kenwood_live.py
 chirp/drivers/puxing.py
 chirp/drivers/rh5r_v2.py
 chirp/drivers/th_uv3r.py
 chirp/drivers/th_uvf8d.py
 chirp/drivers/tmd710.py
 chirp/drivers/tmv71.py
 chirp/drivers/tmv71_ll.py
 chirp/drivers/vx6.py
 chirp/drivers/vx7.py
 chirp/drivers/vx8.py
 chirp/drivers/vxa700.py
 chirp/drivers/wouxun.py
 chirp/drivers/yaesu_clone.py
 chirp/errors.py
 chirp/import_logic.py
 chirp/memmap.py
 chirp/platform.py
 chirp/util.py
 chirp/wxui/config.py
 tests/icom_clone_simulator.py
 tests/unit/test_bitwise.py
 tests/unit/test_icom_clone.py
 tests/unit/test_mappingmodel.py
 tests/unit/test_settings.py
 tools/serialsniff.c
Copyright: 2008-2019, Dan Smith <dsmith@danplanet.com>
License: GPL-3+

Files: chirp/drivers/id5100.py
 chirp/sources/przemienniki_eu.py
 chirp/sources/przemienniki_net.py
 chirp/wxui/bankedit.py
 chirp/wxui/clone.py
 chirp/wxui/common.py
 chirp/wxui/developer.py
 chirp/wxui/main.py
 chirp/wxui/memedit.py
 chirp/wxui/printing.py
 chirp/wxui/query_sources.py
 chirp/wxui/radioinfo.py
 chirp/wxui/radiothread.py
 chirp/wxui/report.py
 chirp/wxui/settingsedit.py
 tests/unit/test_alinco_clone.py
 tests/unit/test_icf.py
 tests/unit/test_kguv920.py
Copyright: 2022-2023, Dan Smith <chirp@f.danplanet.com>
License: GPL-3+

Files: chirp/drivers/baofeng_common.py
 chirp/drivers/baofeng_wp970i.py
 chirp/drivers/bf_t8.py
 chirp/drivers/gmrsuv1.py
 chirp/drivers/gmrsv2.py
 chirp/drivers/iradio_uv_5118.py
 chirp/drivers/iradio_uv_5118plus.py
 chirp/drivers/mml_jc8810.py
 chirp/drivers/mursv1.py
 chirp/drivers/radtel_t18.py
 chirp/drivers/retevis_rb15.py
 chirp/drivers/retevis_rb17p.py
 chirp/drivers/retevis_rb28.py
 chirp/drivers/retevis_rt1.py
 chirp/drivers/retevis_rt21.py
 chirp/drivers/retevis_rt22.py
 chirp/drivers/retevis_rt23.py
 chirp/drivers/retevis_rt26.py
 chirp/drivers/retevis_rt76p.py
 chirp/drivers/retevis_rt98.py
 chirp/drivers/tdxone_tdq8a.py
 chirp/drivers/uv5x3.py
 chirp/drivers/uv6r.py
Copyright: 2016-2023, Jim Unroe <rock.unroe@gmail.com>
License: GPL-2+

Files: chirp/drivers/btech.py
 chirp/drivers/vgc.py
Copyright: 2016-2023, Pavel Milanes CO7WT <pavelmc@gmail.com>
  2016-2023, Jim Unroe KC9HI <rock.unroe@gmail.com>
License: GPL-2+

Files: chirp/drivers/lt725uv.py
Copyright: 2018, Rick DeWitt RJD <aa0rd@yahoo.com>
  2021-2023, Mark Hartong AJ4YI <mark.hartong@verizon.net>
  2016-2023, Jim Unroe KC9HI <rock.unroe@gmail.com>
License: GPL-2+ 

Files: chirp/drivers/hobbypcb.py
 chirp/drivers/tdh8.py
 chirp/drivers/template.py
 chirp/drivers/tg_uv2p.py
 chirp/drivers/thuv1f.py
 chirp/drivers/tk8102.py
 chirp/drivers/tk8180.py
 chirp/drivers/uv5r.py
 chirp/drivers/uvb5.py
 chirp/settings.py
Copyright: 2012-2019, Dan Smith <dsmith@danplanet.com>
License: GPL-2+

Files: chirp/drivers/ap510.py
 chirp/drivers/ft1802.py
 chirp/drivers/generic_tpe.py
 chirp/drivers/ts2000.py
 chirp/drivers/vx510.py
 chirp/sources/dmrmarc.py
 chirp/sources/radioreference.py
 chirp/wxui/fips.py
Copyright: 2012-2016, Tom Hayward <tom@tomh.us>
License: GPL-3+

Files: chirp/bandplan.py
 chirp/bandplan_au.py
 chirp/bandplan_iaru_r1.py
 chirp/bandplan_iaru_r2.py
 chirp/bandplan_iaru_r3.py
Copyright: 2013, Sean Burford <sburford@google.com>
License: GPL-3+

Files: chirp/directory.py
 chirp/drivers/kenwood_hmk.py
 chirp/drivers/kenwood_itm.py
 chirp/drivers/vx5.py
Copyright: 2008-2011, Dan Smith <dsmith@danplanet.com>
  2012, Tom Hayward <tom@tomh.us>
License: GPL-3+

Files: chirp/drivers/ft817.py
 chirp/drivers/ft857.py
 chirp/drivers/wouxun_common.py
Copyright: 2012, Filippi Marco <iz3gme.marco@gmail.com>
License: GPL-3+

Files: chirp/drivers/tk3140.py
 chirp/drivers/tk8160.py
Copyright: 2023, Dan Smith <chirp@f.danplanet.com>
License: GPL-2+

Files: chirp/drivers/tk270.py
 chirp/drivers/tk760.py
Copyright: 2016, Pavel Milanes CO7WT, <co7wt@frcuba.co.cu> <pavelmc@gmail.com>
License: GPL-2+

Files: chirp/drivers/bf_t1.py
 chirp/drivers/tk760g.py
Copyright: 2016, Pavel Milanes, CO7WT, <pavelmc@gmail.com>
  2017, Pavel Milanes, CO7WT, <pavelmc@gmail.com>
License: GPL-2+

Files: chirp/drivers/ftm3200d.py
 chirp/drivers/ftm7250d.py
Copyright: 2010, Dan Smith <dsmith@danplanet.com>
  2017, Wade Simmons <wade@wades.im>
License: GPL-3+

Files: chirp/drivers/ft8100.py
 chirp/drivers/th_uv3r25.py
Copyright: 2010, Eric Allen <eric@hackerengineer.net>
  2015, Eric Allen <eric@hackerengineer.net>
License: GPL-3+

Files: chirp/drivers/anytone_ht.py
 chirp/drivers/retevis_rt87.py
Copyright: 2015, Jim Unroe <rock.unroe@gmail.com>
  2021, Jim Unroe <rock.unroe@gmail.com>
License: GPL-3+

Files: chirp/drivers/kguv980p.py
 chirp/drivers/kguv9dplus.py
Copyright: 2022, Mel Terechenok <melvin.terechenok@gmail.com>
License: GPL-3+

Files: chirp/drivers/kg935g.py
 chirp/drivers/kguv8e.py
Copyright: 2019, Pavel Milanes CO7WT <pavelmc@gmail.com>
License: GPL-3+

Files: chirp/drivers/ts480.py
 chirp/drivers/ts590.py
Copyright: 2019, Rick DeWitt <aa0rd@yahoo.com>
License: GPL-3+

Files: chirp/logger.py
 tools/cpep8.py
Copyright: 2015, Zachary T Welch <zach@mandolincreekfarm.com>
License: GPL-3+

Files: chirp/drivers/ft4.py
Copyright: 2011, Dan Smith <dsmith@danplanet.com>
  2012, Dan Smith <dsmith@danplanet.com>
  2019, Dan Clemmensen <DanClemmensen@Gmail.com>
License: GPL-2+ and GPL-3+

Files: chirp/drivers/th_uv88.py
Copyright: NONE
License: GPL-2+

Files: chirp/drivers/h777.py
Copyright: 2013, Andrew Morgan <ziltro@ziltro.com>
License: GPL-2+

Files: chirp/drivers/radioddity_r2.py
Copyright: August 2018 Klaus Ruebsam <dg5eau@ruebsam.eu>
License: GPL-2+

Files: chirp/drivers/uvk5.py
Copyright: 2012, Dan Smith <dsmith@danplanet.com>
  2023, Jacek Lipkowski <sq5bpf@lipkowski.org>
License: GPL-2+

Files: chirp/drivers/kyd.py
Copyright: 2014, Dan Smith <dsmith@danplanet.com>
  2014, Jim Unroe <rock.unroe@gmail.com>
License: GPL-2+

Files: chirp/drivers/th350.py
Copyright: 2013, Dan Smith <dsmith@danplanet.com>
  2019, Zhaofeng Li <hello@zhaofeng.li>
License: GPL-2+

Files: chirp/drivers/th9000.py
Copyright: 2015, David Fannin KK6DF <kk6df@arrl.org>
License: GPL-2+

Files: chirp/drivers/th7800.py
Copyright: 2014, James Lee N1DDK <jml@jmlzone.com>
  2014, Jens Jensen <af5mi@yahoo.com>
  2014, Tom Hayward <tom@tomh.us>
  2016, Nathan Crapo <nathan_crapo@yahoo.com> (TH-7800 only)
License: GPL-2+

Files: chirp/drivers/th9800.py
Copyright: 2014, James Lee N1DDK <jml@jmlzone.com>
  2014, Jens Jensen <af5mi@yahoo.com>
  2014, Tom Hayward <tom@tomh.us>
License: GPL-2+

Files: chirp/drivers/vx2.py
Copyright: 2013, Jens Jensen <kd4tjx@yahoo.com>
License: GPL-2+

Files: chirp/drivers/bjuv55.py
Copyright: 2013, Jens Jensen AF5MI <kd4tjx@yahoo.com>
License: GPL-2+

Files: chirp/drivers/anytone778uv.py
Copyright: 2020, Joe Milbourn <joe@milbourn.org.uk>
  2021, Jim Unroe <rock.unroe@gmail.com>
License: GPL-2+

Files: chirp/drivers/puxing_px888k.py
Copyright: 2016, Leo Barring <leo.barring@protonmail.com>
License: GPL-2+

Files: chirp/drivers/kyd_IP620.py
Copyright: 2015, Lepik.stv <lepik.stv@gmail.com>
License: GPL-2+

Files: chirp/drivers/bj9900.py
Copyright: 2015, Marco Filippi IZ3GME <iz3gme.marco@gmail.com>
License: GPL-2+

Files: chirp/drivers/hg_uv98.py
Copyright: 2022, Masen Furer <kf7hvm@0x26.net>
License: GPL-2+

Files: chirp/drivers/fd268.py
Copyright: 2015, Pavel Milanes CO7WT <pavelmc@gmail.com>
License: GPL-2+

Files: chirp/drivers/ftlx011.py
Copyright: 2019, Pavel Milanes, CO7WT <pavelmc@gmail.com>
License: GPL-2+

Files: chirp/drivers/th_uv8000.py
Copyright: 2019, Rick DeWitt (RJD), <aa0rd@yahoo.com>
License: GPL-2+

Files: chirp/drivers/vx3.py
Copyright: 2011, Rick Farina <sidhayn@gmail.com>
License: GPL-2+

Files: chirp/drivers/boblov_x3plus.py
Copyright: 2018, Robert C Jennings <rcj4747@gmail.com>
License: GPL-2+

Files: chirp/drivers/leixen.py
Copyright: 2014, Tom Hayward <tom@tomh.us>
License: GPL-2+

Files: debian/copyright
Copyright: 2012, Dan Smith <dsmith@danplanet.com>
  2013, Christopher Knadle <Chris.Knadle@coredump.us>
  GPL-3+
License: GPL-2+ and GPL-3+

Files: chirp/drivers/ft1d.py
Copyright: 2010, Dan Smith <dsmith@danplanet.com>
  2014, Angus Ainslie <angus@akkea.ca>
  2017, Nicolas Pike <nick@zbm2.com>
  2023, Declan Rieb <WD5EQY@arrl.net>
License: GPL-3+

Files: chirp/drivers/thd72.py
Copyright: 2010, Vernon Mauery <vernon@mauery.org>
  2016, Angus Ainslie <angus@akkea.ca>
License: GPL-3+

Files: chirp/drivers/ts850.py
Copyright: 2018, Antony Jordan <me6kxv@gmail.com>
License: GPL-3+

Files: chirp/drivers/anytone_iii.py
Copyright: 2020, Brad Schuler K0BAS <brad@schuler.ws>
License: GPL-3+

Files: chirp/drivers/icv80.py
Copyright: 2008, Dan Smith <dsmith at danplanet.com>
License: GPL-3+

Files: chirp/drivers/ga510.py
Copyright: 2011, Dan Smith <dsmith@danplanet.com>
  2020, Jiauxn Yang <jiaxun.yang@flygoat.com>
  2023, Dave Liske <dave@micuisine.com>
License: GPL-3+

Files: chirp/drivers/ft2d.py
Copyright: 2010, Dan Smith <dsmith@danplanet.com>
  2017, Declan Rieb <darieb@comcast.net>
  2017, Wade Simmons <wade@wades.im>
License: GPL-3+

Files: chirp/drivers/ft90.py
Copyright: 2011, Dan Smith <dsmith@danplanet.com>
  2013, Jens Jensen AF5MI <kd4tjx@yahoo.com>
License: GPL-3+

Files: chirp/drivers/ft70.py
Copyright: 2010, Dan Smith <dsmith@danplanet.com>
  2017, Nicolas Pike <nick@zbm2.com>
License: GPL-3+

Files: chirp/drivers/ict7h.py
Copyright: 2012, Eric Allen <ericpallen@gmail.com>
License: GPL-3+

Files: chirp/drivers/id51plus.py
Copyright: 2015, Eric Dropps <kc1ckh@kc1ckh.com>
License: GPL-3+

Files: chirp/drivers/ft818.py
Copyright: 2012, Filippi Marco <iz3gme.marco@gmail.com>
  2018, Vinny Stipo <v@xpctech.com>
License: GPL-3+

Files: chirp/drivers/hf90.py
Copyright: 2023, Finn Thain <vk3fta@fastmail.com.au>
License: GPL-3+

Files: chirp/drivers/icx90.py
Copyright: 2018-2020, Jaroslav Škarvada <jskarvad@redhat.com>
License: GPL-3+

Files: chirp/drivers/vx170.py
Copyright: 2014, Jens Jensen <af5mi@yahoo.com>
License: GPL-3+

Files: chirp/drivers/ft1500m.py
Copyright: 2012, Tom Hayward <tom@tomh.us>
  2019, Josh Small VK2HFF <chirp@vk2hff.ampr.org>
License: GPL-3+

Files: chirp/drivers/kguv8dplus.py
Copyright: 2017, Krystian Struzik <toner_82@tlen.pl>
License: GPL-3+

Files: chirp/drivers/kguv920pa.py
Copyright: 2022, Matthew Handley <kf7tal@gmail.com>
License: GPL-3+

Files: chirp/drivers/ic2730.py
Copyright: 2018, Rhett Robinson <rrhett@gmail.com> Vers 1.0
License: GPL-3+

Files: chirp/drivers/ft450d.py
Copyright: 2018, Rick DeWitt (aa0rd@yahoo.com) V2.0
License: GPL-3+

Files: chirp/drivers/kguv8d.py
Copyright: 2014, Ron Wellsted <ron@m0rnw.uk> M0RNW
License: GPL-3+

Files: chirp/drivers/icp7.py
Copyright: 2017, SASANO Takayoshi (JG1UAA) <uaa@uaa.org.uk>
License: GPL-3+

Files: chirp/drivers/ic2300.py
Copyright: 2017, Windsor Schmidt <windsor.schmidt@gmail.com>
License: GPL-3+

Files: chirp/share/com.chirpmyradio.CHIRP.metainfo.xml
Copyright: 2024, AsciiWolf <mail@asciiwolf.com> 
License: CC0-1.0

License: GPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: CC0-1.0
 CREATIVE COMMONS CORPORATION IS NOT A LAW FIRM AND DOES NOT PROVIDE LEGAL
 SERVICES. DISTRIBUTION OF THIS DOCUMENT DOES NOT CREATE AN ATTORNEY-CLIENT
 RELATIONSHIP. CREATIVE COMMONS PROVIDES THIS INFORMATION ON AN "AS-IS" BASIS.
 CREATIVE COMMONS MAKES NO WARRANTIES REGARDING THE USE OF THIS DOCUMENT OR
 THE INFORMATION OR WORKS PROVIDED HEREUNDER, AND DISCLAIMS LIABILITY FOR
 DAMAGES RESULTING FROM THE USE OF THIS DOCUMENT OR THE INFORMATION OR WORKS
 PROVIDED HEREUNDER.
 .
 Statement of Purpose
 .
 The laws of most jurisdictions throughout the world automatically confer
 exclusive Copyright and Related Rights (defined below) upon the creator and
 subsequent owner(s) (each and all, an "owner") of an original work of
 authorship and/or a database (each, a "Work").
 .
 Certain owners wish to permanently relinquish those rights to a Work for the
 purpose of contributing to a commons of creative, cultural and scientific
 works ("Commons") that the public can reliably and without fear of later
 claims of infringement build upon, modify, incorporate in other works, reuse
 and redistribute as freely as possible in any form whatsoever and for any
 purposes, including without limitation commercial purposes. These owners may
 contribute to the Commons to promote the ideal of a free culture and the
 further production of creative, cultural and scientific works, or to gain
 reputation or greater distribution for their Work in part through the use and
 efforts of others.
 .
 For these and/or other purposes and motivations, and without any expectation
 of additional consideration or compensation, the person associating CC0 with
 a Work (the "Affirmer"), to the extent that he or she is an owner of
 Copyright and Related Rights in the Work, voluntarily elects to apply CC0 to
 the Work and publicly distribute the Work under its terms, with knowledge of
 his or her Copyright and Related Rights in the Work and the meaning and
 intended legal effect of CC0 on those rights.
 .
 1. Copyright and Related Rights. A Work made available under CC0 may be
 protected by copyright and related or neighboring rights ("Copyright and
 Related Rights"). Copyright and Related Rights include, but are not limited
 to, the following:
 .
 i.   the right to reproduce, adapt, distribute, perform, display, communicate,
      and translate a Work;
 ii.  moral rights retained by the original author(s) and/or performer(s);
 iii. publicity and privacy rights pertaining to a person's image or likeness
      depicted in a Work;
 iv.  rights protecting against unfair competition in regards to a Work,
      subject to the limitations in paragraph 4(a), below;
 v.   rights protecting the extraction, dissemination, use and reuse of data
      in a Work;
 vi.  database rights (such as those arising under Directive 96/9/EC of the
      European Parliament and of the Council of 11 March 1996 on the legal
      protection of databases, and under any national implementation thereof,
      including any amended or successor version of such directive); and
 vii. other similar, equivalent or corresponding rights throughout the world
      based on applicable law or treaty, and any national implementations
      thereof.
 .
 2. Waiver. To the greatest extent permitted by, but not in contravention of,
 applicable law, Affirmer hereby overtly, fully, permanently, irrevocably and
 unconditionally waives, abandons, and surrenders all of Affirmer's Copyright
 and Related Rights and associated claims and causes of action, whether now
 known or unknown (including existing as well as future claims and causes of
 action), in the Work (i) in all territories worldwide, (ii) for the maximum
 duration provided by applicable law or treaty (including future time
 extensions), (iii) in any current or future medium and for any number of
 copies, and (iv) for any purpose whatsoever, including without limitation
 commercial, advertising or promotional purposes (the "Waiver"). Affirmer
 makes the Waiver for the benefit of each member of the public at large and to
 the detriment of Affirmer's heirs and successors, fully intending that such
 Waiver shall not be subject to revocation, rescission, cancellation,
 termination, or any other legal or equitable action to disrupt the quiet
 enjoyment of the Work by the public as contemplated by Affirmer's express
 Statement of Purpose.
 .
 3. Public License Fallback. Should any part of the Waiver for any reason be
 judged legally invalid or ineffective under applicable law, then the Waiver
 shall be preserved to the maximum extent permitted taking into account
 Affirmer's express Statement of Purpose. In addition, to the extent the
 Waiver is so judged Affirmer hereby grants to each affected person a
 royalty-free, non transferable, non sublicensable, non exclusive, irrevocable
 and unconditional license to exercise Affirmer's Copyright and Related Rights
 in the Work (i) in all territories worldwide, (ii) for the maximum duration
 provided by applicable law or treaty (including future time extensions),
 (iii) in any current or future medium and for any number of copies, and (iv)
 for any purpose whatsoever, including without limitation commercial,
 advertising or promotional purposes (the "License"). The License shall be
 deemed effective as of the date CC0 was applied by Affirmer to the Work.
 Should any part of the License for any reason be judged legally invalid or
 ineffective under applicable law, such partial invalidity or ineffectiveness
 shall not invalidate the remainder of the License, and in such case Affirmer
 hereby affirms that he or she will not (i) exercise any of his or her
 remaining Copyright and Related Rights in the Work or (ii) assert any
 associated claims and causes of action with respect to the Work, in either
 case contrary to Affirmer's express Statement of Purpose.
 .
 4. Limitations and Disclaimers.
 .
 a. No trademark or patent rights held by Affirmer are waived, abandoned,
    surrendered, licensed or otherwise affected by this document.
 b. Affirmer offers the Work as-is and makes no representations or warranties
    of any kind concerning the Work, express, implied, statutory or otherwise,
    including without limitation warranties of title, merchantability, fitness
    for a particular purpose, non infringement, or the absence of latent or
    other defects, accuracy, or the present or absence of errors, whether or
    not discoverable, all to the greatest extent permissible under applicable
    law.
 c. Affirmer disclaims responsibility for clearing rights of other persons
    that may apply to the Work or any use thereof, including without limitation
    any person's Copyright and Related Rights in the Work. Further, Affirmer
    disclaims responsibility for obtaining any necessary consents, permissions
    or other rights required for any use of the Work.
 d. Affirmer understands and acknowledges that Creative Commons is not a
    party to this document and has no duty or obligation with respect to this
    CC0 or use of the Work.
