Description: Clear lintian script-uses-bin-env error
Author: Dave Hibberd <d@vehibberd.com>
Forwarded: not-needed
Last-Update: 2024-10-26
---
This patch header follows DEP-3: http://dep.debian.net/deps/dep3/
--- a/chirp/cli/experttune.py
+++ b/chirp/cli/experttune.py
@@ -1,4 +1,4 @@
-#!/bin/env python3
+#!/usr/bin/env python3
 
 import argparse
 import contextlib
